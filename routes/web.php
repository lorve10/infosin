<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\ADMIN\AdminController;
use App\Http\Controllers\ADMIN\CustomersController;
use App\Http\Controllers\ADMIN\DevicesController;
use App\Http\Controllers\ADMIN\OrdersController;
use App\Http\Controllers\ADMIN\UsersController;
use App\Http\Controllers\ADMIN\SettingsController;
use App\Http\Controllers\ADMIN\BrandsController;
use App\Http\Controllers\ADMIN\LineaController;
use App\Http\Controllers\ADMIN\ItemsController;
use App\Http\Controllers\ADMIN\CargoController;
use App\Http\Controllers\ADMIN\TypeOrderController;
use App\Http\Controllers\ADMIN\CaratOrdenController;
use App\Http\Controllers\ADMIN\StatusController;
//ruta para Challenger partes
use App\Http\Controllers\ADMIN\ChallengerController;
//// Controladore para Usuario
use App\Http\Controllers\User\GarantiaController;






Route::get('/', function () {
    return redirect('/login');
});
Auth::routes();
//Route::get('/admon/login', [App\Http\Controllers\ADMIN\AdminController::class, 'login']);
Route::get('/encrypted/pass', [App\Http\Controllers\ADMIN\AdminController::class, 'encrypted']);
///Route::post('/login_admin', [AdminController::class, 'login_admon']);

///consultas consumible para todas para usuario o admis
Route::group(['prefix'=>'admon', 'middleware' => 'auth'],function(){
  Route::get('/getStatus', [OrdersController::class, 'Status']);
  Route::get('/getOrders', [OrdersController::class, 'getOrders']);
  Route::get('/getOrderOf', [OrdersController::class, 'getOrderOf']);
  Route::get('/getOrderOut', [OrdersController::class, 'getOrdersOut']);

  Route::get('/getUsers', [UsersController::class, 'listUsers']);
  Route::post('/userId', [UsersController::class, 'userId']);
  /// lista de partes pendientes o partes
  Route::get('/listChallenger', [ChallengerController::class, 'listChallenger']);
  Route::get('/listChallengerP', [ChallengerController::class, 'listChallengerP']);
  ////
  Route::get('/ordenPendi', [OrdersController::class, 'ordenPendientes']);
  Route::get('/ordenHistorial', [OrdersController::class, 'OrdenHistory']);

  Route::get('/partePendient', [ChallengerController::class, 'partesPendientes']);
  Route::get('/ordenRealEntrega', [OrdersController::class, 'ordenRealiPorEntrega']);


});
///esta ruta se protege solo para personas logueadas
Route::group(['middleware' => 'auth'],function(){
  Route::get('/welcome', [AdminController::class, 'start']);
  Route::get('/logout', [AdminController::class, 'logout_admin'])
  ->name('admin.logout');
});


Route::group(['prefix'=>'admon', 'middleware' => 'auth.admin'],function(){

/// Users: Aquí van las rutas para el formulario de users
Route::get('/Usuarios', [UsersController::class, 'Users']);
Route::post('/saveUsers', [UsersController::class, 'saveUsers']);

////Custumer: aqui va la ruta para formulario clientes
Route::get('/clientes', [CustomersController::class, 'Custumer']);
Route::post('/saveCustomers', [CustomersController::class, 'saveCustomers']);
Route::get('/getCustomers', [CustomersController::class, 'listCustomers']);

/// Devices: Aquín van las rutas para el formulario de Devices
Route::get('/Devices', [DevicesController::class, 'Devices']);
Route::post('/saveDevices', [DevicesController::class, 'saveDevices']);
Route::get('/getDevices', [DevicesController::class, 'listDevices']);



/// Brands traer marcas de televisores
Route::get('/get', [AdminController::class, 'Marca']);
Route::get('/getArticulo', [AdminController::class, 'Articulo']);
Route::get('/getCargo', [AdminController::class, 'Cargo']);
Route::get('/form', [AdminController::class, 'form']);
Route::get('/getLinea', [AdminController::class, 'Linea']);

Route::get('/getMunicipio', [AdminController::class, 'Municipio']);
Route::get('/getDepartamento', [AdminController::class, 'Departamento']);

/// Ordenes
Route::get('/ordenes', [OrdersController::class, 'Orders']);
Route::get('/getServe', [OrdersController::class, 'TipoServi']);
Route::get('/getCart', [OrdersController::class, 'TipoCarat']);
Route::get('/getCustDiv', [OrdersController::class, 'Customer_Device']);
Route::post('/save', [OrdersController::class, 'saveOrders']);
Route::post('/saveEntrega', [OrdersController::class, 'updateOrders']);
///rutas pdf de orden
Route::post('/print/order', [OrdersController::class, 'pdfOrder']);
Route::post('/print/ordersalida', [OrdersController::class, 'pdfOrderSalida']);
Route::get('/vista/ordenDpf', [OrdersController::class, 'ordenPdf']);


/// tablas de devices

///configuraciones creacion de cruds por tablas
Route::get('/configuraciones', [SettingsController::class, 'Settings']);
Route::post('/saveMarca', [BrandsController::class, 'saveMarca']);
Route::post('/saveLinea', [LineaController::class, 'saveLinea']);
Route::post('/saveArticulo', [ItemsController::class,'saveArticulo']);
Route::post('/saveCargo', [CargoController::class,'saveCargo']);
Route::post('/savetipoOrden', [TypeOrderController::class,'saveOrderType']);
Route::post('/saveCaracter', [CaratOrdenController::class,'saveCara']);
Route::post('/saveStatus', [StatusController::class,'saveStatus']);

//configuraciones deleted
Route::post('/deleted', [BrandsController::class, 'deleted']);
Route::post('/deletedTipe', [TypeOrderController::class,'deleted']);
Route::post('/deletedCara', [CaratOrdenController::class,'deleted']);
Route::post('/deletedLinea', [LineaController::class, 'deletedLinea']);
Route::post('/deletedStatus', [StatusController::class, 'deleted']);
Route::post('/deletedSArticulo', [ItemsController::class, 'deletedItems']);
Route::post('/deletedCargos', [CargoController::class, 'deleted']);

/// llegada de Challenger Partes
Route::get('/Repuestos', [ChallengerController::class, 'partes']);
Route::post('/savePart', [ChallengerController::class, 'savePart']);


Route::post('/deletedSArticulo', [ItemsController::class, 'deletedItems']);
Route::post('/deletedCargos', [CargoController::class, 'deleted']);
///Challerger partes

});
//// Rutas para empleados
Route::group(['prefix'=>'tecnico', 'middleware' => 'auth.user'],function(){

///rutas para Garantia vista, listar, editar
  Route::get('/Garantia', [GarantiaController::class, 'garantia']);
  Route::get('/getGarantia', [GarantiaController::class, 'getGarantia']);
  Route::post('/saveCobrable',[GarantiaController::class, 'saveCobrable']);/// esta ruta es para ordenes para hacer la reparacion y cambiar estado a reparado

///rutas para Garantia vista, listar, editar
  Route::get('/Extendida', [ExtendidaController::class, 'extendida']);
  Route::get('/getExtendida', [ExtendidaController::class, 'getExtendida']);


///Challenger partes
Route::get('/Challenger', [ChallengerController::class, 'Challenger']);
Route::post('/saveChallenger', [ChallengerController::class, 'saveChallenger']);
///rutas pdf de orden
Route::post('/print/order', [OrdersController::class, 'pdfOrder']);
Route::get('/vista/ordenDpf', [OrdersController::class, 'ordenPdf']);


});
