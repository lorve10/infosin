@extends('layouts.Base')

@section('title', 'Home')

@section('content')
  <!DOCTYPE html>
  <html lang="en" dir="ltr">
    <head>
      <meta charset="utf-8">
      <link href="{{ asset('css/app.css') }}"
      rel="stylesheet">
      <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
      <title>Lista ordenes</title>
    </head>
    <body>
          <div id="IndexOrden"></div>
    </body>
  
    <script>
      $(document).ready(function(){
      $('#garantia').addClass('active');
      })
    </script>
  </html>

@endsection
