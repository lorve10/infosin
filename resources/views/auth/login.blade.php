<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <title>Login</title>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
</head>
  <body>
  <form method="POST" action="{{ route('login') }}">
    @csrf
    <div class="h-screen font-sans login bg-cover">
      <div class="container mx-auto h-full flex flex-1 justify-center items-center">
          <div class="w-full max-w-lg">
            <div class="leading-loose">
              <div class="max-w-sm m-4 p-10 bg-white bg-opacity-25 rounded shadow-xl">
                  <p class="text-white font-medium text-center text-lg font-bold">LOGIN</p>
                    <div class="">
                      <label class="block text-sm text-white" for="email">Email</label>
                      <input class="w-full px-5 py-1 text-gray-700 bg-gray-300 rounded focus:outline-none focus:bg-white"
                         value="{{ old('email') }}" name="email"
                         type="email" id="email"  placeholder="Digite usuario"
                         />
                    </div>

                    <div class="mt-2">
                      <label class="block  text-sm text-white">{{ __('Password') }}</label>
                       <input class="w-full px-5 py-1 text-gray-700 bg-gray-300 rounded focus:outline-none focus:bg-white"
                       name="password"
                       value="{{ old('password') }}"
                        type="password" id="password"
                        placeholder="Digite a contraseña" arial-label="password" required/>
                    </div>


                    <div class="mt-4 justify-center	text-center">
                      <button class="px-4 py-1 align-content: center text-white font-light
                      tracking-wider bg-gray-900 hover:bg-gray-800 rounded"
                      type="submit"
                      >{{ __('Login') }}</button>
                    </div>
                    @error('email')
                        <span class="invalid-feedback mt-2 otra" role="alert">
                            <strong>Contraseña o Correo incorrecto</strong>
                        </span>
                    @enderror

              </div>

            </div>
          </div>
        </div>
      </div>
      </form>
  </body>
</html>
<style media="screen">
  .otra{
    font-weight: bolder;
    margin-left: 37px;
    background-color: darkgray;
  }
  .login{
    background: url('https://tailwindadmin.netlify.app/dist/images/login-new.jpeg');
    background: url('http://bit.ly/2gPLxZ4');
    background-repeat: no-repeat;
    background-size: cover;
  }
</style>
