<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
      <link rel="stylesheet" href="{{ public_path('css/pdfExtenSalida.css') }}" type="text/css">
    <title>
       @if($orden->marca)
        {{$orden->marca}}
      @endif</title>
    <style>

    </style>

  </head>
  <body>
      <table class="encabezado ">
          <tr>
            <td >
              <p class="codigo" style="text-align: left;">UN50TU8000KXZL</p>
            </td>
            <td class="info_empresa" rowspan="2">
              <div class="">
                <span class="h2">INSOFIN</span>
                <p >ELECTRONICA DIGITAL DEL HUILA S.A.S <br>
                  Calle 22 No.6-13 PBX: 8759286 - 8755511 <br>
                  Cel: 3174341364 Neiva - Huila
                </p>
              </div>
            </td>
            <td >
              <p  class="codigo" style="text-align: right;">09P73CXR500243H</p>
            </td>
          </tr>
          <tr>
              <td class="td1">
                <div class="contenedor_logo">
                    <div class="">
                      <img src="" alt="">
                      <img src="{{ public_path('images/marcas.jpg') }}" width="200px" height="80px" class="img"/>
                    </div>
                </div>
              </td>
              <td  class="info_factura">
                <div>
                  <div class="round">
                    <span class="h3">Información</span>
                    <p><strong>Fecha: </strong>{{ date('Y-m-d ') }}</p>
                    <p><strong>Hora: </strong>{{ date('h:i:s A')}}</p>
                    <p><strong>Fecha de entrada: </strong>{{$orden->fecha_entrada}}</p>
                    <p><strong>Seguimiento: </strong></p>
                  </div>
                </div>
              </td>
          </tr>
      </table>

      <table class="tabla">
          <tbody>
            <tr class="">
                <td colspan="2">
                    <label>Nombre: </label>{{$orden->nombre}}
                </td>
                <td colspan="1" class="border px-2">
                    <label >Cédula: </label>{{$orden->id_doc}}
                </td>
                <td colspan="1" >
                    <label >Celular: </label>{{$orden->celular}}
                </td>
            </tr>
            <tr class="">
              <td colspan="2" >
                  <label >Dirección: </label>{{$orden->direccion}}
              </td>
              <td colspan="1" >
                  <label >Ciudad: </label>{{$orden->ciudad}}
              </td>
              <td colspan="1" >
                  <label >Teléfono residencia: </label>{{$orden->telcasa}}
              </td>
            <tr>
              <td colspan="1" style="width:30%">
                  <label >Fecha de entrada: </label>{{$orden->fecha_entrada}}
              </td>
              <td colspan="1" style="width:20%">
                  <label >Articulo: </label>{{$orden->articulo}}
              </td>
              <td colspan="1" style="width:20%">
                  <label >Marca: </label>{{$orden->marca}}
              </td>
              <td colspan="1" style="width:30%">
                  <label>Modelo: </label>{{$orden->modelo}}
              </td>
            </tr>
            <tr>
              <td colspan="1">
                  <label >Serie: </label>{{$orden->serie}}
              </td>
              <td colspan="3" class="">
                  <label class="">Estado físico: </label>{{$orden->estado_fisico}}
              </td>
            </tr>
            <tr class="">
                <td colspan="4" >
                    <label >Accesorios: </label>{{$orden->accesorios}}
                </td>
            </tr>
            <tr>
              <td colspan="1" style="width:30%">
                  <label >Fecha de compra: </label>{{$orden->fecha_compra}}
              </td>
              <td colspan="1" style="width:20%">
                  <label >Factura: </label>{{$orden->factura}}
              </td>
              <td colspan="2" style="width:20%">
                  <label >Distribuidor: </label>{{$orden->distribuidor}}
              </td>
            </tr>
            <tr>
              <td colspan="1" style="width:30%">
                  <label >Fecha de reparado: </label>{{$orden->fecha_reparacion}}
              </td>
              <td colspan="2" style="width:20%">
                  <label >Fecha de entrega: </label>{{$orden->fecha_entrega}}
              </td>
              <td colspan="1" style="width:20%">
                  <label >Código de bloque: </label>
              </td>
            </tr>
            </tbody>
      </table>

      <table class="tabla margin" >
          <tbody>
            <tr>
              <td colspan="1"><span class="reparacion">Síntoma reportado</span></td>
            </tr>
            <tr style="background:#FBFBF9">
              <td colspan="1" style="width:24%">
                  {{$orden->sintoma}}
              </td>
            </tr>
          </tbody>
      </table>
      <table class="tabla margin" >
          <tbody>
            <tr>
              <td colspan="1"><span class="reparacion">Descripción de la falla</span></td>
            </tr>
            <tr style="background:#FBFBF9">
              <td colspan="1" style="width:24%">
                  {{$orden->desc_falla}}
              </td>
            </tr>
          </tbody>
      </table>
      <table class="tabla margin" >
          <tbody>
            <tr>
              <td colspan="1"><span class="reparacion">Descripción de la reparación</span></td>
            </tr>
            <tr style="background:#FBFBF9">
              <td colspan="1" style="width:24%">
                  {{$orden->desc_reparacion}}
              </td>
            </tr>
          </tbody>
      </table>

      <div class="div_politica">
          <table class="tabla2">
            <tr>
              <td colspan="2">
                  <p class="politica" style="text-align: center; margin-bottom:1px"><strong>CONSTANCIA DE RECIBO Y/O REPARACIÓN DE PRODUCTOS MARCA
                  @if($orden->marca)
                   {{$orden->marca}}
                 @endif:
                  </strong></p>
                  <p class="politica">
                    El consumidor con su firma acepta de manera expresa, libre y espontánea las condiciones que se aplicarán para la prestación del servicio técnico de los productos marca @if($orden->marca){{$orden->marca}}@endif, las cuales están concordadas con Ley 1480 de 2011, el Decreto 735 de 2013 y los Decretos que regulen la materia contenidos en el certificado de garantía de su artículo.
                    <strong>CONDICIONES GENERALES PARA SERVICIOS EN GARANTÍA:</strong><br>
                    1. @if($orden->marca){{$orden->marca}}@endif ELECTRONICS COLOMBIA S.A. ofrece servicio en garantía únicamente para los productos importados por este al país.<br>
                    2. El consumidor podrá reclamar servicio en garantía durante el término ofrecido por @if($orden->marca){{$orden->marca}}@endif, para ello se sugiere presentar la fotocopia de la factura de compra del producto y el certificado de garantía. En caso de no contar con la factura de compra,
                    @if($orden->marca){{$orden->marca}}@endif adelantará las validaciones a que haya lugar y en un plazo de tres días hábiles le dará contestación respecto a la solicitud.<br>
                    3. Las reparaciones efectuadas para productos con garantía vigente no representarán ningún costo para el consumidor.<br>
                    4. La reparación deberá realizarse dentro de los treinta (30) días hábiles siguientes, contados a partir del día siguiente a la entrega del bien para la reparación.<br>
                    5. En los casos en los que el productor o proveedor dispongan de un bien en préstamo para el consumidor mientras se efectúa la reparación del mismo, el término para la reparación podrá extenderse hasta por sesenta (60) días hábiles.<br>
                    6. Pasado un (1) mes a partir de la fecha prevista para la devolución o a la fecha en que el consumidor debía aceptar o rechazar expresamente el servicio, sin que el consumidor acuda a retirar el bien, el Centro de Servicios Autorizado lo requerirá para
                    que lo retire dentro de los dos (2) meses siguientes a la remisión de la comunicación. Si el consumidor no lo retira se entenderá por ley que abandona el bien y el prestador del servicio deberá disponer del mismo conforme con la reglamentación vigente.<br>
                    7. La responsabilidad de cualquier siniestro ocurrido durante el transporte del producto desde y hasta las instalaciones del Centro de Servicio será responsabilidad del cliente salvo que el Centro de Servicio sea quien lo movilice.<br>
                    8. Ni @if($orden->marca){{$orden->marca}}@endif, ni el Centro de Servicio amparan con garantía el software, licencias, complementos, memorias del usuario o contenidos de terceros para los casos de aquellos productos que posean dichas propiedades (computadores portátiles, relojes,
                    teléfonos celulares, discos duros, dispositivos MP3, Televisores etc). El cliente previo a requerir soporte técnico, será el único responsable de respaldar o almacenar toda la información, memorias y programas contenidos, almacenados en los productos.<br>
                    9. El consumidor es el único responsable de tomar las medidas necesarias para garantizar la protección de la información contenida en el producto, debiendo desactivar o entregar al Centro de Servicio todas las contraseñas de seguridad antes de que los servicios se lleven a cabo.<br>
                    10. Conforme a la normatividad ambiental todos los repuestos dañados deben tener una correcta disposición final, por lo cual el consumidor acepta que el Centro de Servicio disponga de las partes o residuos resultantes en el proceso de reparación.<br>
                    11. POLÍTICA DE PRIVACIDAD, TRATAMIENTO Y PROTECCIÓN DE DATOS PERSONALES: El CENTRO DE SERVICIOS solo usará, procesará y circulará los datos personales y otra información suministrada por usted, para atender y solucionar la situación que le llevo a requerir soporte técnico. El consumidor tiene derecho a conocer, actualizar, rectificar y/o revocar la autorización para el tratamiento de la información. En particular, son derechos de los titulares de la información según se establece en el artículo 8 de la Ley 1581 de 2012 y sus decretos reglamentarios. El titular de los datos personales podrá hacer ejercicio de sus derechos a través del encargado del tratamiento de datos personales a los correos electrónicos electrodigitalneiva@gmail.com, también a la dirección registrada en este documento.
                  </p>
              </td>
            </tr>
          </table>
      </div>
      <div class="" style="width:100%; margin-top:1px">
        <table class="tabla margin ">
          <tr>
            <td rowspan="2" style="width:20%; font-size:12px; padding:1px">Consumidor autoriza revisión <br>
              o reparacón del producto.</td>
            <td style="width:5%; font-size:12px; padding:1px; text-align:center">SI</td>
            <td rowspan="2" style="width:70%; font-size:12px; padding:1px">Observaciones: </td>
          </tr>
          <tr>
            <td style="width:5%;font-size:12px; padding:1px; text-align:center">NO</td>
          </tr>
        </table>
      </div>

      <table class="tabla" style="margin-top:3px">
            <tr class="" style="background:#FBFBF9">
                <td colspan="1">
                    <label>Tec. que recibió: </label>{{$orden->tecrecibe}}
                </td>
                <td colspan="1" class="border px-2">
                    <label >Tec. que reparó: </label>{{$orden->tecasignado}}
                </td>
            </tr>
        </table>

      <div class="" >
        <table class=" margin " style="width:100%; padding-left:8px">
          <tr>
            <td style="text-align: left;">
                <p class="firmas">
                    <br>
                    Técnique entrega:&nbsp;&nbsp;&nbsp;     _________________________________ <br><br>

                </p>
                <p class="firmas" style="padding-left:52px">
                    Firma:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    _________________________________<br><br>
                </p>
                <p class="firmas" style="padding-left:43px">
                    C.C/NIT:&nbsp;&nbsp;&nbsp;  _________________________________
                </p>
            </td>
            <td  style="text-align: right;padding-right:8px">
                <p class="firmas">
                    <br>
                    Recibe a Satisfacción:&nbsp;&nbsp;&nbsp; &nbsp;     _________________________________ <br><br>
                    Firma:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;     _________________________________<br><br>
                    C.C/NIT:&nbsp;&nbsp;&nbsp; _________________________________<br>
                </p>
            </td>
          </tr>
        </table>
      </div>


    </body>
</html>
