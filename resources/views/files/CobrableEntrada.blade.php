<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ public_path('css/pdf.css') }}" type="text/css">
    <title>COBRABLE ENTRADA
      @if($orden->marca)
        {{$orden->marca}}
      @endif
    </title>
    <style>

    </style>

  </head>
  <body>
      <table class="encabezado">
          <tr>
              <td class="td1">
                <div class="contenedor_logo">
                    <div class="imagen_logo1">
                      <img src="{{ public_path('images/logo.jpg') }}" width="100px" height="50px" class="img"/>
                      <img src="" alt="">
                    </div>
                    <div class="">
                      <img src="" alt="">
                      <img src="{{ public_path('images/marcas.jpg') }}" width="200px" height="50px" class="img"/>
                    </div>
                </div>
              </td>

              <td class="info_empresa">
                <div class="">
                  <span class="h2">INSOFIN</span>

                  <p >ELECTRONICA DIGITAL DEL HUILA S.A.S <br>
                    Calle 22 No.6-13 PBX: 8759286 - 8755511 <br>
                    Cel: 3174341364 Neiva - Huila
                  </p>
                </div>
              </td>

              <td  class="info_factura">
                <div>
                  <div class="round">
                    <span class="h3">Información</span>
                    <p>Fecha: {{ date('Y-m-d ') }} </p>
                    <p>Hora: {{ date('h:i:s A')}}</p>
                    <span>
                      <label >Solicitud de Servicio:</label>
                      <label style="font-size:18px"><strong>{{$orden->orden}}</strong></label>
                    </span>
                  </div>
                </div>
              </td>
          </tr>
      </table>

      <table class="tabla">
          <tbody>
            <tr>
              <td colspan="1" style="width:30%">
                  <label >Fecha de entrada: </label>{{$orden->fecha_entrada}}
              </td>
              <td colspan="1" style="width:20%">
                  <label >Articulo: </label>{{$orden->articulo}}
              </td>
              <td colspan="1" style="width:20%">
                  <label >Marca: </label>{{$orden->marca}}
              </td>
              <td colspan="1" style="width:30%">
                  <label>Modelo: </label>{{$orden->modelo}}
              </td>
            </tr>
            <tr>
              <td colspan="1">
                  <label >Serie: </label>{{$orden->serie}}
              </td>
              <td colspan="3" class="">
                  <label class="">Estado físico: </label>{{$orden->estado_fisico}}
              </td>
            </tr>
            <tr>
              <td colspan="4" class="">
                  <label class="">Síntoma: </label>{{$orden->sintoma}}
              </td>
            </tr>

            <tr class="">
                <td colspan="3" >
                    <label >Accesorios: </label>{{$orden->accesorios}}
                </td>
                <td colspan="1" >
                    <label >Email: </label>{{$orden->email}}
                </td>
            </tr>
            <tr class="">
                <td colspan="3">
                    <label>Nombre: </label>{{$orden->nombre}}
                </td>
                <td colspan="1" class="border px-2">
                    <label >Cédula: </label>{{$orden->id_doc}}
                </td>
            </tr>
            <tr class="">
              <td colspan="3" >
                  <label >Dirección: </label>{{$orden->direccion}}
              </td>
              <td colspan="1" >
                  <label >Ciudad: </label>{{$orden->ciudad}}
              </td>
            <tr>
              <td colspan="1" >
                  <label >Celular: </label>{{$orden->celular}}
              </td>
              <td colspan="2" >
                  <label >Teléfono residencia: </label>{{$orden->telcasa}}
              </td>
              <td colspan="1">
                  <label >Teléfono Oficina: </label>{{$orden->teltrabajo}}
              </td>
            </tr>
            <tr class="">
                <td colspan="3" >
                    <label >Observaciones de Ingreso: </label> {{$orden->obs_entrada}}
                </td>
                <td colspan="1">

                  <label >Registró: </label>{{$orden->regrecibe}}

                </td>
             </tr>
          </tbody>
      </table>

      <div class="div_politica">
          <table class="tabla2">
            <tr>
              <td colspan="2">
                  <p class="politica">
                      - POLÍTICA DE PRIVACIDAD, TRATAMIENTO Y PROTECCIÓN DE DATOS PERSONALES: Autorizo de manera previa, explícita e inequivoca a ELECTRONICA DIGITAL HUILA S.A.S para el tratamiento de mis datos personales aquí suministrados
                      con las finalidades legales, contractuales y aquellas relacionadas en el objeto social de la empresa como servicios técnicos, comerciales y las aquellas contempladas en la política de protección de datos personales para el tratamiento de clientes,
                      publicada en el respectivo establecimiento de comercio. Declaro ser el titular de la información reportada. <br>
                      - La presente Solicitud de servicio, deja contancia de la entrega del articulo que de acuerdo a los detalles de este documento, el suscrito (en adelante CLIENTE) deja para su reparación, mantenimiento u otro servicio por parte de ELECTRÓNICA DIGITAL
                      HUILA (en adelante LA EMPRESA). EL CLIENTE manifiesta que el artículo lo tiene bajo titulo justo, asi mismo declara que no conoce ningun hecho o circunstancia que permita deducir o inferir que el bien fue irregularmente introducido al país.
                      Por lo tanto, asume cualquier responsabilidad que las autoridades administrativas o judiciales determinen en relación con este bien. <br>
                      - En caso de que el CLIENTE no haya retirado el articulo reparado o revisado, pasados NOVENTA DÍAS desde la fecha de entrega del artículo a la EMPRESA (ver fecha), EL CLIENTE manifiesta dejar el producto abonado y autoriza a ELECTRONICA
                      DIGITAL HUILA a vender el artículo por el valor que de la suma de la revisión, la reparación, los repuestos, la mano de obra, el bodegaje ($1000 pesos diarios), la papelería e impuestos y con este valor saldar a la EMPRESA de los gastos incurridos
                      en la prestación del servicio solicitado por el CLIENTE.
                      - La garantía por reparación es de tres (3) meses (solo por las partes reparadas, especificadas eb esta solicitud de servicio.). <br>
                      - La EMPRESA no se hace responsable del artículo en casos especiales como: Vandalismo, terremotos,inundaciones o cualquier factor que afecte el artículo, en hechos fortuitos ajenos a LA EMPRESA. <br>
                      - La recepción del articulo no implica un diagnostico defenitivo del daño o falla del mismo ni del total de la reparación.<br>
                      - La presentación de este documento es requisito indispensable para la entrega del artículo.<br>
                  </p>
              </td>
            </tr>
            <tr>
                <td>
                    <p class="politica">
                        - La garantía por reparación es de tres (3) meses (solo por las partes reparadas, especificadas eb esta solicitud de servicio.). <br>
                        - La EMPRESA no se hace responsable del artículo en casos especiales como: Vandalismo, <br> terremotos,inundaciones o cualquier factor que afecte el artículo, en hechos fortuitos ajenos a LA EMPRESA. <br>
                        - La recepción del articulo no implica un diagnostico defenitivo del daño o falla del mismo ni del total de la reparación.<br>
                        - La presentación de este documento es requisito indispensable para la entrega del artículo.<br>
                    </p>
                </td>
                <td>
                    <p class="firmas">
                        <br>
                        Acepto:&nbsp;&nbsp;&nbsp; &nbsp;     _________________________________ <br><br>
                        Firma:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;     _________________________________<br><br>
                        C.C/NIT:&nbsp; _________________________________<br>
                    </p>
                </td>
            </tr>
          </table>
      </div>

      <div class="desprendible">
              <div class="tab1">
                <table class="tabla_desprendible">
                    <tbody>
                      <tr>
                        <td colspan="2" >
                          <span style="padding:0px; margin:0px">
                            <label >Orden:</label>
                            <label style="font-size:18px"><strong>{{$orden->orden}}</strong></label>
                          </span>
                        </td>
                      </tr>
                      <tr>
                        <td colspan="2">
                            <label >Fecha de entrada: </label>{{$orden->fecha_entrada}}
                        </td>
                      </tr>
                      <tr>
                        <td colspan="2" class="">
                            <label class="">Nombre: </label>{{$orden->nombre}}
                        </td>
                      </tr>
                      <tr>
                        <td colspan="" class="">
                            <label class="">Modelo: </label>{{$orden->modelo}}
                        </td>
                      </tr>
                      <tr>
                        <td colspan="" class="">
                            <label class="">Serie: </label>{{$orden->serie}}
                        </td>
                      </tr>
                      <tr class="">
                          <td colspan="2" >
                              <label >Síntoma: </label>{{$orden->sintoma}}
                          </td>
                      </tr>
                      <tr class="">
                          <td colspan="2" >
                              <label >Accesorios: </label>{{$orden->accesorios}}
                          </td>
                      </tr>
                      <tr class="cobrable">
                          <td colspan="2" >
                              COBRABLE
                          </td>
                      </tr>
                    </tbody>
                </table>
              </div>

              <div class="tab2">
                <table class="tabla_desprendible">
                    <tbody>
                      <tr>
                        <td colspan="6">
                          <span style="padding:0px; margin:0px">
                            <label >Solicitud de servicio: </label>
                            <label style="font-size:18px"><strong>{{$orden->orden}}</strong></label>
                          </span>
                        </td>
                      </tr>
                      <tr>
                        <td colspan="3">
                            <label >Fecha de entrada: </label>{{$orden->fecha_entrada}}
                        </td>
                        <td colspan="3" >
                            <label >Fecha de compra: </label>{{$orden->fecha_compra}}
                        </td>
                      </tr>
                      <tr>
                        <td colspan="3" class="" >
                            <label class="">Nombre: </label>{{$orden->nombre}}
                        </td>
                        <td colspan="3" class="">
                            <label class="">Cedula: </label>{{$orden->id_doc}}
                        </td>
                      </tr>
                      <tr>
                        <td colspan="2" class="" >
                            <label class="">Celular: </label>{{$orden->celular}}
                        </td>
                        <td colspan="2" class="" >
                            <label class="">Teléfono 1: </label>{{$orden->telcasa}}
                        </td>
                        <td colspan="2" class="" >
                            <label class="">Teléfono 2: </label>{{$orden->teltrabajo}}
                        </td>
                      </tr>
                      <tr>
                        <td colspan="2" class="">
                            <label class="">Modelo: </label>{{$orden->modelo}}
                        </td>
                        <td colspan="2" class="" >
                            <label class="">Serie: </label>{{$orden->serie}}
                        </td>
                        <td colspan="2">
                            <label >Artículo: </label>{{$orden->articulo}}
                        </td>
                      </tr>
                      <tr class="">
                          <td colspan="6" >
                              <label >Síntoma: </label>{{$orden->sintoma}}
                          </td>
                      </tr>
                      <tr class="">
                          <td colspan="6" >
                              <label >Accesorios: </label>{{$orden->accesorios}}
                          </td>
                      </tr>
                      <tr class="cobrable">
                          <td colspan="6" >
                              COBRABLE
                          </td>
                      </tr>
                    </tbody>
                </table>
              </div>
      </div>
    </body>
</html>
