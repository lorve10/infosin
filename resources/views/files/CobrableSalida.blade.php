<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ public_path('css/pdfCobraSalida.css') }}" type="text/css">
    <title>COBRABLE SALIDA
      @if($orden->marca)
        {{$orden->marca}}
      @endif
    </title>
    <style>

    </style>
  </head>
  <body>
      <div class="container">
        <table class="encabezado">
            <tr>
                <td class="td1">
                      <div class="contenedor_logo">
                        <img src="{{ public_path('images/marcas.jpg') }}" width="200px" height="80px" class="img"/>
                      </div>
                </td>
                <td class="info_empresa">
                  <div class="">
                    <span class="h2">INSOFIN</span>
                    <p >ELECTRONICA DIGITAL DEL HUILA S.A.S <br>
                      Calle 22 No.6-13 PBX: 8759286 - 8755511 <br>
                      Cel: 3174341364 Neiva - Huila
                    </p>
                  </div>
                </td>

                <td  class="info_factura">
                  <div>
                    <div class="round">
                      <span class="h3">Información</span>
                      <p>Fecha: {{ date('Y-m-d ') }} </p>
                      <p>Hora: {{ date('h:i:s A') }}</p>
                      <span>
                        <label >Solicitud de Servicio:</label>
                        <label style="font-size:18px"><strong>{{$orden->orden}}</strong></label>
                      </span>
                    </div>
                  </div>
                </td>
            </tr>
        </table>

        <table class="tabla">
            <tbody>
              <tr>
                <td colspan="1" style="width:24%">
                    <label >Fecha de entrada: </label>{{$orden->fecha_entrada}}
                </td>
                <td colspan="1" style="width:20%">
                    <label >Articulo: </label>{{$orden->articulo}}
                </td>
                <td colspan="1" style="width:18%">
                    <label >Marca: </label>{{$orden->marca}}
                </td>
                <td colspan="1" style="width:18% ">
                    <label>Modelo: </label>{{$orden->modelo}}
                </td>
                <td colspan="1" style="width:20% ">
                    <label >Serie: </label>{{$orden->serie}}
                </td>
              </tr>
              <tr>
                <td colspan="5" class="">
                    <label class="">Estado físico: </label>{{$orden->estado_fisico}}
                </td>
              </tr>
              <tr>
                <td colspan="5" class="">
                    <label class="">Síntoma: </label>{{$orden->sintoma}}
                </td>
              </tr>
              <tr class="">
                  <td colspan="3" >
                      <label >Accesorios: </label>{{$orden->accesorios}}
                  </td>
                  <td colspan="2" >
                      <label >Email: </label>{{$orden->email}}
                  </td>
              </tr>
              <tr class="">
                  <td colspan="3">
                      <label>Nombre: </label>{{$orden->nombre}}
                  </td>
                  <td colspan="1" class="border px-2">
                      <label >Cédula: </label>{{$orden->id_doc}}
                  </td>
                  <td colspan="1">
                      <label >Celular: </label>{{$orden->celular}}
                  </td>
              </tr>
              <tr class="">
                <td colspan="3" >
                    <label >Dirección: </label>{{$orden->direccion}}
                </td>
                <td colspan="1" >
                    <label >Ciudad: </label>{{$orden->ciudad}}
                </td>
                <td colspan="1">
                    <label >Teléfono casa: </label>{{$orden->telcasa}}
                </td>
              </tr>
              <tr class="">
                  <td colspan="4"  >
                      <label >Observaciones de Ingreso: </label> {{$orden->obs_entrada}}
                  </td>
                  <td colspan="1" >

                    <label >Registró: </label>{{$orden->regrecibe}}

                  </td>
               </tr>
            </tbody>
        </table>

        <table class="tabla margin" >
            <tbody>
              <tr>
                <td colspan="4"><span class="reparacion">Detalle de Reparación</span></td>
              </tr>
              <tr>
                <td colspan="1" style="width:24%">
                    <label >Fecha de salida: </label>{{$orden->fecha_entrega}}
                </td>
                <td colspan="1" style="width:18% ">
                    <label>Precio: </label>
                </td>
                <td colspan="1" style="width:29%">
                    <label >Reparó: </label>{{$orden->tecasignado}}
                </td>
                <td colspan="1" style="width:29%">
                    <label >Entregó: </label>{{$orden->regentregaorden}}
                </td>
              </tr>
              <tr class="">
                  <td colspan="4"  >
                      <label >Observaciones de reparación: </label> {{$orden->obs_reparacion}}
                  </td>
               </tr>
            </tbody>
        </table>

        <div class="div_politica">
            <table class="tabla2">
              <tr>
                <td colspan="2">
                    <p class="politica">
                      - El precio de la reparación descrita en esta Solicitud de Servicio NO INCLUYE IVA, este documento no es una factura <br>
                      - EL CLIENTE manifiesta que el articulo es de uso doméstico.<br>
                      - La garantía es de tres (3) meses (solo por las partes reparadas, especificadas en esta solicitud de servicio). Este periodo pede ser menor de común acuerdo con EL CLIENTE y especificado en el
                      detalle de reparación.<br>
                      - La garantía no cubre daños causados por: mal manejo (en los que se encuentra el uso diferente al doméstico), exceso de voltaje en la red, rayos, condiciones ambientales diferentes (exceso de polvo,
                      temperatura y humedad alta, terremotos, inundaciones, etc.), hurto, equipos intervenidos por personal ajeno a la EMPRESA.<br>
                    </p>
                </td>
              </tr>
              <tr>
                <td colspan="2">
                  <p style="margin-top: 3px; text-align: center; font-weight:13px">Recibe a satisfacción</p>
                </td>
              </tr>
              <tr>
                <td>
                  <p class="firma2">
                    __________________________________ <br>
                    Firma y sello de Electrónica Digital</p>
                </td>
                  <td>
                      <p class="firmas">
                          Nombre:&nbsp;&nbsp;    _________________________________
                      </p>
                      <p class="firmas">
                          Firma:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;     _________________________________
                      </p>
                      <p class="firmas">
                          C.C/NIT:&nbsp; _________________________________
                      </p>

                  </td>
              </tr>
            </table>
        </div>
      </div>

      <div class="container2">
        <table class="encabezado">
            <tr>
                <td class="td1">
                      <div class="contenedor_logo">
                        <img src="{{ public_path('images/marcas.jpg') }}" width="200px" height="80px" class="img"/>
                      </div>
                </td>
                <td class="info_empresa">
                  <div class="">
                    <span class="h2">INSOFIN</span>
                    <p >Calle 22 No.6-13 PBX: 8759286 - 8755511 <br>
                                    Cel: 3174341364 Neiva - Huila <br>
                                    Email: electrodigitalneiva@gmail.com
                    </p>
                  </div>
                </td>

                <td  class="info_factura">
                  <div>
                    <div class="round">
                      <span class="h3">Información</span>
                      <p>Fecha: {{ date('Y-m-d ') }} </p>
                      <p>Hora: {{ date('H:i:s') }}</p>
                      <span>
                        <label >Solicitud de Servicio:</label>
                        <label style="font-size:18px"><strong>{{$orden->orden}}</strong></label>
                      </span>
                    </div>
                  </div>
                </td>
            </tr>
        </table>

        <table class="tabla">
            <tbody>
              <tr>
                <td colspan="1" style="width:24%">
                    <label >Fecha de entrada: </label>{{$orden->fecha_entrada}}
                </td>
                <td colspan="1" style="width:20%">
                    <label >Articulo: </label>{{$orden->articulo}}
                </td>
                <td colspan="1" style="width:18%">
                    <label >Marca: </label>{{$orden->marca}}
                </td>
                <td colspan="1" style="width:18% ">
                    <label>Modelo: </label>{{$orden->modelo}}
                </td>
                <td colspan="1" style="width:20% ">
                    <label >Serie: </label>{{$orden->serie}}
                </td>
              </tr>
              <tr>
                <td colspan="5" class="">
                    <label class="">Estado físico: </label>{{$orden->estado_fisico}}
                </td>
              </tr>
              <tr>
                <td colspan="5" class="">
                    <label class="">Síntoma: </label>{{$orden->sintoma}}
                </td>
              </tr>
              <tr class="">
                  <td colspan="3" >
                      <label >Accesorios: </label>{{$orden->accesorios}}
                  </td>
                  <td colspan="2" >
                      <label >Email: </label>{{$orden->email}}
                  </td>
              </tr>
              <tr class="">
                  <td colspan="3">
                      <label>Nombre: </label>{{$orden->nombre}}
                  </td>
                  <td colspan="1" class="border px-2">
                      <label >Cédula: </label>{{$orden->id_doc}}
                  </td>
                  <td colspan="1">
                      <label >Celular: </label>{{$orden->celular}}
                  </td>
              </tr>
              <tr class="">
                <td colspan="3" >
                    <label >Dirección: </label>{{$orden->direccion}}
                </td>
                <td colspan="1" >
                    <label >Ciudad: </label>{{$orden->ciudad}}
                </td>
                <td colspan="1">
                    <label >Teléfono casa: </label>{{$orden->telcasa}}
                </td>
              </tr>
              <tr class="">
                  <td colspan="4"  >
                      <label >Observaciones de Ingreso: </label> {{$orden->obs_entrada}}
                  </td>
                  <td colspan="1" >
                    @if($orden->regrecibe)
                    <label >Registró: </label>{{$orden->regrecibe}}
                    @endif
                  </td>
               </tr>
            </tbody>
        </table>
        <table class="tabla margin" >
            <tbody>
              <tr>
                <td colspan="4"><span class="reparacion">Detalle de Reparación</span></td>
              </tr>
              <tr>
                <td colspan="1" style="width:24%">
                    <label >Fecha de salida: </label>{{$orden->fecha_entrega}}
                </td>
                <td colspan="1" style="width:18% ">
                    <label>Precio: </label>
                </td>
                <td colspan="1" style="width:29%">
                    <label >Reparó: </label>{{$orden->tecasignado}}               </td>
                <td colspan="1" style="width:29%">
                    <label >Entregó: </label>{{$orden->regentregaorden}}
                </td>
              </tr>
              <tr class="">
                  <td colspan="4"  >
                      <label >Observaciones de reparación: </label> {{$orden->obs_reparacion}}
                  </td>
               </tr>
            </tbody>
        </table>
        <div class="div_politica">
            <table class="tabla2">
              <tr>
                <td colspan="2">
                    <p class="politica">
                      - El precio de la reparación descrita en esta Solicitud de Servicio NO INCLUYE IVA, este documento no es una factura <br>
                      - EL CLIENTE manifiesta que el articulo es de uso doméstico.<br>
                      - La garantía es de tres (3) meses (solo por las partes reparadas, especificadas en esta solicitud de servicio). Este periodo pede ser menor de común acuerdo con EL CLIENTE y especificado en el
                      detalle de reparación.<br>
                      - La garantía no cubre daños causados por: mal manejo (en los que se encuentra el uso diferente al doméstico), exceso de voltaje en la red, rayos, condiciones ambientales diferentes (exceso de polvo,
                      temperatura y humedad alta, terremotos, inundaciones, etc.), hurto, equipos intervenidos por personal ajeno a la EMPRESA.<br>
                    </p>
                </td>
              </tr>
              <tr>
                <td colspan="2">
                  <p style="margin-top: 3px; text-align: center; font-weight:13px">Recibe a satisfacción</p>
                </td>
              </tr>
              <tr>
                <td>
                  <p class="firma2">
                    __________________________________ <br>
                    Firma y sello de Electrónica Digital</p>
                </td>
                  <td>
                      <p class="firmas">
                          Nombre:&nbsp;&nbsp;    _________________________________
                      </p>
                      <p class="firmas">
                          Firma:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;     _________________________________
                      </p>
                      <p class="firmas">
                          C.C/NIT:&nbsp; _________________________________
                      </p>

                  </td>
              </tr>
            </table>
        </div>
      </div>


    </body>
</html>
