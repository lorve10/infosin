<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ public_path('css/pdfExtenSalida.css') }}">
    <title>LG SALIDA</title>
    <style>


    </style>

  </head>
  <body>
      <table class="encabezado ">
          <tr>
            <td >
              <p class="codigo" style="text-align: left;">UN50TU8000KXZL</p>
            </td>
            <td class="info_empresa" rowspan="2">
              <div class="">
                <span class="h2">INSOFIN</span>
                <p >ELECTRONICA DIGITAL DEL HUILA S.A.S <br>
                  Calle 22 No.6-13 PBX: 8759286 - 8755511 <br>
                  Cel: 3174341364 Neiva - Huila
                </p>
              </div>
            </td>
            <td >
              <p  class="codigo" style="text-align: right;">09P73CXR500243H</p>
            </td>
          </tr>
          <tr>
              <td class="td1">
                <div class="contenedor_logo">
                    <div class="">
                      <img src="" alt="">
                      <img src="{{ public_path('images/marcas.jpg') }}" width="200px" height="80px" class="img"/>
                    </div>
                </div>
              </td>
              <td  class="info_factura">
                <div>
                  <div class="round">
                    <span class="h3">Información</span>
                    <p><strong>Fecha: </strong>{{ date('Y-m-d ') }}</p>
                    <p><strong>Hora: </strong>{{ date('h:i:s A') }}</p>
                    <p><strong>Fecha de entrada: </strong>{{$orden->fecha_entrada}}</p>
                    <p><strong>Seguimiento: </strong></p>
                  </div>
                </div>
              </td>
          </tr>
      </table>

      <table class="tabla">
          <tbody>
            <tr class="">
                <td colspan="2">
                    <label>Nombre: </label>{{$orden->nombre}}
                </td>
                <td colspan="1" class="border px-2">
                    <label >Cédula: </label>{{$orden->id_doc}}
                </td>
                <td colspan="1" >
                    <label >Celular: </label>{{$orden->celular}}
                </td>
            </tr>
            <tr class="">
              <td colspan="2" >
                  <label >Dirección: </label>{{$orden->direccion}}
              </td>
              <td colspan="1" >
                  <label >Ciudad: </label>{{$orden->ciudad}}
              </td>
              <td colspan="1" >
                  <label >Teléfono residencia: </label>{{$orden->telcasa}}
              </td>
            <tr>
              <td colspan="1" style="width:30%">
                  <label >Fecha de entrada: </label>{{$orden->fecha_entrada}}
              </td>
              <td colspan="1" style="width:20%">
                  <label >Articulo: </label>{{$orden->articulo}}
              </td>
              <td colspan="1" style="width:20%">
                  <label >Marca: </label>{{$orden->marca}}
              </td>
              <td colspan="1" style="width:30%">
                  <label>Modelo: </label>{{$orden->modelo}}
              </td>
            </tr>
            <tr>
              <td colspan="1">
                  <label >Serie: </label>{{$orden->serie}}
              </td>
              <td colspan="3" class="">
                  <label class="">Estado físico: </label>{{$orden->estado_fisico}}
              </td>
            </tr>
            <tr class="">
                <td colspan="4" >
                    <label >Accesorios: </label>{{$orden->accesorios}}
                </td>
            </tr>
            <tr>
              <td colspan="1" style="width:30%">
                  <label >Fecha de compra: </label>{{$orden->fecha_compra}}
              </td>
              <td colspan="1" style="width:20%">
                  <label >Factura: </label>{{$orden->factura}}
              </td>
              <td colspan="2" style="width:20%">
                  <label >Distribuidor: </label>{{$orden->distribuidor}}
              </td>
            </tr>
            <tr>
              <td colspan="1" style="width:30%">
                  <label >Fecha de reparado: </label>{{$orden->fecha_reparacion}}
              </td>
              <td colspan="2" style="width:20%">
                  <label >Fecha de entrega: </label>{{$orden->fecha_entrega}}
              </td>
              <td colspan="1" style="width:20%">
                  <label >Código de bloque: </label>N/A
              </td>
            </tr>
            </tbody>
      </table>

      <table class="tabla margin" >
          <tbody>
            <tr >
              <td colspan="1"><span class="reparacion">Síntoma reportado</span></td>
            </tr>
            <tr style="background:#FBFBF9">
              <td colspan="1" style="width:24%">
                  {{$orden->sintoma}}
              </td>
            </tr>
          </tbody>
      </table>
      <table class="tabla margin" >
          <tbody>
            <tr>
              <td colspan="1"><span class="reparacion">Descripción de la falla</span></td>
            </tr>
            <tr style="background:#FBFBF9">
              <td colspan="1" style="width:24%">
                  {{$orden->desc_falla}}
              </td>
            </tr>
          </tbody>
      </table>
      <table class="tabla margin" >
          <tbody>
            <tr>
              <td colspan="1"><span class="reparacion">Descripción de la reparación</span></td>
            </tr>
            <tr style="background:#FBFBF9">
              <td colspan="1" style="width:24%">
                    {{$orden->desc_reparacion}}
              </td>
            </tr>
          </tbody>
      </table>

      <div class="div_politica">
          <table class="tabla2">
            <tr>
              <td colspan="2">
                  <p class="politica" style="text-align: center; margin-bottom:1px"><strong>CONSTANCIA DE RECIBO Y/O REPARACIÓN DE PRODUCTOS MARCA LG:</strong></p>
                  <p class="politica">
                    El consumidor con su firma acepta de manera expresa, libre y espontánea las condiciones que se aplicarán para la prestación del servicio técnico de los productos marca LG, las cuales están concordadas con Ley 1480 de 2011, el Decreto 735 de 2013 y los Decretos que regulen la materia contenidos en el certificado de garantía de su artículo.
                    <strong>CONDICIONES GENERALES PARA SERVICIOS EN GARANTÍA:</strong><br>
                    I. Declaración. El consumidor declara que se acoge a las politicas de prestación de servicios técnicos de LG, en particular: (l) al término de 2 meses de abandono del producto, la validez de 8 días calendario de la cotización del valor del servicio técnico
                    fuera de garantía, y la tarifa de bodegaje del producto correspondiente a $5.000 COP. También declara que se acoge a las políticas de tratamiento de datos personales de LG, y da su autorización expresa para el tratamiento de sus datos personales.<br>
                    II. Términos y condiciones sobre la prestación del servicio técnico: LG a través de sus Centros de Servicio Autorizados prestaran los servicios técnicos sobre los electrodomésticos de la marca LG Electronics, así: 1)Al momento de solicitar la prestación del servicio técnico dentro o fuera de la garantía anunciada por LG, se expedira una orden de servicio donde se dejara la fecha de recepción del bien, el nombre del propietario o de quien hace su entrega, dirección, teléfono, identificación del bien, clase de servicio, las sumas que se abonan como parte del precio, el término de la garantía que se otorga y de ser posible el valor del servicio. 2)Si se deja el producto en cutodia del CSA, se verifican siempre las condiciones físicas y cosméticas en las cuales es entregado el bien, dejando como constancia la firma y nombre del solicitante del servicio. 3)El valor de la cotización por la prestacion del servicio técnico fuera de garantia por concepto de mano de obra o suministros de la(s) partes(s) a la(s) que haya(n) a lugar, tendrá una vigencia de 8 días calendario contada a partir de su expedición. Por lo tanto, si se solicita el valor de una nueva cotización, no se garantiza el precio dado, ya que estará sujeta sin previo aviso a las variaciones de precios e impuestos decretados por el Gobierno Nacional. 4)Transcurrido 1 mes a partir de la fecha prevista para la devolución del bien, no es retirado por el propietario o de quien hizo su entrega, se le requerirá por el CSA por el medio de notificación registrado en el presente documento, para que lo retire dentro de un término máximo de 2 meses siguientes a su notificación. Vencido este término y no de haberse retirado el producto del CSA, se entenderá legalmente que lo abandona y el prestador del servicio (LG Electronics Colombia Ltda) adquirirá el derecho real de dominio sobre el bien abandonado. 5)Si se quiere obtener nuevamente el derecho real de dominio por el propietarioo de quien hizo la entrega del bien abandonado, se deja a
                    discrecionalidad del prestador del servicio técnico LG Electronics Colombia Ltda la devolución del bien abandonado. Motivo por el cual si se acepta la devolución del bien abandonado y siempre y cuando se cuente con su existencia: a) No se responderá
                    por las condiciciones físicas, estéticas y de funcionalidad en las cuales sea etregado. b)Se deberá cancelar un valor de $5.000 COP, por cada día de abandono. c)No se otorgara ninguna garantía por la prestación del servicio técnico sobre el bien
                    abandonado. 6)LG otorgara una garantía por la prestación del servicio técnico fuera de garantía de 3 meses siguientes a la entrega formal del bien, por concepto de mano de obra y sobre el suministro de la(s) parte(s) a la(s) que haya(n) lugar.<br>
                    III. Tratamiento de datos personales. 1) En cumplimineto con la normatividad vigente de tratameitno de datos personales (habeas data/ley 1581/12 y decretos reglamentarios), en mi calidad de consumidor o solicitante del servicio técnico autorizo de
                    manera libre, previa, expresa y voluntaria a LG Electronics Colombia Ltda y/o al centro de servicio Autorizado, para que precese, recopilen, confirmen, tramiten y usen mis datos personales para la prestación del servicio técnico, como también para
                    cualquier reclamación que se presente sobre el producto dado en servicio técnico. 2)El consumidor o solicitante del servicio técnico declara que suministra la indormación veraz sobre sus datos personales. Debido a lo anterior, LG presume la veracidad
                    de la información suministrada por el consumidoro solicitante del servicio técnico, debido a lo anterior, LG no asumira la obligación de verificar, la identidad del consumidor o solicitante del servicio técnico, ni la veracidad, vigencia, suficiencia y
                    autencidad autencidad de los datos que proporcionen. Por lo tanto, LG no asume responsabilidad por daños y/o perjuicios de toda naturaleza que pudieran tener origen en la falta de veracidad, vigencia, suficiencia o autencidad de la información,
                    incluyendo daños y perjuicios que puedan deberse a la homonimia o a la sumplantación de la identidad. 3)Que el titular de los datos personales declara que ha sido informado de la política de manejo de datos personales de LG y que puede consultarla
                    en http://www.lg.com/co/privacy 4)El titular de los datos de la información personal tiene derecho a conocer, rectificar, actualizar y revocar la autorización o solicitar supimir los datos suministrados, como también conocer los usos que se le ha dado a la información personal siempre y cuando no lo impida una disposición legal, contactándose al correo electrónico pqrcolombia@lge.com y datospersonales.cb@lgepartner.com
                  </p>
              </td>
            </tr>
          </table>
      </div>
      <div class="" style="width:100%; margin-top:1px">
        <table class="tabla margin ">
          <tr>
            <td rowspan="2" style="width:20%; font-size:12px; padding:1px">Consumidor autoriza revisión <br>
              o reparacón del producto.</td>
            <td style="width:5%; font-size:12px; padding:1px; text-align:center">SI</td>
            <td rowspan="2" style="width:70%; font-size:12px; padding:1px">Observaciones: </td>
          </tr>
          <tr>
            <td style="width:5%;font-size:12px; padding:1px; text-align:center">NO</td>
          </tr>
        </table>
      </div>

      <table class="tabla" style="margin-top:3px">
            <tr class="" style="background:#FBFBF9">
                <td colspan="1">
                    <label>Tec. que recibió: </label>{{$orden->tecrecibe}}
                </td>
                <td colspan="1" class="border px-2">
                    <label >Tec. que reparó: </label>{{$orden->tecasignado}}
                </td>
            </tr>
        </table>

      <div class="" >
        <table class=" margin " style="width:100%; padding-left:8px">
          <tr>
            <td style="text-align: left;">
                <p class="firmas">
                    <br>
                    Técnique entrega:&nbsp;&nbsp;&nbsp;     _________________________________ <br><br>

                </p>
                <p class="firmas" style="padding-left:52px">
                    Firma:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    _________________________________<br><br>
                </p>
                <p class="firmas" style="padding-left:43px">
                    C.C/NIT:&nbsp;&nbsp;&nbsp;  _________________________________
                </p>
            </td>
            <td  style="text-align: right;padding-right:8px">
                <p class="firmas">
                    <br>
                    Recibe a Satisfacción:&nbsp;&nbsp;&nbsp; &nbsp;     _________________________________ <br><br>
                    Firma:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;     _________________________________<br><br>
                    C.C/NIT:&nbsp;&nbsp;&nbsp; _________________________________<br>
                </p>
            </td>
          </tr>
        </table>
      </div>


    </body>
</html>
