@extends('layouts.Base')

@section('title', 'Home')

@section('content')
  <!DOCTYPE html>
  <html lang="en" dir="ltr">
    <head>
      <meta charset="utf-8">
      <link href="{{ asset('css/app.css') }}"
      rel="stylesheet">
      <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
      <title></title>
    </head>
    <body>
      @csrf
          <div id="IndexPerson">
          </div>
    </body>
  </html>

@endsection
