@extends('layouts.Base')

@section('title', 'Home')

@section('content')


<html>
      <head>
        <title>Welcome</title>
        <meta charset="utf-8">
        <meta name="viewport" content="initial-scale=1.0"/>
        <link href="{{ asset('css/app.css') }}"
        rel="stylesheet">
        <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
      </head>

      <body>
        <div style="background:white; padding:30px; border-radius: 5px;" id="">
          <h2 style="text-align: center; font-size:50px; font-weight: bold; color:#001250 ">
            @if(Auth::user()->genero == 'F')
             Bienvenida
            @elseif(Auth::user()->genero == 'M')
             Bienvenido
            @endif a INSOFIN
          </h2>
          <h2 style="text-align: center; font-size:15px; color:#140463 ">
            Sistema integral de información de Electrónica digital
          </h2>
          <br>
          <p style="text-align: center; font-size:30px;font-weight: bold ">{{ Auth::user()->name}}</p>
          <br>

          @if(Auth::user()->position == 1)
            <div id="Welcome"></div>
            <!-- <div style="display: flex; justify-content: center;">
              <img src="{{ asset('images/insofinLogoNombre.png') }}" width="300px" height="400px" class="img"/>
            </div> -->
            </div>
          @else
            <div></div>
              <div style="display: flex; justify-content: center;">
                <img src="{{ asset('images/insofinLogoNombre.png') }}" width="300px" height="400px" class="img"/>
              </div>
            </div>
          @endif


        <script src=/js/app.js>
          $(document).ready(function(){
          $('#welcome').addClass('active');
          })
        </script>
      </body>

</html>

<!DOCTYPE html>

@endsection
