@extends('layouts.Base')

@section('title','Home')

@section('content')

<html>
    <head>
      <title>Clientes</title>

      <meta charset="UTF-8" />
      <meta name="viewport" content="initial-scale=1.0" />
      <link href="{{ asset('css/app.css') }}" rel="stylesheet">
      <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
   </head>

   <body class="">
     @csrf
      <div id="Customers"></div>
    </body>

    <script>
      $(document).ready(function(){
      $('#cliente').addClass('active');
      })
    </script>

</html>
<!DOCTYPE html>


@endsection
