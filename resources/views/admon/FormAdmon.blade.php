@extends('layouts.Base')

@section('title','Home')

@section('content')

<!DOCTYPE html>
<div>
  <head>
    <meta charset="utf-8">
    <title></title>
    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
     <link href="{{ asset('css/app.css') }}" rel="stylesheet">

  </head>
  <body>
    @csrf
    <div id = "Form">
    </div>
  </body>

</div>
<script>

$(document).ready(function(){
$('#Form').addClass('active');

})
</script>

@endsection
