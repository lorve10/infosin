@extends('layouts.Base')

@section('title', 'Home')

@section('content')

<html>
      <head>
        <title>Ordenes</title>
        <meta charset="utf-8">
        <meta name="viewport" content="initial-scale=1.0"/>
        <link href="{{ asset('css/app.css') }}"
        rel="stylesheet">
        <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
      </head>

      <body>
        @csrf
        <div id="Orders">

        </div>
        <input type="hidden" id="position" value={{Auth::user()->position}} />
      </body>
      <script>

        $(document).ready(function(){
        $('#ordenes').addClass('active');
        })
      </script>

</html>
<!DOCTYPE html>

@endsection
