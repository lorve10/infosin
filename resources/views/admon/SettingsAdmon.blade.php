@extends('layouts.Base')

@section('title', 'Home')

@section('content')

@if(Auth::user()->position == 1)
<html>
      <head>
        <title>Usuarios</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-
        width, initial-scale=1.0"/>
        <link href="{{ asset('css/app.css') }}"
        rel="stylesheet">
        <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
      </head>

      <body>
        @csrf
        <div id="Settings"></div>

      </body>

</html>
@else
  <h1>Error no tienes permisos administrador para entrar a esta ruta....</h1>
@endif
<!DOCTYPE html>

@endsection
