import React,{ useState, useEffect} from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import Swal from 'sweetalert2';
import api from '../../Servis/Api';
import util from "../../means/util";
import Pagination from '../../means/paginate';


function Cargos(props) {
  const [ cargo, setCargo ] = useState();
  const [ listCargos, setListCargos ] = useState([]);
  const [ listCargosBack, setListCargosBack ] = useState([]);
  const [ currentPage, setCurrentPage] = useState(1)
  const [ perPage, setPerPage] = useState(6)

  useEffect(()=>{
    obtain_cargos()
  },[])

  // Funcion para obtener los cargos
  const obtain_cargos = () => {
    api.get('admon/getCargo').then(response =>{
      setListCargos(response.data)
      setListCargosBack(response.data)
    })
  }

  const updateCurrentPage = async (number) => {
    await setListCargos([])
    await setCurrentPage(number)
    const dataNew = util.paginate(listCargosBack,number,perPage)
    await setListCargos(dataNew)
  }

  //Funcion para guardar los datos
  const saveCargos = async () => {
    var message = ''
    var error = false
    if( cargo == null){
      error = true
      message = "Debes agregar un cargo"
    }
    if (error){
      util.MessageError(message)
    }
    else {
      const data = new FormData()
      data.append('cargo', cargo)
      api.post('admon/saveCargo',data).then(response=>{
        if(props.id == null){
          util.MessageSuccess('Se guardó con exito');
          obtain_cargos()
          setCargo('')
        }
        else{
          util.MessageSuccess('Editado con exito');
        }
      })
    }
  }

  const deleted = (value) => {
    Swal.fire({
    title: 'Seguro que deseas elimiar este elemento?',
    icon: 'question',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonText:'No',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Si'
    }).then((result) => {
      if (result.isConfirmed) {
        const data = new FormData()
        data.append('id', value)
        api.post('admon/deletedCargos', data).then(response =>{
          if (response.data == 1 ) {
            Swal.fire(
              'Deleted!',
              'Eliminado correctamente.',
              'success'
            )
            obtain_cargos()
          }
          else if (response.data == 2){
            util.MessageError("No puede eliminar este cargo porque se está asociada a un Usuario")
          }
        })
      }
      else{
        util.MessageError("No se eliminó")
      }
    })
  }

  return(
    <div className="container">
      <div className="flex mb-3">
        {
          props.id > 0 ?
          <h2 className="text-gray-800 text-3xl font-bold ml-3 mt-3">Editar cargo</h2>
          :
          <h2 className="text-gray-800 text-3xl font-bold ml-3 mt-3">Registrar cargo</h2>
        }
      </div>

      <div className="container col-12 flex flex-col md:flex-row justify-between">
          <div className="w-full md:w-2/5">
            <label className="block text-gray-700 text-sm font-bold mb-2"
           for="username">
                Cargos
            </label>
            <input className="shadow apparence-none border rounded w-full py-2 px-2 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            type="text" placeholder="Cargos"
            value={cargo}
            onChange={(e)=>setCargo(e.target.value)}
             />

             <div className="mb-3 mt-4 flex flex-col sm:flex-row  justify-between">
                 <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold my-2 py-2 px-4 rounded-full" onClick={()=>props.goBack()} >
                 Regresar
                 </button>
                 <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold my-2 py-2 px-4 rounded-full" onClick={()=>saveCargos()}>
                 Guardar
                 </button>
             </div>

          </div>

          <div className="my-3">
                <div class="w-full ">
                  <div class="w-full ">
                      <table className="table-auto rounded-lg shadow-lg">
                        <thead className="table-auto">
                          <tr class="text-sm font-semibold tracking-wide text-center text-gray-900 bg-blue-400 uppercase border-b border-gray-600">
                            <th class="px-4 py-3">Item</th>
                            <th class="px-4 py-3">Marca</th>
                            <th class="px-4 py-3">Eliminar</th>
                          </tr>
                        </thead>
                        <tbody class="bg-white">
                          {
                            listCargos.map((item, index) =>{
                              return(
                                  <tr class="text-gray-700">
                                    <td class="px-4 py-3 text-sm border text-center">{index+1}</td>
                                    <td class="px-4 py-3 text-sm border text-center">{item.cargo}</td>
                                    <td class="px-5 py-2 text-sm border text-center">
                                      <span class="material-icons" onClick={()=>deleted(item.id)}>
                                      deleted
                                      </span>
                                    </td>
                                  </tr>
                              );
                            })
                          }
                        </tbody>
                      </table>
                      <div className="d-flex col-md-12 col-12 justify-content-end mt-2">
                        <Pagination currentPage={currentPage} perPage={perPage} countdata={listCargosBack.length} updateCurrentPage={(number)=>updateCurrentPage(number)}/>
                      </div>
                  </div>
                </div>
          </div>


      </div>
    </div>

  );


} export default Cargos;
