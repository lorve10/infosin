import React,{ useState, useEffect} from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import Swal from 'sweetalert2';
import api from '../../Servis/Api';
import util from "../../means/util";
import Pagination from '../../means/paginate';

function Linea(props) {

  const [ linea, setLinea ] = useState(null);
  const [ listLinea, setListLinea ] = useState([]);
  const [ listLineaBack, setListLineaBack ] = useState([]);
  const [ currentPage, setCurrentPage] = useState(1)
  const [ perPage, setPerPage] = useState(6)

  useEffect(()=>{
      obtain_lineas()
  },[])

  const obtain_lineas = () =>{
    api.get('admon/getLinea').then(response =>{
      setListLinea(response.data)
      setListLineaBack(response.data)
      })
  }

  const updateCurrentPage = async (number) => {
    await setLinea([])
    await setCurrentPage(number)
    const dataNew = util.paginate(listLineaBack,number,perPage)
    await setLinea(dataNew)
  }


  ///funcion para guardar datos
  const saveLinea = async () => {
    var message = ''
    var error = false

    if( linea == null){
      error = true
      message = "Debes agregar una linea"
    }
    if (error){
      util.MessageError(message)
    }
    else {
      const data = new FormData()
      data.append('linea', linea)
      api.post('admon/saveLinea',data).then(response=>{
        if(props.id == null){
          util.MessageSuccess('Se guardó con éxito');
          obtain_lineas()
          setLinea('')
        }
        else{
          util.MessageSuccess('Editado con éxito');
        }
      })
    }
  }

  const deleted = (value) => {
    Swal.fire({
    title: 'Seguro que deseas elimiar este elemento?',
    icon: 'question',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonText:'No',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Si'
    }).then((result) => {
      if (result.isConfirmed) {
        const data = new FormData()
        data.append('id', value)
        api.post('admon/deletedLinea', data).then(response =>{
          if (response.data == 1 ) {
            Swal.fire(
              'Deleted!',
              'Eliminado correctamente.',
              'success'
            )
            obtain_lineas()
          }
          else if (response.data == 2){
            util.MessageError("No puede eliminar esta linea porque se está asociada a un dispositivo")
          }
        })
      }
      else{
        util.MessageError("No se eliminó")
      }
    })
  }


  return(
    <div  className="container">

      <div className="flex mb-3">
        {
          props.id > 0 ?
          <h2 className="text-gray-800 text-3xl font-bold ml-3 mt-3">Editar linea</h2>
          :
          <h2 className="text-gray-800 text-3xl font-bold ml-3 mt-3">Registrar linea</h2>
        }
      </div>

      <div className="container col-12 flex flex-col md:flex-row justify-between">

            <div className="w-full md:w-2/5">
               <label className="block text-gray-700 text-sm font-bold mb-2" for="username">
                   Linea
               </label>
               <input className="shadow appearance-none border rounded w-full py-2 px-2 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                 type="text" placeholder="Linea"
                 value = {linea}
                 onChange = {(e)=>setLinea(e.target.value)}
                 />


                 <div className="mb-3 mt-4 flex flex-col sm:flex-row justify-between">
                     <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold my-2 py-2 px-4 rounded-full" onClick={()=>props.goBack()}>
                       Regresar
                     </button>
                     <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold my-2 py-2 px-4 rounded-full " onClick={()=>saveLinea()} >
                       Guardar
                     </button>
                 </div>
            </div>


            <div className="my-3">
                  <div class="w-full ">
                    <div class="w-full ">
                        <table className="table-auto rounded-lg shadow-lg">
                          <thead className="table-auto">
                            <tr class="text-sm font-semibold tracking-wide text-center text-gray-900 bg-blue-400 uppercase border-b border-gray-600">
                              <th class="px-4 py-3">Item</th>
                              <th class="px-4 py-3">Linea</th>
                              <th class="px-4 py-3">Eliminar</th>
                            </tr>
                          </thead>
                          <tbody class="bg-white">
                            {
                              listLinea.map((item, index) =>{
                                return(
                                    <tr class="text-gray-700">
                                      <td class="px-4 py-3 text-sm border text-center">{index+1}</td>
                                      <td class="px-4 py-3 text-sm border text-center">{item.line}</td>
                                      <td class="px-2 py-2 text-sm border text-center">
                                        <span class="material-icons" onClick={()=>deleted(item.id)}>
                                        delete
                                        </span>
                                      </td>
                                    </tr>
                                );
                              })
                            }
                          </tbody>
                        </table>
                        <div className="d-flex col-md-12 col-12 justify-content-end mt-2">
                          <Pagination currentPage={currentPage} perPage={perPage} countdata={listLineaBack.length} updateCurrentPage={(number)=>updateCurrentPage(number)}/>
                        </div>

                    </div>
                  </div>

            </div>


      </div>




   </div>
  )
}

export default Linea;
