  import React,{ useState, useEffect} from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import Swal from 'sweetalert2';
import api from '../../Servis/Api';
import util from "../../means/util";
import Pagination from '../../means/paginate';
import Compressor from 'compressorjs';
import '../../../../css/app.css';
import Select from 'react-select';
import makeAnimated from 'react-select/animated';
function Brands(props) {

  const [ marca, setMarca ] = useState('');
  const [ urlImg, setUrlImg ] = useState(null)
  const [ img, setImg ] = useState({})
  const [ listMarcas, setListMarcas ] = useState([]);
  //// variables paginador
  const [ listMarcasBack, setListMarcasBack ] = useState([]);
  const [currentPage, setCurrentPage] = useState(1)
  const [perPage, setPerPage] = useState(6)

  /// variables tipo
  const [ type, setType ] = useState(0)
  const [ typeSelect, setTypeSelect ] = useState([
    {
    value:'1',
    label:'Marca',
    },
    {
    value:'2',
    label:'Aseguradora',
    },
    {
    value:'3',
    label:'Cobrable',
    }
  ])
  const [ animatedComponents, setAnimatedComponents] = useState(makeAnimated);


  useEffect(()=>{
    obtain_marcas()
  },[])

  //Funcion para obtener marcas
  const obtain_marcas = () =>{
    api.get('admon/get').then(response =>{
      setListMarcas(response.data)
      setListMarcasBack(response.data)
      })
  }
  const updateCurrentPage = async (number) => {
    await setListMarcas([])
    await setCurrentPage(number)
    const dataNew = util.paginate(listMarcasBack,number,perPage)
    await setListMarcas(dataNew)
  }


  // Función para guardar
  const saveMarcas = async () => {
    var message = ''
    var error = false
    if( type.value == null || type.value == 0){
      error = true
      message = "Debes agregar una tipo"
    }

    if( marca == null){
      error = true
      message = "Debes agregar una marca"
    }

    if (error){
      util.MessageError(message)
    }
    else {
      const data = new FormData()
      data.append('marca', marca)
      data.append('tipo', type.value)
      data.append('file', img)
      api.post('admon/saveMarca',data).then(response=>{
        if(props.id == null){
          setMarca('')
          setImg({})
          setUrlImg(null)
          setType(0)
          util.MessageSuccess('Se guardó con éxito');
          obtain_marcas()
        }
        else{
          setMarca('')
          setImg({})
          setUrlImg(null)
          setType(0)
          loadImage(null);
          util.MessageSuccess('Editado con éxito');
        }
      })

    }

  }
  ///funcion para comprimir la imagen
  const loadImage = (event) => {
    const file = event.target.files[0];
    if (file.type == "image/png" || file.type == "image/jpg" || file.type == "image/jpeg"){
      if (file.size <= 10000000) {
        new Compressor(file, {
          quality: 0.6,
          success(result) {
            var file = new File ([result], result.name, {type:result.type});
            setImg(file)
            let reader = new FileReader();
            reader.onload = e => {
              setUrlImg(e.target.result)
            };
            reader.readAsDataURL(file);
          }
        })
      }
      else{
        util.MessageError("Imagen muy pesada");
      }
    }
    else{
      util.MessageError("Formato no válido: Recuerda que debe ser un formato PNG, JPG O JPEG");
    }

  }


  const deleted = (value) =>{
    Swal.fire({
    title: 'Seguro que deseas elimiar este elemento?',
    icon: 'question',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonText:'No',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Si'
    }).then((result) => {
      if (result.isConfirmed) {
        const data = new FormData()
        data.append('id', value)
        api.post('admon/deleted', data).then(response =>{
          if(response.data == 1){
            Swal.fire(
              'Deleted!',
              'Eliminado correctamente.',
              'success'
            )
            obtain_marcas()
          }
          else if (response.data == 2) {
            util.MessageError("No puedes eliminar esta marca por que esta asociado a un dispositivo")
          }

        })
      }
      else {
        util.MessageError("No se eliminó")
      }
    })

  }

  return(
    <div  className="container">

      <div className="flex mb-3">
        {
          props.id > 0 ?
          <h2 className="text-gray-800 text-3xl font-bold ml-3 mt-3">Editar </h2>
          :
          <h2 className="text-gray-800 text-3xl font-bold ml-3 mt-3">Registrar </h2>
        }
      </div>

      <div className="container col-12 flex flex-col md:flex-row justify-between">

            <div className="w-full md:w-2/5">

                 <label className="block text-gray-700 text-sm font-bold mb-2" >
                      Tipo
                 </label>
                 <Select
                  className="shadow block text-gray-700 text-sm font-bold mb-2"
                  value={type}
                  closeMenuOnSelect={true}
                  components={animatedComponents}
                  options={typeSelect}
                  onChange={(e)=>setType(e)}
                  placeholder = "Seleccionar tipo"
                  />

               <label className="block text-gray-700 text-sm font-bold mb-2" >
                    Nombre
              </label>
               <input className="shadow appearance-none border rounded w-full py-2 px-2 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                 type="text" placeholder="Nombre"
                 value = {marca}
                 onChange = {(e)=>setMarca(e.target.value)}
                 />

               <label className="block text-gray-700 text-sm font-bold mb-2 mt-2" >
                   Logo
               </label>
               <div className="d-flex mt-2">
                 <div className="">
                   <div className="  ">
                     <label htmlFor="activa">
                       {
                         urlImg == null ?
                         <div className="material-icons border radius-10 muestra">
                           <div className="img-foto">camera_alt</div>
                           </div>
                           :<img className="imagen"  src = {urlImg}/>
                         }
                       </label>
                     </div>
                     <div className="justify-Content-center flie">
                       <input onChange = {(e)=>loadImage(e)} className="file" id = "activa" type="file"/>
                     </div>

                   </div>
                 </div>


                 <div className="mb-3 mt-4 flex flex-row justify-between">
                     <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold my-2 py-2 px-4 rounded-full" onClick={()=>props.goBack()}>
                       Regresar
                     </button>
                     <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold my-2 py-2 px-4 rounded-full " onClick={()=>saveMarcas()} >
                       Guardar
                     </button>
                 </div>
            </div>


            <div className="my-3">
                  <div className="w-full ">
                    <div className="w-full ">
                        <table className="table-auto rounded-lg shadow-lg">
                          <thead className="table-auto">
                            <tr className="text-sm font-semibold tracking-wide text-center text-gray-900 bg-blue-400 uppercase border-b border-gray-600">
                              <th className="px-4 py-3">Prefijo</th>
                              <th className="px-4 py-3">Tipo</th>
                              <th className="px-4 py-3">Nombre</th>
                              <th className="px-4 py-3">Eliminar</th>
                            </tr>
                          </thead>
                          <tbody className="bg-white">
                            {
                              listMarcas.map((item, index) =>{
                                return(
                                    <tr className="text-gray-700" key={index}>
                                      <td className="px-2 py-3 text-sm text-center border">{item.id}</td>
                                      <td className="px-2 py-3 text-sm text-center border">{item.tipo == 1 ?
                                           "Marca": item.tipo == 2? "Aseguradora": item.tipo==3? "cobrable": null}</td>
                                      <td className="px-2 py-3 text-sm text-center border">{item.marc}</td>
                                      <td className="px-2 py-2 text-sm border text-center">
                                        <span className="material-icons" onClick={()=>deleted(item.id)}>
                                        delete
                                        </span>
                                      </td>
                                    </tr>
                                );
                              })
                            }
                          </tbody>
                        </table>
                        <div className="d-flex col-md-12 col-12 justify-content-end mt-2">
                          <Pagination currentPage={currentPage} perPage={perPage} countdata={listMarcasBack.length} updateCurrentPage={(number)=>updateCurrentPage(number)}/>
                        </div>
                    </div>
                  </div>
            </div>

      </div>

   </div>
  )
}

export default Brands;
