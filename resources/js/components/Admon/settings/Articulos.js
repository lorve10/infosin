import React,{ useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import Swal from 'sweetalert2';
import api from '../../Servis/Api';
import util from "../../means/util";
import Pagination from '../../means/paginate';


function Articulo(props) {

  const [ articulo, setArticulo ] = useState(null);
  const [ listArticulo, setListArticulo] = useState([]);
  const [ listArticuloBack, setListArticuloBack] = useState([]);
  const [ currentPage, setCurrentPage] = useState(1)
  const [ perPage, setPerPage] = useState(6)


  useEffect(()=>{
    obtain_articulos()
  },[])

  //funcio para obtener los articulos
  const obtain_articulos = () =>{
    api.get('admon/getArticulo').then(response => {
      setListArticulo(response.data)
      setListArticuloBack(response.data)
    }).catch(console.warn)
  }

  const updateCurrentPage = async (number) => {
    await setListArticulo([])
    await setCurrentPage(number)
    const dataNew = util.paginate(listArticuloBack,number,perPage)
    await setListArticulo(dataNew)
  }

  //funcion para guardar los datos
  const saveArticulo = async () => {
    var message = ''
    var error = false
    if(articulo == null){
      error = true
      message = 'Debes ingresar un articulo'
    }

    if(error){
      util.MessageError(message)
    }
    else {
      const data = new FormData()
      data.append('articulo', articulo)
      api.post('admon/saveArticulo',data).then(response=>{
        console.log('se guardo');
        if(response.success){
          if (props.id == null) {
            util.MessageSuccess('Se guardó con éxito');
            obtain_articulos()
            setArticulo('')
          }
          else{
            util.MessageSuccess('Se edito Correctamente')
          }
        }
        else{
          util.MessageError('No se logro guardar')
        }
      })
    }
  }

  const deleted = (value) => {
    Swal.fire({
    title: 'Seguro que deseas elimiar este elemento?',
    icon: 'question',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonText:'No',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Si'
    }).then((result) => {
      if (result.isConfirmed) {
        const data = new FormData()
        data.append('id', value)
        api.post('admon/deletedSArticulo', data).then(response =>{
          if (response.data == 1 ) {
            Swal.fire(
              'Deleted!',
              'Eliminado correctamente.',
              'success'
            )
            obtain_articulos()
          }
          else if (response.data == 2){
            util.MessageError("No puede eliminar este articulo porque se está asociada a un dispositivo")
          }
        })
      }
      else{
        util.MessageError("No se eliminó")
      }
    })
  }

  return(
    <div className="container">
      <div className="flex mb-3">
        {
          props.id > 0 ?
            <h2 className="text-gray-800 text-3xl font-bold ml-3 mt-3">Editar articulo</h2>
            :
            <h2 className="text-gray-800 text-3xl font-bold ml-3 mt-3">Registrar articulo</h2>
        }
      </div>
      <div className="container col-12 flex flex-col md:flex-row justify-between">
        <div className="w-full md:w-2/5">
            <label className="block text-gray-700 text-sm font-bold mb-2">
            Articulo
            </label>
            <input className="shadow apparence-none border rounded w-full py-2 px-2 text-gray-700 leading-tight focus:outline-none focus:shadow-putline"
            type="text" placeholder="Articulo"
            value={articulo}
            onChange = {(e)=>setArticulo(e.target.value)}
            />
            <div className="mb-3 mt-4 flex flex-col sm:flex-row  justify-between">
                <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold my-2 py-2 px-4 rounded-full" onClick={()=>props.goBack()} >
                Regresar
                </button>
                <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold my-2 py-2 px-4 rounded-full" onClick={()=>saveArticulo()}>
                Guardar
                </button>
            </div>
        </div>
        <div className="my-3">
            <div className="w-full">
              <table className="table-auto rounder-lg shadow-lg">
                <thead className="table-auto">
                  <tr className="text-sm font.semibold tracking-wide text-center text-gray-900 bg-blue-500 uppercase border-gray-600">
                    <th className="px-4 py-3">Item</th>
                    <th className="px-4 py-3">Articulo</th>
                    <th className="px-4 py-3">Eliminar</th>
                  </tr>
                </thead>
                <tbody>
                  {
                    listArticulo.map((item, index)=>{
                      return(
                        <tr className="text-gray-700">
                          <th className="px-4 py-3 text-sm border text-center">{index+1}</th>
                          <th className="px-4 py-3 text-sm border text-center">{item.articu}</th>
                          <th className="px-4 py-3 text-sm border text-center">
                            <spam className="material-icons" onClick={()=>deleted(item.id)}>delete</spam>
                          </th>
                        </tr>
                      );
                    })
                  }
                </tbody>
              </table>
              <div className="d-flex col-md-12 col-12 justify-content-end mt-2">
                <Pagination currentPage={currentPage} perPage={perPage} countdata={listArticuloBack.length} updateCurrentPage={(number)=>updateCurrentPage(number)}/>
              </div>
            </div>

        </div>
      </div>
    </div>

  );


}
export default Articulo;
