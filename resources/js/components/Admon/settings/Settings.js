import React,{ useState, useEffect} from 'react';
import ReactDOM from 'react-dom';
import Brands from './Brands';
import Linea from './Linea';
import Articulo from './Articulos';
import Cargos from './Cargos';
import TipoOrden from './TipoOrden';
import CaratOrden from './CaratOrden';
import History from './History';
import EstadoOrden from './EstadoOrden';
import util from "../../means/util";
import Pagination from '../../means/paginate';


function Settings() {

  const [ shofrom, setShofrom ] = useState(1)
  //const ip = 'http://localhost:8000/';
  const ip = 'http://electrodigitalhuila.com/';

  ///regresar a la pagina principal
    const goBack =()=>{
      setShofrom(1)
    }


  return(
    <div className="py-4 px-8 bg-white shadow-lg rounded-lg ">

      {
        shofrom == 1 ?
        <div>

          <div className="container mx-auto mt-3 text-center">
                  <h1 className="mb-4 text-3xl font-bold">Configuraciones</h1>

                  <div className="flex px-14 xl:px-36 flex flex-wrap flex-col sm:px-20 flex-row justify-center items-start ">
                      <div className="flex-1 p-0 my-3 mx-5 w-4/5 md:w-1/4 ">
                          <button onClick={()=>setShofrom(2)} >
                              <div className="flex flex-row overflow-hidden h-auto border rounded-xl shadow shadow-lg p-1">
                                <img className="block h-6 w-25 sm: h-auto w-1/3 flex-none bg-cover rounded-xl w-1 h-1" src={ip+'images/marca.png'}/>
                                <div className="flex-1 p-4 ">
                                  <div className="text-black font-bold ">Prefijos</div>
                                </div>
                              </div>
                          </button>
                      </div>
                      <div className="flex-1 p-0 my-3 mx-5 w-4/5 md:w-1/4 ">
                          <button onClick={()=>setShofrom(3)} >
                              <div className="flex flex-row overflow-hidden h-auto border rounded-xl shadow shadow-lg p-1">
                                <img className="block h-6 w-25 sm: h-auto w-1/3 flex-none bg-cover rounded-xl w-1 h-1" src={ip+'images/linea.png'}/>
                                <div className="flex-1 p-4">
                                  <div className="text-black font-bold ">Lineas</div>
                                </div>
                              </div>
                          </button>
                      </div>
                  </div>

                  <div className="flex px-14 xl:px-36 flex flex-wrap flex-col sm:px-20 flex-row justify-center items-start ">
                      <div className="flex-1 p-0 my-3 mx-5 w-4/5 md:w-1/4 ">
                          <button onClick={()=>setShofrom(4)} >
                              <div className="flex flex-row overflow-hidden h-auto border rounded-xl shadow shadow-lg p-1">
                                <img className="block h-6 w-25 sm: h-auto w-1/3 flex-none bg-cover rounded-xl w-1 h-1" src={ip+'images/articulos.png'}/>
                                <div className="flex-1 p-4">
                                  <div className="text-black font-bold ">Articulos</div>
                                </div>
                              </div>
                          </button>
                      </div>
                      <div className="flex-1 p-0 my-3 mx-5 w-4/5 md:w-1/4 ">
                          <button onClick={()=>setShofrom(5)} >
                              <div className="flex flex-row overflow-hidden h-auto border rounded-xl shadow shadow-lg p-1">
                                <img className="block h-6 w-25 sm: h-auto w-1/3 flex-none bg-cover rounded-xl w-1 h-1" src={ip+'images/cargos.png'}/>
                                <div className="flex-1 p-4">
                                  <div className="text-black font-bold ">Cargos</div>
                                </div>
                              </div>
                          </button>
                      </div>
                  </div>

                  <div className="flex px-14 xl:px-36 flex flex-wrap flex-col sm:px-20 flex-row justify-center items-start ">
                      <div className="flex-1 p-0 my-3 mx-5 w-4/5 md:w-1/4 ">
                          <button onClick={()=>setShofrom(6)} >
                              <div className="flex flex-row overflow-hidden h-auto border rounded-xl shadow shadow-lg p-1">
                                <img className="block h-6 w-25 sm: h-auto w-1/3 flex-none bg-cover rounded-xl w-1 h-1" src={ip+'images/tipoServicio.png'}/>
                                <div className="flex flex-1 p-2 lg:flex flex-1 p-4">
                                  <div className="text-black font-bold ">Tipo servicio</div>
                                </div>
                              </div>
                          </button>
                      </div>
                      <div className="flex-1 p-0 my-3 mx-5 w-4/5 md:w-1/4 ">
                          <button onClick={()=>setShofrom(7)} >
                              <div className="flex flex-row overflow-hidden h-auto border rounded-xl shadow shadow-lg p-1">
                                    <img className="block h-6 w-25 sm: h-auto w-1/3 flex-none bg-cover rounded-xl w-1 h-1" src={ip+'images/caracteristicaOrden.png'}/>
                                <div className="flex-1 p-3">
                                  <div className="text-black font-bold ">Caracteristica de orden</div>
                                </div>
                              </div>
                          </button>
                      </div>
                  </div>
                  <div className="flex px-14 xl:px-36 flex flex-wrap flex-col sm:px-20 flex-row justify-center items-start ">
                      <div className="flex-1 p-0 my-3 mx-5 w-4/5 md:w-1/4 ">
                          <button onClick={()=>setShofrom(8)} >
                              <div className="flex flex-row overflow-hidden h-auto border rounded-xl shadow shadow-lg p-1">
                                <img className="block h-6 w-25 sm: h-auto w-1/3 flex-none bg-cover rounded-xl w-1 h-1" src={ip+'images/estado.png'}/>
                                <div className="flex flex-1 p-2 lg:flex flex-1 p-4">
                                  <div className="text-black font-bold ">Estado orden</div>
                                </div>
                              </div>
                          </button>
                      </div>
                      <div className="flex-1 p-0 my-3 mx-5 w-4/5 md:w-1/4 ">
                          <button onClick={()=>setShofrom(9)} >
                              <div className="flex flex-row overflow-hidden h-auto  border rounded-xl shadow shadow-lg p-1">
                                <img className="block h-6 w-25 sm: h-auto w-1/3 flex-none bg-cover rounded-xl w-1 h-1" src={ip+'images/proximo.png'}/>
                                <div className="flex-1 p-4">
                                  <div className="text-black font-bold ">Historial</div>
                                </div>
                              </div>
                          </button>
                      </div>
                  </div>

            </div>

      </div>


        :null
      }

      <div>
          {
            shofrom == 2 ?
            <Brands goBack = {()=>goBack()}/>
            :shofrom == 3 ?
            <Linea goBack = {()=>goBack()}/>
            :shofrom == 4 ?
            <Articulo goBack = {()=>goBack()} />
            :shofrom == 5 ?
            <Cargos goBack = {()=>goBack()}/>
            :shofrom == 6 ?
            <TipoOrden goBack = {()=>goBack()}/>
            :shofrom == 7 ?
            <CaratOrden goBack = {()=>goBack()}/>
            :shofrom == 8 ?
            <EstadoOrden goBack = {()=>goBack()}/>
            :shofrom == 9 ?
              <History goBack = {()=>goBack()}/>
            :null
          }
      </div>
   </div>
  )
}

export default Settings;
if (document.getElementById('Settings')) {
    ReactDOM.render(<Settings />, document.getElementById('Settings'));
}
