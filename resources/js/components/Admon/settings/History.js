import React,{ useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import Swal from 'sweetalert2';
import api from '../../Servis/Api';
import util from "../../means/util";
import Pagination from '../../means/paginate';
import * as moment from 'moment';

function CaratOrden(props) {

  const [ tipoCara, setTipoCara ] = useState(null);
  const [ codigo, setCodigo ] = useState(null);
  const [ list, setList] = useState([]);
  const [ listBack, setListBack] = useState([]);
  const [currentPage, setCurrentPage] = useState(1)
  const [perPage, setPerPage] = useState(6)

  useEffect(()=>{
    obtain_tipoCara()
  },[])


  const updateCurrentPage = async (number) => {
    await setlist([])
    await setCurrentPage(number)
    const dataNew = util.paginate(listBack,number,perPage)
    await setlist(dataNew)
  }

  //funcio para obtener los articulos
  const obtain_tipoCara = () =>{
   api.get('admon/ordenHistorial').then(response => {
      setList(response.data)
      setListBack(response.data)
    }).catch(console.warn)
  }





  return(
    <div className="container">
      <div className="flex mb-3 justify-center">

            <h2 className="text-gray-800 text-3xl font-bold ml-3 mt-3 text-center">Historial de ordenes - usuarios </h2>

      </div>
      <div className="container col-12 flex flex-col md:flex-row justify-center">
        <div className="my-3">
            <div className="w-full">
              <table className="table-auto rounder-lg shadow-lg">
                <thead className="table-auto">
                  <tr className="text-sm font.semibold tracking-wide text-center text-gray-900 bg-blue-500 uppercase border-gray-600">
                    <th className="px-4 py-3">Item</th>
                    <th className="px-4 py-3">Nombre Usuario</th>
                    <th className="px-4 py-3">Correo</th>
                    <th className="px-4 py-3">Orden</th>
                    <th className="px-4 py-3">Fecha creacion</th>

                  </tr>
                </thead>
                <tbody>
                  {
                    list.map((item, index)=>{
                      var fecha_creacion = moment(item.created_at).format("YYYY-MM-DD");

                      return(
                        <tr className="text-gray-700" key={index}>
                          <th className="px-4 py-3 text-sm border text-center">{item.id}</th>
                          <th className="px-4 py-3 text-sm border text-center">{item.nombre}</th>
                          <th className="px-4 py-3 text-sm border text-center">{item.correo}</th>
                            <th className="px-4 py-3 text-sm border text-center">{item.orden}</th>
                              <th className="px-4 py-3 text-sm border text-center">{fecha_creacion}</th>
                        </tr>
                      );
                    })
                  }
                </tbody>
              </table>
              <div className="d-flex col-md-12 col-12 justify-content-end mt-2">
                <Pagination currentPage={currentPage} perPage={perPage} countdata={listBack.length} updateCurrentPage={(number)=>updateCurrentPage(number)}/>
              </div>
            </div>
            <div className="mb-3 mt-4 flex flex-col sm:flex-row justify-end">
            <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold my-2 py-2 px-4 rounded-full " onClick={()=>props.goBack()} >
            Regresar
            </button>
          </div>
        </div>
      </div>
    </div>

  );


}
export default CaratOrden;
