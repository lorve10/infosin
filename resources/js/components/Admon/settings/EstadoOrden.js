import React,{ useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import Swal from 'sweetalert2';
import api from '../../Servis/Api';
import util from "../../means/util";
import Pagination from '../../means/paginate';

function EstadoOrden(props) {

  const [ nombre, setNombre ] = useState(null);
  const [ list, setList] = useState([]);
  const [ listBack, setListBack] = useState([]);

  const [ currentPage, setCurrentPage] = useState(1)
  const [ perPage, setPerPage] = useState(6)



  useEffect(()=>{
    obtain_estado()
  },[])

  //funcio para obtener los articulos
  const obtain_estado = () =>{
   api.get('admon/getStatus').then(response => {
      setList(response.data)
      setListBack(response.data)
    }).catch(console.warn)
  }

  const updateCurrentPage = async (number) => {
    await setList([])
    await setCurrentPage(number)
    const dataNew = util.paginate(listBack,number,perPage)
    await setList(dataNew)
  }
  //funcion para guardar los datos
  const saveCara = async () => {
    var message = ''
    var error = false

    if(nombre == null){
      error = true
      message = 'Debes ingresar un nombre'
    }

    if(error){
      util.MessageError(message)
    }
    else {
      const data = new FormData()
      data.append('nombre', nombre)
      api.post('admon/saveStatus',data).then(response=>{
        if(props.id == null){
          util.MessageSuccess('Se guardó con éxito');
          obtain_estado()
          setNombre("")
        }
        else{
          util.MessageSuccess('Editado con éxito');
        }
      })
    }
  }


  const deleted = (value) =>{
    Swal.fire({
    title: 'Seguro que deseas elimiar este elemento?',
    icon: 'question',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonText:'No',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Si'
    }).then((result) => {
      if (result.isConfirmed) {
        const data = new FormData()
        data.append('id', value)
        api.post('admon/deletedStatus', data).then(response =>{
          if(response.data == 1){
            Swal.fire(
              'Deleted!',
              'Eliminado correctamente.',
              'success'
            )
            obtain_estado()
          }
          else if (response.data == 2) {
            util.MessageError("No puedes eliminar este estados por que esta asociado a un Orden")
          }

        })
      }
      else {
        util.MessageError("No se eliminó")
      }
    })

  }


  return(
    <div className="container">
      <div className="flex mb-3">

            <h2 className="text-gray-800 text-3xl font-bold ml-3 mt-3">Registrar estado de orden</h2>

      </div>
      <div className="container col-12 flex flex-col md:flex-row justify-between">
        <div className="w-full md:w-2/5">
            <label className="block text-gray-700 text-sm font-bold mb-2">
                Nombre
            </label>
            <input className="shadow apparence-none border rounded w-full py-2 px-2 text-gray-700 leading-tight focus:outline-none focus:shadow-putline"
            type="text" placeholder="Caracteristica de orden"
            value={nombre}
            onChange = {(e)=>setNombre(e.target.value)}
            />

            <div className="mb-3 mt-4 flex flex-col sm:flex-row  justify-between">
                <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold my-2 py-2 px-4 rounded-full" onClick={()=>props.goBack()} >
                Regresar
                </button>
                <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold my-2 py-2 px-4 rounded-full" onClick={()=>saveCara()}>
                Guardar
                </button>
            </div>
        </div>
        <div className="my-3">
            <div className="w-full">
              <table className="table-auto rounder-lg shadow-lg">
                <thead className="table-auto">
                  <tr className="text-sm font.semibold tracking-wide text-center text-gray-900 bg-blue-500 uppercase border-gray-600">
                    <th className="px-4 py-3">Item</th>
                    <th className="px-4 py-3">Nombre</th>
                    <th className="px-4 py-3">Eliminar</th>
                  </tr>
                </thead>
                <tbody>
                  {
                    list.map((item, index)=>{
                      return(
                        <tr className="text-gray-700">
                          <th className="px-4 py-3 text-sm border text-center">{index}</th>
                          <th className="px-4 py-3 text-sm border text-center">{item.nombreStado}</th>
                          <th className="px-4 py-3 text-sm border text-center">
                            <spam className="material-icons" onClick={()=>deleted(item.id)}>delete</spam>
                          </th>
                        </tr>
                      );
                    })
                  }
                </tbody>
              </table>
              <div className="d-flex col-md-12 col-12 justify-content-end mt-2">
                <Pagination currentPage={currentPage} perPage={perPage} countdata={listBack.length} updateCurrentPage={(number)=>updateCurrentPage(number)}/>
              </div>
            </div>

        </div>
      </div>
    </div>

  );


}
export default EstadoOrden;
