import React,{ useState, useEffect} from 'react';
import ReactDOM from 'react-dom';
import '../../../../css/app.css';
import Swal from 'sweetalert2';
import api from '../../Servis/Api';
import util from "../../means/util";
import Pagination from '../../means/paginate';

export default function List ({ resive }){

  const [ text, setText ] = useState('')
  const [ listCust, setListCust ] = useState([])
  const [ listContenido, setListContenido ] = useState([])
  /// variables paginador
  const [ listCustBack, setListCustBack ] = useState([])
  const [ currentPage, setCurrentPage ] = useState(1)
  const [ perPage, setPerPage ] = useState(6)
  const [ count, setCount ] = useState(0)


  const prue = getParameterByName('prueba');


  function getParameterByName(name) {  /// esta funcion recoje la info que viene de la ruta
      if (name !== "" && name !== null && name != undefined) {
          name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
          var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
              results = regex.exec(location.search);
          return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
      } else {
          var arr = location.href.split("/");
          return arr[arr.length - 1];
      }
  }

  // useEffect(()=>{
  //   const timer = setTimeout(() => {
  //     obtain()
  //     setCount(prev=>prev + 1);
  //   }, 10000);
  //   return () => clearTimeout(timer)
  // },[count])

  /// obtener datos de los clientes
  useEffect(()=>{
    if (prue == '') {
        obtain()
    }
    if (prue == 5) {
      obtain2()
    }

  },[prue])

  const obtain = () =>{
    api.get('admon/listChallenger')
    .then(response =>{
      setListCust(response.data)
      setListCustBack(response.data)
      setListContenido(response.data)
      console.log(response.data);
    }).catch(error => console.error("Error: ", error))
  }
  const obtain2 = () =>{
    api.get('admon/listChallengerP')
    .then(response =>{
      setListCust(response.data)
      setListCustBack(response.data)
      setListContenido(response.data)
    }).catch(error => console.error("Error: ", error))
  }

  const updateCurrentPage = async (number) => {
    await setListCust([])
    await setCurrentPage(number)
    const dataNew = util.paginate(listCustBack,number,perPage)
    await setListCust(dataNew)
  }

  /// funcion de swar alertas con estilo
    const MessageError = async (data) => {
        Swal.fire({
          title: 'Error',
          text: data,
          icon: 'warning',
        })
      }
      const MessageSuccess = async (data) => {
        Swal.fire({
          text: data,
          icon: 'success',
        })
      }


  ///funcion para filtrar datos de clientes en la tabla
  const filterOrderText = (event) => {
      var text = event.target.value
      const data = listContenido
      const newData = data.filter(function(item){
          var textAll = " "
          textAll = textAll+" "+item.pedido.toUpperCase()
          textAll = textAll+" "+item.modelo_part.toUpperCase()
          textAll = textAll+" "+item.ordenkall.orden
          textAll = textAll+" "+item.tecnicoped.name.toUpperCase()
          textAll = textAll+" "+item.solicita

          const textData = text.toUpperCase()
          return textAll.indexOf(textData) > -1
      })
      setListCust(newData)
      setText(text)
    }

  const repart = (data) =>{ resive(data) }

return(
  <div className="mt-4 ">
    <div className="flex justify-content-center mb-4">
      <div className="col-6">
        <input className="shadow appearance-none border text-center rounded-full w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
        type="text" placeholder="Buscar repuestos"
        value = {text}
        onChange={(e) => filterOrderText(e)}
        />
      </div>
    </div>

    <div className="overflow-auto h-5/6 ...">
        <section className="container mx-auto p-6 font-mono">
          <div className="w-full mb-8 overflow-hidden rounded-lg shadow-lg">
            <div className="w-full overflow-x-auto">
            <table className="w-full ">
              <thead className="">
                <tr className="text-md font-semibold tracking-wide text-left text-gray-900 bg-blue-400 uppercase border-b border-gray-600">
                  <th className="px-2 py-3 text-center">Pedido</th>
                  <th className="px-2 py-3 text-center">Parte número</th>
                  <th className="px-2 py-3 text-center">Descripción</th>
                  <th className="px-2 py-3 text-center">Cantidad</th>
                  <th className="px-2 py-3 text-center">Modelo</th>
                  <th className="px-2 py-3 text-center">Orden</th>
                  <th className="px-2 py-3 text-center">Técnico</th>
                  <th className="px-2 py-3 text-center">Fecha pedido técnico</th>
                  <th className="px-2 py-3 text-center">Solicita</th>
                  <th className="px-2 py-3 text-center">Fecha pedido</th>
                  <th className="px-2 py-3 text-center">Acciones</th>

                </tr>
              </thead>
              <tbody className="bg-white">

                {
                  listCust.map((item,index)=>(
                    <tr className={item.estado == 0 ?  "bg-red-300 text-gray-700 " : item.estado == 2 ? "bg-green-500 text-gray-700" : null } key={index}>
                        <td className="px-4 py-3 border">
                          <div className="flex items-center text-sm">
                              <p className="font-semibold text-black">{item.pedido}</p>
                          </div>
                        </td>
                        <td className="px-4 py-3 text-ms font-semibold border">{item.partenumero}</td>
                        <td className="px-4 py-3 text-sm border">{item.descripcion}</td>
                        <td className="px-4 py-3 text-sm border">{item.cantidad}</td>
                        <td className="px-4 py-3 text-sm border">{item.modelo}</td>
                          <td className="px-4 py-3 text-sm border">{item.ordenkall.orden}</td>
                          <td className="px-4 py-3 text-sm border">{item.tecnicoped.name}</td>
                          <td className="px-4 py-3 text-sm border">{item.fechapedidotec}</td>
                          <td className="px-4 py-3 text-sm border">{item.solicita}</td>
                          <td className="px-4 py-3 text-sm border">{item.fechapedido}</td>
                          <td className="px-5 py-2 text-sm border">
                            {
                               item.estado == 2 ?
                               <span className="material-icons cursor-pointer"
                                 style={{fontSize:30}}
                                 onClick={()=>MessageSuccess("Este producto ya se registro con exito")}>
                               file_download_done
                               </span>
                               :
                               <span className="material-icons cursor-pointer"
                                 style={{fontSize:30}}
                                 onClick={()=>repart(item)}>
                               file_download_done
                               </span>
                            }

                          </td>
                      </tr>
                  ))
                }
              </tbody>
            </table>
            <div className="d-flex col-md-12 col-12 justify-content-end mt-2">
              <Pagination currentPage={currentPage} perPage={perPage} countdata={listCustBack.length} updateCurrentPage={(number)=>updateCurrentPage(number)}/>
            </div>
            </div>
          </div>
        </section>
    </div>
  </div>
)
}
