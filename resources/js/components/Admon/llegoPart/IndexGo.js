import ReactDOM from 'react-dom';
import React, { useState } from 'react';
import Form from './Form';
import List from './List';

function IndexGo() {
  const [ showForm, setShowForm ] = useState(1);
  const [ info, setInfo ] = useState({});
  const [ id, setId ] = useState(null);
  const goBack = () =>{ setShowForm(1); }

  const resive = (data) =>{
    console.log("llego al resive");
    console.log(data);
    setShowForm(2);
    setInfo(data)
    console.log(data.ordenkall.id);
    setId(data.ordenkall.id)
  }

  return(
      <div className="py-4 px-8 bg-white shadow-lg rounded-lg">
      {
        showForm == 1 ?
       <div>
            <div className="flex justify-content-start mt-0 ml-3">
                <div className="bg-green-500 text-white p-2 mr-2 rounded-lg">Repuesto llegó</div>
                <div className="bg-red-300 text-white p-2 rounded-lg">Repuesto en tramite</div>
            </div>
            <div className=" flex justify-content-end mt-0">
                  <h2 className="text-gray-800 text-3xl font-semibold mr-4">Lista repuestos </h2>
            </div>
        </div>
          :null
        }
        <div className="flex justify-content-end mr-7 mt-3">
        </div>
          {
            showForm == 1 ?
            <List  resive={(data)=>resive(data)} />
            :showForm == 2 ?
            <Form  list= {info} goBack = {()=>goBack()} id={id}/>
            :null
          }
      </div>

    );
}
export default IndexGo;
if (document.getElementById('IndexGo')) {
    ReactDOM.render(<IndexGo />, document.getElementById('IndexGo'));
}
