import React,{ useState, useEffect} from 'react';
import ReactDOM from 'react-dom';
import '../../../../css/app.css';
import axios from 'axios';
import Swal from 'sweetalert2';
import api from '../../Servis/Api';
import Select from 'react-select';
import makeAnimated from 'react-select/animated';

export default function Customers({id, list, goBack }) {
  ///variables del formulario

   const [ fechaLlegada, setFechaLlegada ] = useState("")
   const [ cantidad, setCantidad ] = useState("")
   const [ observaciones, setObservaciones ] = useState("")
   const [ cliente, setCliente ] = useState("")
   const [ orden, setOrden ] = useState("")
   const [ tecnico, setTecnico ] = useState("")
   const [ ordenSelect, setOrdenSelect ] = useState([])
   const [ tecnicoSelect, setTecnicoSelect ] = useState([]);
   const [ clienteSelect, setClienteSelect ] = useState([]);

   const [animatedComponents, setAnimatedComponents] = useState(makeAnimated);



   useEffect(()=>{
     obtain_orden();
     obtain_tecnico();
     obtain_cliente();
   },[])



   const obtain_orden = () =>{
     api.get('admon/getOrders').then(response=>{
       let res = response.data;
       let orden = []
       res.map(item=>{
         const data = {
           value: item.id, label: item.orden
         }
         orden.push(data);
         setOrdenSelect(orden);

         if(id > 0){
          let filtro = res.filter(e=>e.id == id);
          const data = {
            value: filtro[0].id, label: filtro[0].orden
          }
          setOrden(data)
         }

     })
   }).catch(error => console.error("Error: ", error))
 }

 const obtain_cliente = () =>{
   api.get('admon/getCustomers').then(response=>{
     let res = response.data;
     let orden = []
     res.map(item=>{
       const data = {
         value: item.id, label: item.cust_nombre
       }
       orden.push(data);
       setClienteSelect(orden);
       if(id > 0){
         let filtro = res.filter(e=>e.id == list.ordenkall.customer_id);
         const data = {
           value: filtro[0].id, label: filtro[0].cust_nombre
         }
         setCliente(data)
       }
   })
 }).catch(error => console.error("Error: ", error))
}

   const obtain_tecnico = () =>{
     api.get('admon/getUsers').then(response=>{
        let res = response.data;
        let tecni = [];
        res.map(item=>{
          const data = {
            value: item.id, label: item.name
          }
          tecni.push(data)
          setTecnicoSelect(tecni)
        })
     }).catch(error => console.error("Error: ", error))
   }

/// funcion de swar alertas con estilo
  const MessageError = async (data) => {
      Swal.fire({
        title: 'Error',
        text: data,
        icon: 'warning',
      })
    }
    const MessageSuccess = async (data) => {
      Swal.fire({
        text: data,
        icon: 'success',
      })
    }

///funcion para guardar datos
  const onClickLogin = async () => {

    let message = ''
    let error = false

    if (tecnico == "") {
      error = true
      message = "Debes seleccionar quién recibe"
    }
    else if (fechaLlegada == "") {
      error = true
      message = "Debes agregar fecha de llegada"
    }
    else if (cantidad == "") {
      error = true
      message = "Debes agregar una cantidad"
    }  else if (observaciones == "") {
        error = true
        message = "Debes agregar una observación"
    }
    else if (cliente == "") {
      error = true
      message = "Debes seleccionar un cliente"
    }
    else if (orden == "") {
      error = true
      message = "Debes agregar una orden"
    }
    if (error) {
      MessageError(message)
    }
    else {
      const data = new FormData()
      data.append('id', list.id)
      data.append('recibe', tecnico.value)
      data.append('fechallegada', fechaLlegada)
      data.append('cantidadllega', cantidad)
      data.append('observaciones', observaciones)
      data.append('order_id', orden.value)
      data.append('customer_id', cliente.value)
      api.post('admon/savePart',data).then(response=>{
        if (response.success) {
            MessageSuccess("Parte ha llegado");
            goBack();
        }else {
          console.log("no guardooooo");
        }
      }).catch(err => console.warn(err))
    }
  }

  return(
    <div >
      <div className="flex mb-3">
          <h2 className="text-gray-800 text-3xl font-semibold ml-3">Llegada del repuesto</h2>
      </div>
      <div className="overflow-auto h-5/6 ...">
          <div className="grid grid-cols-1 sm:grid-cols-2 mt-2 gap-4 px-3 ">
              <div className="">
                 <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="password">
                      Recibe
                  </label>
                  <Select
                  className="shadow block text-gray-700 text-sm font-bold mb-2"
                  value={tecnico}
                  closeMenuOnSelect={true}
                  components={animatedComponents}
                  options={tecnicoSelect}
                  onChange={(e)=>setTecnico(e)}
                  placeholder= "Seleccionar"
                  />
              </div>
               <div className="">
                  <label className="block text-gray-700 text-sm font-bold mb-2">
                       Fecha de llegada
                   </label>
                    <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                      type="date" placeholder="fecha llegada"
                      value = {fechaLlegada}
                      onChange = {(e)=>setFechaLlegada(e.target.value)}
                      />
               </div>
          </div>
          <div className="grid grid-cols-1 sm:grid-cols-2 mt-2 gap-4 px-3">
              <div className="">
                 <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="username">
                    Cantidad llegada
                 </label>
                 <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                   type="number" placeholder="Cantidad llegada"
                   value = {cantidad}
                   onChange = {(e)=>setCantidad(e.target.value)}
                   />
              </div>
                <div className="">
                   <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="username">
                      Observaciones
                   </label>
                   <textarea className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                     type="text" placeholder="Observaciones"
                     value = {observaciones}
                     onChange = {(e)=>setObservaciones(e.target.value)}
                     ></textarea>
                </div>

          </div>
          <div className="grid grid-cols-1 sm:grid-cols-2 mt-2 gap-4 px-3">
                <div className="">
                   <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="password">
                        Cliente
                    </label>
                    <Select
                    className="shadow block text-gray-700 text-sm font-bold mb-2"
                    value={cliente}
                    closeMenuOnSelect={true}
                    components={animatedComponents}
                    options={clienteSelect}
                    onChange={(e)=>setCliente(e)}
                    placeholder= "Seleccionar cliente"
                    />
                </div>
                <div className="">
                   <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="username">
                      Orden
                   </label>
                   <Select
                   className="shadow block text-gray-700 text-sm font-bold mb-2"
                   value={orden}
                   closeMenuOnSelect={true}
                   components={animatedComponents}
                   options={ordenSelect}
                   onChange={(e)=>setOrden(e)}
                   placeholder= "Seleccionar orden"
                   />
                </div>
          </div>
          <div className="mt-5 p-2 flex mr-3 justify-content-between">
              <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full" onClick={()=>goBack()}>
                Regresar
              </button>
              <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full" onClick={()=>onClickLogin()} >
                Guardar
              </button>
          </div>
      </div>

    </div>
  );

}
