import React, { useState } from 'react';
import ReactDom from 'react-dom';
import Form from './Form';
import List from './List';


function IndexPart(){
  const [ showForm, setShowForm ] = useState(1);
  const goBack = () =>{ setShowForm(1); }
  const [ id, setId ] = useState(null)

  return(
      <div className="py-4 px-8 bg-white shadow-lg rounded-lg">
    {
      showForm == 1 ?
      <div>
        <div className="flex justify-content-start mt-0 ml-3">
            <div className="bg-green-500 text-white p-2 mr-2 rounded-lg">Repuesto llegó</div>
            <div className="bg-red-300 text-white p-2 rounded-lg">Repuesto en trámite</div>
        </div>
        <div className="flex justify-content-end mr-7 mt-3">
            <button className="bg-blue-500 hover:bg-blue-700 text-white font-semibold py-2 px-4 rounded-full" onClick={()=>setShowForm(2)}>
              Generar pedido
            </button>
        </div>
      </div>
      :null
    }
          {
            showForm == 1 ?
            <List  />
            :showForm == 2 ?
            <Form  id={id}  goBack = {()=>goBack()}/>
            :null
          }
      </div>

    );
  }
export default IndexPart;
if(document.getElementById('IndexPart')){
    ReactDom.render(<IndexPart/>, document.getElementById('IndexPart'))
}
