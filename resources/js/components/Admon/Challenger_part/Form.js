import React,{ useState, useEffect} from 'react';
import ReactDOM from 'react-dom';
import '../../../../css/app.css';
import axios from 'axios';
import Swal from 'sweetalert2';
import api from '../../Servis/Api';
import Select from 'react-select';
import makeAnimated from 'react-select/animated';

export default function Customers({id, goBack}) {
  ///variables del formulario
   const [ numero, setNumero ] = useState("")
   const [ descripcion, setDescripcion ] = useState("")
   const [ pedido, setPedido ] = useState("");
   const [ cantidad, setCantidad ] = useState("")
   const [ modelo, setModelo ] = useState("")
   const [ orden, setOrden ] = useState("")
   const [ tecnico, setTecnico ] = useState("")
   const [ fechaPeTecn, setFechaPeTecn ] = useState("")
   const [ solicitado, setSolicitado ] = useState("")
   const [ fechaPedido, setFechaPedido] = useState("")
   const [ animatedComponents, setAnimatedComponents ] = useState(makeAnimated);

  const [ ordenSelect, setOrdenSelect ] = useState([])
  const [ tecnicoSelect, setTecnicoSelect ] = useState([]);





   useEffect(()=>{
     obtain_orden();
     obtain_tecnico();
   },[])


   const obtain_orden = () =>{
     api.get('admon/getOrders').then(response=>{
       let res = response.data;
       let orden = []
       res.map(item=>{
         const data = {
           value: item.id, label: item.orden
         }
         orden.push(data);
         setOrdenSelect(orden);

     })
   }).catch(error => console.error("Error: ", error))
 }


 /// funcion de filtrado por departamento
 // const filtrado = (value) => {
 //     setDepartamento(value);
 //     obtain_ciudad(value.value, true)
 // }

   const obtain_tecnico = () =>{
     api.get('admon/getUsers').then(response=>{
        let res = response.data;
        let tecni = [];
        let filtro = res.filter(e=>e.position == 2);
        filtro.map(item=>{
          const data = {
            value: item.id, label: item.name
          }
          tecni.push(data)
          setTecnicoSelect(tecni)
        })
     }).catch(error => console.error("Error: ", error))
   }

/// funcion de swar alertas con estilo
  const MessageError = async (data) => {
      Swal.fire({
        title: 'Error',
        text: data,
        icon: 'warning',
      })
    }
    const MessageSuccess = async (data) => {
      Swal.fire({
        text: data,
        icon: 'success',
      })
    }

///funcion para guardar datos
  const onClickLogin = () => {

    let message = ''
    let error = false

    if (numero == "") {
      error = true
      message = "Debes agregar un numero"
    }
    else if (orden == "") {
      error = true
      message = "Debes agregar una orden"
    }
    if (error) {
      MessageError(message)
    }
    else {
      const data = new FormData()
      data.append('id', id)
      data.append('pedido', pedido)
      data.append('partenumero', numero)
      data.append('descripcion', descripcion)
      data.append('cantidad', cantidad)
      data.append('modelo', modelo)
      data.append('ordenkalley', orden.value)
      data.append('tecnico', tecnico.value)
      data.append('fechapedidotec', fechaPeTecn)
      data.append('solicita', solicitado)
      data.append('fechapedido', fechaPedido)
      api.post('tecnico/saveChallenger',data).then(response=>{
        if (response.success) {
          if(id == null){
            MessageSuccess("Parte registrado exitosamente")
            goBack();
          }
          else{
            MessageSuccess("Editado Correctamente")
            goBack();
          }
        }
      }).catch(error => console.error("Error: ", error))
    }
  }

  return(
    <div >
      <div className="flex mb-3">
          <h2 className="text-gray-800 text-3xl font-semibold ml-3">Realizar pedido</h2>
      </div>
      <div className="grid grid-cols-1 sm:grid-cols-2 mt-2 gap-4 px-3">
            <div className="">
               <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="username">
                  Orden
               </label>
               <Select
               className="shadow block text-gray-700 text-sm font-bold mb-2"
               value={orden}
               closeMenuOnSelect={true}
               components={animatedComponents}
               options={ordenSelect}
               onChange={(e)=>setOrden(e)}
               placeholder= "Seleccionar orden"
               />
            </div>
            <div className="">
               <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="password">
                    Modelo
                </label>
                 <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                   type="text" placeholder="Modelo"
                   value = {modelo}
                   onChange = {(e)=>setModelo(e.target.value)}
                   />
            </div>
      </div>
      <div className="overflow-auto h-5/6 ...">
          <div className="grid grid-cols-1 sm:grid-cols-2 mt-2 gap-4 px-3 ">
                <div className="">
                   <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="nombre">
                     Parte Número
                   </label>
                   <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                     value={numero}
                     onChange={(event)=>setNumero(event.target.value)}
                     type="number" placeholder="Numero"
                    />
                </div>
               <div className="">
                  <label className="block text-gray-700 text-sm font-bold mb-2">
                       Descripción
                   </label>
                    <textarea className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                      type="text" placeholder="Descripción"
                      value = {descripcion}
                      onChange = {(e)=>setDescripcion(e.target.value)}
                      ></textarea>
               </div>
          </div>
          <div className="grid grid-cols-1 sm:grid-cols-2 mt-2 gap-4 px-3">
              <div className="">
                 <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="username">
                    Pedido
                 </label>
                 <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                   type="text" placeholder="Pedido"
                   value = {pedido}
                   onChange = {(e)=>setPedido(e.target.value)}
                   />
              </div>
                <div className="">
                   <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="username">
                      Cantidad
                   </label>
                   <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                     type="number" placeholder="Cantidad"
                     value = {cantidad}
                     onChange = {(e)=>setCantidad(e.target.value)}
                     />
                </div>
          </div>
          <div className="grid grid-cols-1 sm:grid-cols-2 mt-2 gap-4 px-3">
                <div className="">
                   <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="password">
                        Técnico
                    </label>
                    <Select
                    className="shadow block text-gray-700 text-sm font-bold mb-2"
                    value={tecnico}
                    closeMenuOnSelect={true}
                    components={animatedComponents}
                    options={tecnicoSelect}
                    onChange={(e)=>setTecnico(e)}
                    placeholder= "Seleccionar técnico"
                    />
                </div>
                <div className="">
                   <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="username">
                      Fecha de pedido técnico
                   </label>
                   <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                     type="date" placeholder="Celular"
                     value = {fechaPeTecn}
                     onChange = {(e)=>setFechaPeTecn(e.target.value)}
                     />
                </div>

          </div>
          <div className="grid grid-cols-1 sm:grid-cols-2 mt-2 gap-4 px-3">
                <div className="">
                   <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="username">
                      Fecha pedido
                   </label>
                   <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                     type="date"  value = {fechaPedido}
                     onChange = {(e)=>setFechaPedido(e.target.value)}
                     />
                </div>
                <div className="">
                   <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="password">
                       Solicitado
                    </label>
                     <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                       type="number" placeholder="solicitado"
                       value = {solicitado}
                       onChange = {(e)=>setSolicitado(e.target.value)}
                       />
                </div>
          </div>
          <div className="mt-5 p-2 flex mr-3 justify-content-between">
              <button className="bg-blue-500 hover:bg-blue-700 text-white font-semibold py-2 px-4 rounded-full" onClick={()=>goBack()}>
                Regresar
              </button>
              <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full" onClick={()=>onClickLogin()} >
                Guardar
              </button>
          </div>
      </div>

    </div>
  );

}
