import React,{ useState, useEffect} from 'react';
import ReactDOM from 'react-dom';
import '../../../../css/app.css';
import axios from 'axios';
import Swal from 'sweetalert2';
import api from '../../Servis/Api';
import NewUsers from './NewUsers'
import ListUsers from './ListUsers'



function Users (){
  /// Variables
    const [ shofrom, setShofrom ] = useState(1)
    const [ id, setId ] = useState(null)
    const [ data, setData ] = useState({})

    ///regresar a la pagina principal
      const goBack =()=>{
        setShofrom(1)
        setId(null)
        setData({})
      }

      ///funcion para pasar datos a los input para editar
      const dataUpdate = (data) =>{

        var nombre = data.name
        var documento = data.iddoc
        var direccion = data.address
        var ciudad = data.city
        var departamento = data.departamento
        var barrio = data.neighborhood
        var celular = data.movil
        var telefono = data.phone
        var posicion = data.position
        var fechaNacimiento = data.birth_date
        var fechaContratacion = data.hired_date
        var estadoEmpleado = data.status
        var observaciones = data.notes
        var email = data.email
        var password = data.password
        var genero = data.genero


        const dataForAll = {
          nombre,
          documento,
          direccion,
          barrio,
          ciudad,
          departamento,
          celular,
          telefono,
          posicion,
          fechaNacimiento,
          fechaContratacion,
          estadoEmpleado,
          observaciones,
          email,
          genero,
          password
        }

        setId(data.id)
        setData(dataForAll)
        setShofrom(2)
      }

  return(

    <div className="py-4 px-8 bg-white shadow-lg rounded-lg" >
    <div className="flex justify-content-end mr-7 mt-3">
      {
        shofrom == 1 ?
        <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full" onClick={()=>setShofrom(2)}>
          Crear usuario
        </button>
        :null
      }
    </div>

      {shofrom == 1 ?
        <ListUsers dataUpdate = {(data)=>dataUpdate(data)}/>
        :shofrom == 2 ?
        <NewUsers list = {data} id = {id} goBack = {()=>goBack()}/>
        :null
      }

    </div>
  );


}
export default Users;
if (document.getElementById('Users')) {
    ReactDOM.render(<Users />, document.getElementById('Users'));
}
