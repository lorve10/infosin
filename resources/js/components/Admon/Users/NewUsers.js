import React,{ useState, useEffect} from 'react';
import ReactDOM from 'react-dom';
import '../../../../css/app.css';
import axios from 'axios';
import Swal from 'sweetalert2';
import api from '../../Servis/Api';
import Select from 'react-select';
import makeAnimated from 'react-select/animated';
import util from "../../means/util";

function NewUsers ({
  id,
  list,
  goBack
}){
  /// Variables
  const [ nombre, setNombre ] = useState("")
  const [ documento, setDocumento ] = useState("")
  const [ departamento, setDepartamento ] = useState("")
  const [ departamentoSelect, setDepartamentoSelect ] = useState([])
  const [ ciudad, setCiudad ] = useState("")
  const [ ciudadSelect, setCiudadSelect ] = useState([])
  const [ direccion, setDireccion ] = useState("")
  const [ barrio, setBarrio ] = useState("")
  const [ email, setEmail ] = useState("")
  const [ celular, setCelular ] = useState("")
  const [ telefono, setTelefono ] = useState("")
  const [ fechaNacimiento, setFechaNacimiento ] = useState("")
  const [ fechaContratacion, setFechaContratacion ] = useState("")
  const [ posicion, setPosicion ] = useState("")
  const [ posicionSelect, setPosicionSelect ] = useState([])
  const [ estadoEmpleado, setEstadoEmpleado ] = useState("")
  const [ estadoSelect, setEstadoSelect ] = useState([
    { value:'Activo', label:'Activo'},
    { value:'Inactivo', label:'Inactivo'}
  ])
  const [ password, setPassword ] = useState("")
  const [ observaciones, setObservaciones ]  = useState("")
  const [animatedComponents, setAnimatedComponents] = useState(makeAnimated);
  const [ genero, setGenero ] = useState("")
  const [ generoSelect, setGeneroSelect ] = useState([
    { value:'F', label:'F'},
    { value:'M', label:'M'}
  ])

  useEffect(()=>{
    validar()
    obtain_cargo()
    estado()
    obtain_departamento()
    generos()
    obtain_ciudad()
  },[])

  ///funcion para validar si existe la id para poder editar
    const validar = () =>{
      if(id > 0){
        setNombre(list.nombre == null ? " ": list.nombre)
        setDocumento(list.documento)
        setDireccion(list.direccion)
        setCelular(list.celular)
        setTelefono(list.telefono)
        setPosicion(list.posicion)
        setCiudad(list.ciudad)
        setDepartamento(list.departamento)
        setBarrio(list.barrio == null ? "" : list.barrio)
        setFechaNacimiento(list.fechaNacimiento)
        setFechaContratacion(list.fechaContratacion)
        setEstadoEmpleado(list.estadoEmpleado)
        setObservaciones(list.observaciones)
        setEmail(list.email)
        setPassword(list.password)
        setGenero(list.genero)
      }
    }

  const estado = () => {

      if (list.estadoEmpleado === "Activo"){
        const data = {
          value:'Activo', label:'Activo'
        }
        setEstadoEmpleado(data)
      } else if (list.estadoEmpleado === "Inactivo") {
        const data = {
          value:'Inactivo', label:'Inactivo'
        }
        setEstadoEmpleado(data)
      }
  }

  const generos = () => {
    if (list.genero === "F") {
      const data = {
        value:'F', label:'F'
      }
      setGenero(data)
    } else if (list.genero === "M"){
      const data = {
        value:'M', label:'M'
      }
      setGenero(data)
    }
  }



  // Funcion para obtener los cargos y listarlos en el select
      const obtain_cargo = async () =>{
        api.get('admon/getCargo')
        .then(response=>{
          let posicion = [];

          let res = response.data;
          res.map(item=>{
          const data = { value:item.id, label:item.cargo }
          posicion.push(data)
          setPosicionSelect(posicion)

          })
          if (id > 0 ){
            let filtro = res.filter(f=>f.id == list.posicion)
            const data = {
              value: filtro[0].id,
              label: filtro[0].cargo
            }
            setPosicion(data)
          }
        }).catch(error => console.warn())
      }

      // Funcion para obtener los departamentos y listarlos en el select
       const obtain_departamento = async () =>{
         api.get('admon/getDepartamento')
         .then(response=>{
             let departamento = [];
             let res = response.data;
             res.map(item=>{
               const data = { value:item.id_departamento, label:item.departamento }
               departamento.push(data)
               setDepartamentoSelect(departamento)
             })

             if (id > 0 ){
               let filtroDepart = res.filter(e=>e.id_departamento == list.departamento)
               console.log(filtroDepart);
               const data = {
                 value: filtroDepart[0].id_departamento,
                 label: filtroDepart[0].departamento
               }
               setDepartamento(data)
               obtain_ciudad(data.value, true)
             }
         }).catch(error => console.warn())
       }


    /// funcion de filtrado por departamento
    const filtrado = (value) => {
        setDepartamento(value);
        obtain_ciudad(value.value, true)
        setCiudad("")
    }


    // Funcion para obtener las ciudades y listarlos en el select
     const obtain_ciudad = async (valor, data) =>{
       setCiudadSelect([])
       api.get('admon/getMunicipio')
       .then(response=>{
         let res = response.data;
         let ciudad = [];
         let filtroDepart = response.data.filter((e)=>e.departamento_id === valor);
         filtroDepart.map(item=>{
           const data = {
             value:item.id_municipio,
             label:item.municipio
           }
           ciudad.push(data)
           setCiudadSelect(ciudad)
         })
         if (id > 0  ){
           let filtroCiudad = res.filter(f=>f.id_municipio == list.ciudad)
           const data = {
             value: filtroCiudad[0].id_municipio,
             label: filtroCiudad[0].municipio
           }
           setCiudad(data)
         }
       }).catch(error => console.warn())
     }

  ///funcion para guardar datos
      const saveUsers = async () => {
        var message = ''
        var error = false

        if( nombre == null){
          error = true
          message = "Debes agregar un nombre"
        }

        else if ( documento == null){
          error = true
          message = "Debes agregar un documento"
        }
        else if (ciudad == null){
          error = true
          message = "Debes agregar una Ciudad"
        }
        else if (direccion == null){
          error = true
          message = "Debes agregar una direccion"
        }
        else if (barrio == null){
          error = true
          message = "Debes agregar un barrio"
        }
        else if (email == null){
          error = true
          message = "Debes ingresar un email"
        }
        else if (celular == null){
          error = true
          message = "Debes agregar una celular"
        }
        else if (telefono == null){
          error = true
          message = "Debes agregar un telefono"
        }
        else if (fechaNacimiento == null){
          error = true
          message = "Debes agregar una fecha de nacimiento"
        }
        else if (fechaContratacion == null){
          error = true
          message = "Debes agregar una fecha de contratación"
        }
        else if (posicion == null){
          error = true
          message = "Debes agregar una posicion"
        }
        else if (estadoEmpleado == null){
          error = true
          message = "Debes agregar un Estado"
        }
        if (error){
          util.MessageError(message)
        }
        else {
          const data = new FormData()
          data.append('id', id)
          data.append('nombre', nombre)
          data.append('documento', documento)
          data.append('direccion', direccion)
          data.append('ciudad', ciudad.value)
          data.append('departamento', departamento.value)
          data.append('barrio', barrio)
          data.append('celular', celular)
          data.append('telefono', telefono)
          data.append('posicion', posicion.value)
          data.append('fechaNacimiento', fechaNacimiento)
          data.append('fechaContratacion', fechaContratacion)
          data.append('estadoEmpleado', estadoEmpleado.value)
          data.append('genero', genero.value)

          data.append('observaciones', observaciones)
          data.append('email', email)
          data.append('password', password)
          api.post('admon/saveUsers',data).then(response=>{

          if (response.success) {
            console.log("entrooo");
            if(id == null){
              util.MessageSuccess('Se guardó con exito');
              goBack();

            }
            else{
              util.MessageSuccess('Editado con exito');
              goBack();
            }
          }
          else {
            util.MessageError('Ocurrio un erro');
          }

          }).catch(err => console.warn())

        }

      }


  return(

    <div>
      <div className="flex mb-3">
        {
          id > 0 ?
          <h2 className="text-gray-800 text-3xl font-semibold ml-3">Editar Usuarios</h2>
          :
          <h2 className="text-gray-800 text-3xl font-semibold ml-3">Registrar Usuario</h2>
        }
      </div>

      <div className="overflow-auto h-5/6 ...">

        <div className="grid grid-cols-1 sm:grid-cols-2 mt-2 gap-4 px-3">
              <div className="">
                 <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="nombre">
                   Nombre completo
                 </label>
                 <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                   value={nombre}
                   onChange={(event)=>setNombre(event.target.value)}
                   type="text" placeholder="Nombre completo"
                   id="nombre"
                   name="nombre"
                  />
              </div>
             <div className="">
                <label className="block text-gray-700 text-sm font-bold mb-2">
                     Documento de identidad
                 </label>
                  <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                    type="number" placeholder="Documento de identidad"
                    value = {documento}
                    onChange = {(e)=>setDocumento(e.target.value)}
                    />
             </div>
        </div>

        <div className="grid grid-cols-1 sm:grid-cols-2 mt-2 gap-4 px-3">
            <div className="">
               <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="username">
                  Departamento
               </label>
               <Select
               className="shadow block text-gray-700 text-sm font-bold mb-2"
               value={departamento}
               closeMenuOnSelect={true}
               components={animatedComponents}
               options={departamentoSelect}
               onChange={(e)=>filtrado(e)}
               placeholder= "Seleccionar departamento"
               />
            </div>
            <div className="">
               <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="username">
                  Ciudad / Municipio
               </label>
               <Select
               className="shadow block text-gray-700 text-sm font-bold mb-2"
               value={ciudad}
               closeMenuOnSelect={true}
               components={animatedComponents}
               options={ciudadSelect}
               onChange={(e)=>setCiudad(e)}
               placeholder= "Seleccionar ciudad/municipio"
               />
            </div>
        </div>

        <div className="grid grid-cols-1 sm:grid-cols-2 mt-2 gap-4 px-3">

              <div className="">
                 <label className="block text-gray-700 text-sm font-bold mb-2" >
                    Dirección
                 </label>
                 <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                   type="text" placeholder="Dirección"
                   value = {direccion}
                   onChange = {(e)=>setDireccion(e.target.value)}
                   />
              </div>
              <div className="">
                 <label className="block text-gray-700 text-sm font-bold mb-2" >
                    Barrio
                 </label>
                 <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                   type="text" placeholder="Barrio"
                   value = {barrio}
                   onChange = {(e)=>setBarrio(e.target.value)}
                   />
              </div>
        </div>


        <div className="grid grid-cols-1 sm:grid-cols-2 mt-2 gap-4 px-3">

              <div className="">
                 <label className="block text-gray-700 text-sm font-bold mb-2" >
                      Email
                  </label>
                   <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                     value = {email}
                     onChange = {(e)=>setEmail(e.target.value)}
                     type="email" placeholder="Email"/>
              </div>
              <div className="">
                 <label className="block text-gray-700 text-sm font-bold mb-2" >
                    Celular
                 </label>
                 <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                   type="number" placeholder="Celular"
                   value = {celular}
                   onChange = {(e)=>setCelular(e.target.value)}
                   />
              </div>
        </div>

        <div className="grid grid-cols-1 sm:grid-cols-2 mt-2 gap-4 px-3">

              <div className="">
                 <label className="block text-gray-700 text-sm font-bold mb-2" >
                      Teléfono
                  </label>
                   <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                     value = {telefono}
                     onChange = {(e)=>setTelefono(e.target.value)}
                     type="number" placeholder="Teléfono"/>
              </div>
              <div className="">
                 <label className="block text-gray-700 text-sm font-bold mb-2" >
                    Fecha nacimiento
                 </label>
                 <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                   type="date" placeholder="Fecha nacimiento"
                   value = {fechaNacimiento}
                   onChange = {(e)=>setFechaNacimiento(e.target.value)}
                   />
              </div>
        </div>

        <div className="grid grid-cols-1 sm:grid-cols-2 mt-2 gap-4 px-3">
             <div className="">
                <label className="block text-gray-700 text-sm font-bold mb-2" >
                     Fecha contratación
                 </label>
                  <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                    type="date" placeholder="Fecha contratación"
                    value = {fechaContratacion}
                    onChange = {(e)=>setFechaContratacion(e.target.value)}
                    />
             </div>
             <div className="">
                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="username">
                   Posición
                </label>
                <Select
                className="shadow block text-gray-700 text-sm font-bold mb-2"
                value={posicion}
                closeMenuOnSelect={true}
                components={animatedComponents}
                options={posicionSelect}
                onChange={(e)=>setPosicion(e)}
                placeholder= "Seleccionar posición"
                />
             </div>
        </div>

        <div className="grid grid-cols-1 sm:grid-cols-3 mt-2 gap-4 px-3">

            <div className="">
               <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="username">
                  Estado
               </label>
               <Select
               className="shadow block text-gray-700 text-sm font-bold mb-2"
               closeMenuOnSelect={true}
               components={animatedComponents}
               value={
                 estadoEmpleado == "" ? estadoSelect[0] : estadoEmpleado
               }
               options={estadoSelect}
               onChange={(e)=>setEstadoEmpleado(e)}
               placeholder= "Seleccionar estado"
               />
            </div>
            <div className="">
               <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="username">
                  Genero
               </label>
               <Select
               className="shadow block text-gray-700 text-sm font-bold mb-2"
               value={genero}
               closeMenuOnSelect={true}
               components={animatedComponents}
               options={generoSelect}
               onChange={(e)=>setGenero(e)}
               placeholder= "Seleccionar genero"
               />
            </div>

            {
              id > 0 ?
                null
                :
                <div className="">
                   <label className="block text-gray-700 text-sm font-bold mb-2" >
                      Password
                   </label>
                   <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                     type="text" placeholder="Password"
                     value = {password}
                     onChange = {(e)=>setPassword(e.target.value)}
                     />
                </div>
            }
       </div>

       <div className="grid grid-cols-1 mt-2 px-3">
              <div className="">
                 <label className="block text-gray-700 text-sm font-bold mb-2" >
                    Observaciones
                 </label>
                 <textarea className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                 rows="5" cols="147"
                 value = {observaciones}
                 onChange = {(e)=>setObservaciones(e.target.value)}
                 >
                 </textarea>
              </div>
        </div>

        <div className="mt-5 p-2 flex mr-3 justify-content-between">
            <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full" onClick={()=>goBack()} >
              Regresar
            </button>
            <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full" onClick={()=>saveUsers()} >
              Guardar
            </button>
        </div>

      </div>

    </div>
  );


}
export default NewUsers;
