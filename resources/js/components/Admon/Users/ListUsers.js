import React,{ useState, useEffect} from 'react';
import ReactDOM from 'react-dom';
import '../../../../css/app.css';
import Swal from 'sweetalert2';
import api from '../../Servis/Api';
import { Button, Modal } from 'react-bootstrap';
import util from "../../means/util";
import Pagination from '../../means/paginate';

function ListUsers ({
    dataUpdate
  }){

  const [ text, setText ] = useState('')
  const [ listUsers, setListUsers ] = useState([])
  const [ listContenido, setListContenido ] = useState([])
  const [ posicion, setPosicion ] = useState("")
  const [ posicionSelect, setPosicionSelect ] = useState([])
  ///variables de paginador
  const [ listUserBack, setListUserBack ] = useState([])
  const [ currentPage, setCurrentPage ] = useState(1)
  const [ perPage, setPerPage ] = useState(6)
  /// opciones modal
  const [ listDetalle, setListDetalle] = useState([])
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  /// obtener datos de los clientes
  useEffect(()=>{
    obtain_Users()
  },[])

  const obtain_Users = () =>{
    api.get('admon/getUsers').then(response =>{
      let res = response.data
      setListUsers(res)
      setListUserBack(res)
      setListContenido(res)
      }).catch(error => console.error("Error: ", error))
  }

  const updateCurrentPage = async (number) => {
    await setListUsers([])
    await setCurrentPage(number)
    const dataNew = util.paginate(listUserBack,number,perPage)
    await setListUsers(dataNew)
  }

  ///funcion para filtrar datos de clientes en la tabla
  const filterOrderText = (event) => {
      var text = event.target.value
      const data = listContenido
      const newData = data.filter(function(item){
          var textAll = " "
          textAll = textAll+" "+item.name.toUpperCase()
          textAll = textAll+" "+item.iddoc
          textAll = textAll+" "+item.posicion.cargo.toUpperCase()
          textAll = textAll+" "+item.birth_date
          textAll = textAll+" "+item.status
          const textData = text.toUpperCase()
          return textAll.indexOf(textData) > -1
      })
      setListUsers(newData)
      setText(text)
    }

  ///funcion editar que pasa el valor a la funcion dataUpdate
    const editar = (data)=>{
      dataUpdate(data);
    }

    const detalles = (data) =>{
      handleShow();
      setListDetalle(data);z
    }

  return(
    <div className="mt-4">
      <div className="flex justify-content-center mb-4">
        <div className="col-6">
          <input className="text-center shadow appearance-none border rounded-full w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
          type="text" placeholder="Buscar usuario"
          value = {text}
          onChange={(e) => filterOrderText(e)}
          />
        </div>
      </div>
      <div className="overflow-auto h-5/6 ...">
          <section className="container mx-auto p-6 font-mono">
            <div className="w-full mb-8 overflow-hidden rounded-lg shadow-lg">
              <div className="w-full overflow-x-auto">
              <table className="w-full">
                <thead className="">
                  <tr className="text-md font-semibold tracking-wide text-left text-gray-900 bg-blue-400 uppercase border-b border-gray-600">
                    <th className="px-3 py-3 w-32 text-center">Nombre</th>
                    <th className="px-2 py-3 text-center">Documento</th>
                    <th className="px-2 py-3 text-center">Celular / Teléfono</th>
                    <th className="px-2 py-3 text-center">Posición</th>
                    <th className="px-1 py-3 text-center text-center">Fecha Cumpleaños</th>
                    <th className="px-2 py-3 text-center">Estado</th>
                    <th className="px-2 py-3 text-center">Detalles</th>
                    <th className="px-2 py-3 text-center">Acciones</th>
                  </tr>
                </thead>
                <tbody className="bg-white">
                  {
                    listUsers.map((item, index)=>{

                      return(
                        <tr className="text-gray-700" key={index}>
                          <td className="px-2 py-3 text-sm border">
                            <div className="flex items-center text-sm">
                                <p className="font-semibold text-black">{item.name}</p>
                            </div>

                          </td>
                          <td className="px-2 py-3 text-ms font-semibold border border text-center">{item.iddoc}</td>
                          <td className="px-0 py-3 text-sm border text-center">
                            <span className="px-2 py-1 font-semibold leading-tight text-green-700 bg-green-100 rounded-sm">{item.movil}</span>
                          </td>
                          <td className="px-3 py-3 text-sm border border text-center">{item.posicion.cargo}</td>
                          <td className="px-2 py-3 text-sm text-center border border text-center">{item.birth_date}</td>
                          <td className="px-3 py-3 text-sm text-center border border text-center">{item.status}</td>
                          <td className="px-2 py-3 text-sm border text-center">
                            <span className="material-icons cursor-pointer" onClick={()=>detalles(item)}>
                              <span className="material-icons">
                              visibility
                              </span>
                            </span>
                          </td>
                          <td className="px-0 py-3 text-sm border text-center border">
                            <span className="material-icons cursor-pointer" onClick={()=>editar(item)}>
                            edit
                            </span>
                          </td>
                        </tr>
                      )
                    })
                  }
                </tbody>
              </table>
              <div className="d-flex col-md-12 col-12 justify-content-end mt-2">
                <Pagination currentPage={currentPage} perPage={perPage} countdata={listUserBack.length} updateCurrentPage={(number)=>updateCurrentPage(number)}/>
              </div>
              </div>
            </div>
          </section>
      </div>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Detalles cliente</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="container">
            <p><strong>Nombre: </strong> {listDetalle.name}</p>
            <p><strong>Documento:</strong> {listDetalle.iddoc}</p>
            <p><strong>Género:</strong> {listDetalle.genero == 'F' ? 'Femenimo' : 'Masculino'}</p>
            <p><strong>Dirección:</strong> {listDetalle.address}</p>
            {
              listDetalle.departament ?
              <div>
                <p><strong>Ciudad:</strong> {listDetalle.municipio.municipio == null ? "" :listDetalle.municipio.municipio}</p>
                <p><strong>Departamento:</strong> {listDetalle.departament.departamento == null ? "" :listDetalle.departament.departamento}</p>
              </div>
              :null
            }
            {
              listDetalle.posicion ?
              <div>
                <p><strong>Cargo:</strong> {listDetalle.posicion.cargo == null ? "" :listDetalle.posicion.cargo}</p>
              </div>
              :null
            }
            <p><strong>Celular:</strong> {listDetalle.movil}</p>
            <p><strong>Teléfono:</strong> {listDetalle.phone}</p>
            <p><strong>Cumpleaños:</strong>{listDetalle.birth_date}</p>
            <p><strong>Fecha de contrato:</strong>{listDetalle.hired_date}</p>
            <p><strong>Correo:</strong> {listDetalle.email}</p>
            <p><strong>Observaciones:</strong> {listDetalle.notes}</p>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Cerrar
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  )


}
export default ListUsers;
