import React,{ useState, useEffect} from 'react';
import ReactDOM from 'react-dom';
import '../../../../css/app.css';
import axios from 'axios';
import Swal from 'sweetalert2';
import api from '../../Servis/Api';
import Form from './Form';
import List from './List'


function Index (){
  /// Variables
  const [ shoform, setShoForm ] = useState(1)
  const [ id, setId] = useState(null)
  const [ data, setData] = useState({})

  ///regresar a la pagina principal
  const goBack=() => {
    setShoForm(1)
    setId(null)
    setData({})
  }

  ///funcion para pasar datos a los input para editar
  const dataUpdate = (data) =>  {
    var marca = data.marca
    var articulo = data.articulo
    var linea = data.linea
    var modelo = data.modelo
    var serie = data.serie
    var observacion = data.observaciones

    const dataForAll = {
      marca,
      articulo,
      linea,
      modelo,
      observacion,
      serie
    }

    setId(data.id)
    setData(dataForAll)
    setShoForm(2)
  }

    return(
      <div className="py-4 px-8 bg-white shadow-lg rounded-lg ">
        <div className="flex justify-content-end mr-7 mt-3">

          {
            shoform == 1 ?
            <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full" onClick={()=>setShoForm(2)}>Crear dispositivo</button>
            :null
          }
        </div>

        {
          shoform == 1 ?
          <List dataUpdate={(data)=> dataUpdate(data)}/>
          :shoform == 2 ?
          <Form  list={data} id={id} goBack={()=> goBack()} />
          :null
        }
      </div>

    );
}
export default Index;
if (document.getElementById('Index')) {
    ReactDOM.render(<Index />, document.getElementById('Index'));
}
