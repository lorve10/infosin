import React,{ useState, useEffect} from 'react';
import '../../../../css/app.css';
import axios from 'axios';
import Swal from 'sweetalert2';
import api from '../../Servis/Api';
import util from "../../means/util";
import Pagination from '../../means/paginate';



function List ({
    dataUpdate
}){
  ///Variables
  const [ text, setText ] = useState('')
  const [ listDevice, setlistDevice ] = useState([])
  const [ listContenido, setListContenido ] = useState([])
  ///variables para paginador
  const [ listDeviceBack, setListDeviceBack ] = useState([])
  const [ currentPage, setCurrentPage] = useState(1)
  const [ perPage, setPerPage] = useState(6)

  /// obtener datos de los clientes, se ejecuta automaticamente la consulta, solo una vez
  useEffect(()=>{
    obtain_Devices()
  },[])

  const obtain_Devices = async () =>{
  api.get('admon/getDevices')
    .then(response=>{
      let res = response.data;
      console.log(res);
      setlistDevice(res)
      setListDeviceBack(res)
      setListContenido(res)
    })
  }

  const updateCurrentPage = async (number) => {
    await setlistDevice([])
    await setCurrentPage(number)
    const dataNew = util.paginate(listDeviceBack,number,perPage)
    await setlistDevice(dataNew)
  }


  ///funcion para filtrar datos de clientes en la tabla
  const filterOrderText = (event) => {
      var text = event.target.value
      const data = listContenido

      const newData = data.filter(function(item){
          var textAll = " "

          textAll = textAll+" "+item.brands.marca
          textAll = textAll+" "+item.items.articu.toUpperCase()
          textAll = textAll+" "+item.oline.line.toUpperCase()
          textAll = textAll+" "+item.modelo.toUpperCase()
          textAll = textAll+" "+item.serie.toUpperCase()

          const textData = text.toUpperCase()
          return textAll.indexOf(textData) > -1
      })
      setlistDevice(newData)
      setText(text)
    }

///funcion editar que pasa el valor a la funcion dataUpdate
  const editar = (data)=>{
    dataUpdate(data);
  }

  return(
    <div className="mt-4">
      <div className="flex justify-content-center mb-4">
        <div className="col-6">
          <input className="text-center shadow appearance-none border rounded-full w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
          type="text" placeholder="Buscar dispositivo"
          value = {text}
          onChange={(e) => filterOrderText(e)}
          />
        </div>
      </div>

      <div className="">
          <section className="container mx-auto p-6 font-mono">
            <div className="w-full mb-8 overflow-hidden rounded-lg shadow-lg">
              <div className="w-full overflow-x-auto">
              <table className="w-full ">
                <thead className="">
                  <tr className="text-md font-semibold tracking-wide text-center text-gray-900 bg-blue-400 uppercase border-b border-gray-600">
                    <th className="px-2 py-3">Marca</th>
                    <th className="px-2 py-3">Articulo</th>
                    <th className="px-2 py-3">Linea</th>
                    <th className="px-2 py-3">Modelo</th>
                    <th className="px-2 py-3">Serie</th>
                    <th className="px-2 py-3">Acciones</th>
                  </tr>
                </thead>
                <tbody className="bg-white">

                  {
                    listDevice.map((item,index)=>{
                      return(
                        <tr className="text-gray-700" key={item.id}>

                          <td className="px-2 py-3 text-ms font-semibold border text-center">{item.brands.marc}
                          </td>
                          <td className="px-2 py-3 text-ms border text-center">
                            {item.items.articu}
                          </td>
                          <td className="px-2 py-3 text-sm border text-center">
                            {item.oline.line}
                          </td>
                          <td className="px-2 py-3 text-sm border text-center">{item.modelo}</td>
                          <td className="px-2 py-3 text-sm border text-center">{item.serie}</td>
                          <td className="px-0 py-2 text-sm border text-center">
                            <span className="material-icons cursor-pointer" onClick={()=>editar(item)}>
                            edit
                            </span>
                          </td>
                        </tr>
                      );
                    })
                  }
                </tbody>
              </table>
              <div className="d-flex col-md-12 col-12 justify-content-end mt-2">
                <Pagination currentPage={currentPage} perPage={perPage} countdata={listDeviceBack.length} updateCurrentPage={(number)=>updateCurrentPage(number)}/>
              </div>
              </div>
            </div>

          </section>

      </div>

    </div>
  );

}
export default List;
