import React,{ useState, useEffect} from 'react';
import ReactDOM from 'react-dom';
import '../../../../css/app.css';
import Swal from 'sweetalert2';
import api from '../../Servis/Api';
import Select from 'react-select';
import makeAnimated from 'react-select/animated';
import util  from '../../means/util';



function Form ({ id, goBack, list }){
  /// Variables del formulario
  const [ marca, setMarca ] = useState(null)
  const [ marcaSelect, setMarcaSelect ] = useState([])
  const [ articulo, setArticulo ] = useState(null)
  const [ articuloSelect, setArticuloSelect ] = useState([])
  const [ lineaSelect, setLineaSelect ] = useState([])
  const [ linea, setLinea ] = useState(null)
  const [ modelo, setModelo ] = useState("")
  const [ serie, setSerie ] = useState("")
  const [ observacion, setObservacion ] = useState("")
  const [animatedComponents, setAnimatedComponents] = useState(makeAnimated);
  const [ load, setLoad ] = useState(false);
  const [ cliente, setCliente] = useState([])
  const [ clienteSelect, setClienteSelect ] = useState([])



  useEffect(()=>{
    if(id > 0){
      setModelo(list.modelo)
      setSerie(list.serie)
      setObservacion(list.observacion)
      console.log(list);
    }
  },[id])

  useEffect(()=>{
    obtain_marca()
    obtain_items()
    obtain_linea()
    obtain_cliente()
  },[])


  // const obtain_cliente = async () =>{
  //   api.get('admon/getCustDiv')
  //     .then(data=>{
  //       let res = data;
  //       let cliente = [];
  //       let filtro = res.filter(e=>e.persona.id )
  //       res.map(item=>{
  //          const data = { value:item.persona.id, label:item.persona.cust_id_doc + ' - ' + item.persona.cust_nombre }
  //          cliente.push(data)
  //          setClienteSelect(cliente)
  //      })
  //   })
  // }

  const obtain_cliente = async () =>{
    api.get('admon/getCustomers')
    .then(response=>{
      let cliente = [];
      var res = response.data;

      res.map(item=>{
          const data = { value:item.id, label:item.cust_id_doc + ' - ' + item.cust_nombre }
          cliente.push(data)
          setClienteSelect(cliente)


      }).catch(error => console.error("Error: ", error))
    })
    if (id > 0) {
      api.get('admon/getCustDiv')
        .then(response=>{
          let marca = [];
          var res = response.data;
          let filtrado = res.filter(item =>item.device_id == id);
          filtrado.map(item=>{
            const data = {  value:item.persona.id, label:item.persona.cust_id_doc + ' - ' + item.persona.cust_nombre}
            setCliente(data)
          })

      }).catch(error => console.error("Error: ", error))
    }
    /// consuslta para validar id del cliente tabla pibote

  }

  const obtain_marca = async () =>{
    api.get('admon/get')
    .then(response=>{
        let marca = [];
        var res = response.data;
        let filtro = res.filter(e=>e.tipo == 1);
        filtro.map(item=>{
          const data = {
            value:item.id,
            label:item.marc
          }
          marca.push(data)
          setMarcaSelect(marca)
        })
        if (id > 0) {
           var filtroMarca = filtro.filter(e=>e.id == list.marca)
          const data = { value: filtroMarca[0].id, label: filtroMarca[0].marc }
          setMarca(data)
        }
    }).catch(error => console.error("Error: ", error))
  }

  const obtain_items = () =>{
    api.get('admon/getArticulo')
    .then(response=>{

        let articulo = [];
        var res = response.data
        res.map(item=>{
          const data = { value:item.id, label:item.articu }
          articulo.push(data);
          setArticuloSelect(articulo)
        })
        if (id > 0) { var filtroArticulo= res.filter(e=>e.id == list.articulo)
          const data = { value: filtroArticulo[0].id, label: filtroArticulo[0].articu }
          setArticulo(data)
        }
    }).catch(error => console.error("Error: ", error))
  }

  /// Funcion para traer lineas
  const obtain_linea = () =>{
    api.get('admon/getLinea')
    .then(response=>{
      let linea = [];
      var res = response.data
      res.map(item=>{
        const data = { value:item.id,  label:item.line }
        linea.push(data);
        setLineaSelect(linea)
      })
      if (id > 0) {
        var filtroLinea = res.filter(e=>e.id == list.linea)
        const data = { value: filtroLinea[0].id, label: filtroLinea[0].line }
        setLinea(data)
      }
    }).catch(error => console.error("Error: ", error))
  }

///funcion para guardar datos
    const saveDevices = async () => {
      var message = ''
      var error = false
      setLoad(true)
      if(cliente.length == 0){
        error = true
        message = "Debes seleccionar un cliente"
      }
      else if( marca == null){
        error = true
        message = "Debes agregar una marca"
      }
      else if ( articulo == null){
        error = true
        message = "Debes agregar un articulo"
      }
      else if (linea == null){
        error = true
        message = "Debes ingresar una linea"
      }
      else if (modelo == ''){
        error = true
        message = "Debes agregar un modelo"
      }
      else if (serie == ''){
        error = true
        message = "Debes agregar una serie"
      }
      else if (observacion == ''){
        error = true
        message = "Debes agregar una observacion"
      }
      if (error){
        util.MessageError(message)
      }
      else {

        const data = new FormData()
        data.append('id', id)
        data.append('marca', marca.value)
        data.append('articulo', articulo.value)
        data.append('linea', linea.value)
        data.append('modelo', modelo)
        data.append('serie', serie)
        data.append('observacion', observacion)
        data.append('customerId', cliente.value)
        api.post('admon/saveDevices',data).then(response=>{
          if (response.success) {
            if(id == null){
              util.MessageSuccess('Se guardó con exito')
              goBack();
              setLoad(false)
            }
            else{
              util.MessageSuccess('Editado con exito')
              goBack();
              setLoad(false)
            }
          }
          else{
            console.log("erro");
          }

        }).catch(err => console.warn(err))
      }
    }

    return(

      <div >
        <div className="flex mb-3">
        {
          id > 0 ?
          <h2 className="text-gray-800 text-3xl font-semibold ml-3">Editar dispositivo</h2>
          :
          <h2 className="text-gray-800 text-3xl font-semibold ml-3">Registrar dispositivo</h2>
        }
        </div>

        <div className="overflow-auto h-5/6 ...">
            <div className="grid grid-cols-1 sm:grid-cols-2 mt-2 gap-4 px-3">
                  <div className="">
                     <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="username">
                        Cliente
                     </label>
                     <Select
                     className="shadow block text-gray-700 text-sm font-bold mb-2"
                     value={cliente}
                     closeMenuOnSelect={true}
                     components={animatedComponents}
                     options={clienteSelect}
                     onChange={(e)=>setCliente(e)}
                     placeholder= "Seleccionar cliente"
                     />
                  </div>
            </div>
              <div className="grid grid-cols-1 sm:grid-cols-2 mt-2 gap-4 px-3">

                  <div className="">
                     <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="username">
                       Marca
                     </label>
                     <Select
                      className="shadow block text-gray-700 text-sm font-bold mb-2"
                      value={marca}
                      closeMenuOnSelect={true}
                      components={animatedComponents}
                      options={marcaSelect}
                      onChange={(e)=>setMarca(e)}
                      placeholder = "Seleccionar marca"
                      />
                  </div>
                 <div className="">
                    <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="password">
                         Articulo
                     </label>
                     <Select
                      className="shadow block text-gray-700 text-sm font-bold mb-2"
                      value={articulo}
                      closeMenuOnSelect={true}
                      components={animatedComponents}
                      options={articuloSelect}
                      onChange={(e)=>setArticulo(e)}
                      placeholder = "Seleccionar articulo"
                      />
                 </div>
            </div>
            <div className="grid grid-cols-1 sm:grid-cols-2 mt-2 gap-4 px-3">
                  <div className="">
                     <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="username">
                        Linea
                     </label>
                     <Select
                     className=" shadow block text-gray-700 text-sm font-bold mb-2"
                     value={linea}
                     closeMenuOnSelect={true}
                     components={animatedComponents}
                     options={lineaSelect}
                     onChange={(e)=>setLinea(e)}
                     placeholder= "Seleccionar linea"
                     />

                  </div>
                 <div className="">
                    <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="password">
                         Modelo
                     </label>
                      <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                      value={modelo}
                      onChange={(event)=>setModelo(event.target.value)}
                      type="text" placeholder="Seleccionar modelo"
                      id="modelo"
                      maxLength="99"
                      name="modelo"
                      />
                 </div>
            </div>
            <div className="grid grid-cols-1 sm:grid-cols-2 mt-2 gap-4 px-3">
                  <div className="">
                     <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="username">
                        Serie
                     </label>
                     <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                     value={serie}
                     onChange={(event)=>setSerie(event.target.value)}
                     type="text" placeholder="Serie"
                     id="serie"
                     name="serie"
                     />
                  </div>

                  <div className="">
                     <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="username">
                         Observación
                     </label>
                     <textarea className="shadow appearance-none border rounded w-full py-2 px-2 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                       type="text" placeholder="Observaciones"
                       value = {observacion}
                       onChange = {(e)=>setObservacion(e.target.value)}
                       ></textarea>
                  </div>
            </div>

            <div className="mt-5 p-2 flex mr-3 justify-content-between">
                <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full" onClick={()=>goBack()}>
                  Regresar
                </button>
                <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full" onClick={()=>saveDevices()} >
                  Guardar
                </button>
            </div>
        </div>
      </div>
    );
}
 export default Form;
