import React,{ useState, useEffect} from 'react';
import ReactDOM from 'react-dom';
import '../../../../css/app.css';
import Swal from 'sweetalert2';
import api from '../../Servis/Api';
import util from "../../means/util";
import Pagination from '../../means/paginate';

export default function List ({
    dataUpdate,
}){

  const [ text, setText ] = useState('')
  const [ listCust, setListCust ] = useState([])
  const [ listContenido, setListContenido ] = useState([])
  /// variables paginador
  const [ listCustBack, setListCustBack ] = useState([])
  const [ currentPage, setCurrentPage ] = useState(1)
  const [ perPage, setPerPage ] = useState(6)


  /// obtener datos de los clientes
  useEffect(()=>{
    obtain_Customers()
  },[])

  const obtain_Customers = () =>{
    api.get('admon/getCustomers')
    .then(response =>{
      setListCust(response.data)
      setListCustBack(response.data)
      setListContenido(response.data)
    }).catch(error => console.error("Error: ", error))
  }

  const updateCurrentPage = async (number) => {
    await setListCust([])
    await setCurrentPage(number)
    const dataNew = util.paginate(listCustBack,number,perPage)
    await setListCust(dataNew)
  }


  ///funcion para filtrar datos de clientes en la tabla
  const filterOrderText = (event) => {
      var text = event.target.value
      const data = listContenido
      const newData = data.filter(function(item){
          var textAll = " "
          textAll = textAll+" "+item.cust_nombre.toUpperCase()
          textAll = textAll+" "+item.cust_id_doc.toUpperCase()
          textAll = textAll+" "+item.cust_ciudad.toUpperCase()

          const textData = text.toUpperCase()
          return textAll.indexOf(textData) > -1
      })
      setListCust(newData)
      setText(text)
    }

///funcion editar que pasa el valor a la funcion dataUpdate
  const editar = (data)=>{
    dataUpdate(data);
  }

return(
  <div className="mt-4 ">
    <div className="flex justify-content-center mb-4">
      <div className="col-6">
        <input className="text-center shadow appearance-none border rounded-full w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
        type="text" placeholder="Buscar cliente"
        value = {text}
        onChange={(e) => filterOrderText(e)}
        />
      </div>
    </div>

    <div className="overflow-auto h-5/6 ...">
        <section className="container mx-auto p-6 font-mono">
          <div className="w-full mb-8 overflow-hidden rounded-lg shadow-lg">
            <div className="w-full overflow-x-auto">
            <table className="w-full ">
              <thead className="">
                <tr className="text-md font-semibold tracking-wide text-center text-gray-900 bg-blue-400 uppercase border-b border-gray-600">
                  <th className="px-2 py-3 w-32">Nombre</th>
                  <th className="px-2 py-3">Documento</th>
                  <th className="px-2 py-3">Celular</th>
                  <th className="px-2 py-3">Dirección</th>
                  <th className="px-2 py-3">barrio</th>
                  <th className="px-2 py-3">ciudad</th>
                  <th className="px-2 py-3">Acciones</th>
                </tr>
              </thead>
              <tbody className="bg-white">
                {
                  listCust.map((item,index)=>(
                      <tr className="text-gray-700" key={index}>
                        <td className="px-2 py-3 border">
                          <div className="flex items-center text-sm">
                              <p className="font-semibold text-black">{item.cust_nombre}</p>
                          </div>
                        </td>
                        <td className="px-2 py-3 text-center text-ms font-semibold border">{item.cust_id_doc}</td>
                        <td className="px-2 py-3 text-xs border text-center">
                          <span className="px-2 py-1 font-semibold leading-tight text-green-700 bg-green-100 rounded-sm">{item.cust_celular}</span>
                        </td>
                        <td className="px-2 py-3 text-sm border">{item.cust_direccion}</td>
                        <td className="px-2 py-3 text-sm border">{item.cust_barrio}</td>
                        <td className="px-2 py-3 text-sm border text-center">
                          {
                            item.municipio == null ? ""
                            :item.municipio.municipio
                          }
                        </td>
                        <td className="px-0 py-2 text-sm border text-center">
                          <span className="material-icons cursor-pointer" onClick={()=>editar(item)}>
                          edit
                          </span>
                        </td>
                      </tr>
                  ))
                }
              </tbody>
            </table>
            <div className="d-flex col-md-12 col-12 justify-content-end mt-2">
              <Pagination currentPage={currentPage} perPage={perPage} countdata={listCustBack.length} updateCurrentPage={(number)=>updateCurrentPage(number)}/>
            </div>
            </div>
          </div>
        </section>
    </div>
  </div>
)
}
