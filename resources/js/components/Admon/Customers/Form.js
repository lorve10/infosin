import React,{ useState, useEffect} from 'react';
import ReactDOM from 'react-dom';
import '../../../../css/app.css';
import axios from 'axios';
import Swal from 'sweetalert2';
import api from '../../Servis/Api';
import util  from '../../means/util';
import Select from 'react-select';
import makeAnimated from 'react-select/animated';

export default function Customers({
  id,
  list,
  goBack
}) {
  ///variables del formulario
   const [ nombre, setNombre ] = useState("")
   const [ documento, setDocumento ] = useState("")
   const [ direccion, setDireccion ] = useState("")
   const [ barrio, setBarrio ] = useState("")
   const [ ciudad, setCiudad ] = useState("")
   const [ departamento, setDepartamento ] = useState("")
   const [ departamentoSelect, setDepartamentoSelect ] = useState([])
   const [ ciudadSelect, setCiudadSelect ] = useState([])
   const [animatedComponents, setAnimatedComponents] = useState(makeAnimated);
   const [ email, setEmail ] = useState("")
   const [ celular, setCelular ] = useState("")
   const [ teleCasa, setTeleCasa ] = useState("")
   const [ teleTrabajo, setTeleTrabajo] = useState("")
   const [ token, setToken] = useState('')
   useEffect(()=>{
     let csrf = document.querySelector('meta[name="csrf-token"]').content;
     setToken(csrf);
   },[])

   useEffect(()=>{
     obtain_departamento()
     if(id > 0){
       setNombre(list.nombre)
       setDocumento(list.documento)
       setDireccion(list.direccion)
       setBarrio(list.barrio)
       setCiudad(list.ciudad)
       setDepartamento(list.departamento)
       setEmail(list.email)
       setCelular(list.celular)
       setTeleCasa(list.teleCasa)
       setTeleTrabajo(list.teleTrabajo)
     }
   },[id])

   // Funcion para obtener los departamentos y listarlos en el select
    const obtain_departamento = async () =>{
      api.get('admon/getDepartamento')
      .then(response=>{
          let departamento = [];
          let res = response.data;
          res.map(item=>{
            const data = { value:item.id_departamento, label:item.departamento }
            departamento.push(data)
            setDepartamentoSelect(departamento)
          })

          if (id > 0 ){
            let filtroDepart = res.filter(e=>e.id_departamento == list.departamento)
            console.log(filtroDepart);
            const data = {
              value: filtroDepart[0].id_departamento,
              label: filtroDepart[0].departamento
            }
            setDepartamento(data)
            obtain_ciudad(data.value, true)
          }
      }).catch(error => console.warn())
    }


   /// funcion de filtrado por departamento
   const filtrado = (value) => {
       setDepartamento(value);
       obtain_ciudad(value.value, true)
   }


   // Funcion para obtener las ciudades y listarlos en el select
    const obtain_ciudad = async (valor, data) =>{
      setCiudadSelect([])
      api.get('admon/getMunicipio')
      .then(response=>{
        let res = response.data;
        let ciudad = [];
        let filtroDepart = response.data.filter((e)=>e.departamento_id === valor);
        filtroDepart.map(item=>{
          const data = {
            value:item.id_municipio,
            label:item.municipio
          }
          ciudad.push(data)
          setCiudadSelect(ciudad)
        })
        if (id > 0 ){
          let filtroCiudad = res.filter(f=>f.id_municipio == list.ciudad)
          const data = {
            value: filtroCiudad[0].id_municipio,
            label: filtroCiudad[0].municipio
          }
          setCiudad(data)
        }
      }).catch(error =>console.warn())
    }



    ///funcion para guardar datos
  const onClickLogin = async () => {

    let message = ''
    let error = false

    if(nombre == ''){
      error = true
      message = "Debes agregar el nombre"
    }
    else if (documento == '') {
      error = true
      message = "Debes agregar el Numero de documento"
    }
    else if (barrio == '') {
      error = true
      message = "Debes agregar el barrio"
    }
    else if (ciudad == '') {
      error = true
      message = "Debes agregar la ciudad"
    }
    else if (departamento == '') {
      error = true
      message = "Debes agregar un departamento"
    }
    else if (email == '') {
      error = true
      message = "Debes agregar un correo"
    }
    else if (celular == '') {
      error = true
      message = "Debes agregar el Numero de celular"
    }
    if (error) {
      util.MessageError(message)
    }
    else {
      const data = new FormData()
      data.append('id', id)
      data.append('nombre', nombre)
      data.append('documento', documento)
      data.append('direccion', direccion)
      data.append('barrio', barrio)
      data.append('ciudad', ciudad.value)
      data.append('departamento', departamento.value)
      data.append('celular', celular)
      data.append('email', email)
      data.append('teleCasa', teleCasa)
      data.append('teleTrabajo', teleTrabajo)
      api.post('admon/saveCustomers',data).then(response=>{

        if (response.success) {
          if(id == null){
            util.MessageSuccess("Cliente registrado exitosamente")
            goBack();
          }
          else{
            util.MessageSuccess("Editado Correctamente")
            goBack();
          }
        }
        else {
          util.MessageError("Ocurrio un erro");
        }


      }).catch(err => console.warn(err))
    }
  }

  return(
    <div >
      <input type="hidden" name="_token" value={token} />

      <div className="flex mb-3">
        {
          id > 0 ?
          <h2 className="text-gray-800 text-3xl font-semibold ml-3">Editar cliente</h2>
          :
          <h2 className="text-gray-800 text-3xl font-semibold ml-3">Registrar cliente</h2>
        }
      </div>
      <div className="overflow-auto h-5/6 ...">
          <div className="grid grid-cols-1 sm:grid-cols-2 mt-2 gap-4 px-3 ">
                <div className="">
                   <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="nombre">
                     Nombre
                   </label>
                   <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                     value={nombre}
                     onChange={(event)=>setNombre(event.target.value)}
                     type="text" placeholder="Nombre"
                     id="nombre"
                     name="nombre"
                    />
                </div>
               <div className="">
                  <label className="block text-gray-700 text-sm font-bold mb-2">
                       Documento de identidad / Nit
                   </label>
                    <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                      type="number" placeholder="Documento de identidad / Nit"
                      value = {documento}
                      onChange = {(e)=>setDocumento(e.target.value)}
                      />
               </div>
          </div>
          <div className="grid grid-cols-1 sm:grid-cols-2 mt-2 gap-4 px-3">
                <div className="">
                   <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="username">
                      Dirección
                   </label>
                   <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                     type="text" placeholder="Dirección"
                     value = {direccion}
                     onChange = {(e)=>setDireccion(e.target.value)}
                     />
                </div>
               <div className="">
                  <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="password">
                       Barrio
                   </label>
                    <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                      type="text" placeholder="Barrio"
                      value = {barrio}
                      onChange = {(e)=>setBarrio(e.target.value)}
                      />
               </div>
          </div>

          <div className="grid grid-cols-1 sm:grid-cols-2 mt-2 gap-4 px-3">
              <div className="">
                 <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="username">
                    Departamento
                 </label>
                 <Select
                 className="shadow block text-gray-700 text-sm font-bold mb-2"
                 value={departamento}
                 closeMenuOnSelect={true}
                 components={animatedComponents}
                 options={departamentoSelect}
                 onChange={(e)=>filtrado(e)}
                 placeholder= "Seleccionar departamento"
                 />
              </div>
              <div className="">
                 <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="username">
                    Ciudad / Municipio
                 </label>
                 <Select
                 className="shadow block text-gray-700 text-sm font-bold mb-2"
                 value={ciudad}
                 closeMenuOnSelect={true}
                 components={animatedComponents}
                 options={ciudadSelect}
                 onChange={(e)=>setCiudad(e)}
                 placeholder= "Seleccionar ciudad/municipio"
                 />
              </div>
          </div>

          <div className="grid grid-cols-1 sm:grid-cols-2 mt-2 gap-4 px-3">

               <div className="">
                  <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="password">
                       Email
                   </label>
                    <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                      value = {email}
                      onChange = {(e)=>setEmail(e.target.value)}
                      type="email" placeholder="Correo"/>
               </div>
               <div className="">
                  <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="username">
                     Celular
                  </label>
                  <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                    type="number" placeholder="Celular"
                    value = {celular}
                    onChange = {(e)=>setCelular(e.target.value)}
                    />
               </div>
          </div>
          <div className="grid grid-cols-1 sm:grid-cols-2 mt-2 gap-4 px-3">
               <div className="">
                  <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="password">
                       Teléfono casa
                   </label>
                    <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                      type="number" placeholder="Teléfono casa"
                      value = {teleCasa}
                      onChange = {(e)=>setTeleCasa(e.target.value)}
                      />
               </div>
               <div className="">
                  <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="username">
                     Teléfono trabajo
                  </label>
                  <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                    type="number" placeholder="Teléfono trabajo"
                    value = {teleTrabajo}
                    onChange = {(e)=>setTeleTrabajo(e.target.value)}
                    />
               </div>
          </div>
          <div className="mt-5 p-2 flex mr-3 justify-content-between">
              <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full" onClick={()=>goBack()}>
                Regresar
              </button>
              <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full" onClick={()=>onClickLogin()} >
                Guardar
              </button>
          </div>
      </div>

    </div>
  );

}
