import React,{ useState, useEffect} from 'react';
import ReactDOM from 'react-dom';
import '../../../../css/app.css';
import Swal from 'sweetalert2';
import api from '../../Servis/Api';
import Form from './Form';
import List from './List';

function Customers() {

  const [ shofrom, setShofrom ] = useState(1)
  const [ id, setId ] = useState(null)
  const [ data, setData ] = useState({})

///regresar a la pagina principal
  const goBack =()=>{
    setShofrom(1)
    setId(null)
    setData({})
  }

  ///funcion para pasar datos a los input para editar
  const dataUpdate = (data) =>{

    let nombre = data.cust_nombre
    let documento = data.cust_id_doc
    let direccion = data.cust_direccion
    let barrio = data.cust_barrio
    let ciudad = data.cust_ciudad
    let departamento = data.cust_departamento
    let email = data.cust_email
    let celular = data.cust_celular
    let teleCasa = data.cust_telcasa
    let teleTrabajo = data.cust_teltrabajo

    const dataForAll = {
      nombre,
      documento,
      direccion,
      barrio,
      ciudad,
      departamento,
      email,
      celular,
      teleCasa,
      teleTrabajo
    }
    setId(data.id)
    setData(dataForAll)
    setShofrom(2)
  }

  return(
    <div className="py-4 px-8 bg-white shadow-lg rounded-lg">
      <div className="flex justify-content-end mr-7 mt-3">
        {
          shofrom == 1 ?
          <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full" onClick={()=>setShofrom(2)}>
            Crear cliente
          </button>
          :null
        }
      </div>
        {
          shofrom == 1 ?
          <List  dataUpdate ={(data)=>dataUpdate(data)} />
          :shofrom == 2 ?
          <Form  list = {data} id = {id} goBack = {()=>goBack()}/>
          :null
        }
    </div>
  );
}

export default Customers;
if (document.getElementById('Customers')) {
    ReactDOM.render(<Customers />, document.getElementById('Customers'));
}
