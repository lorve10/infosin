import React,{ useState, useEffect} from 'react';
import '../../../../css/app.css';
import axios from 'axios';
import Swal from 'sweetalert2';
import api from '../../Servis/Api';



function Property(){

    const [ ordenPendiente, setOrdenPendiente ] = useState(0);
    const [ partePendiente, setPartePendiente ] = useState(0);
    const [ ordeRealEntrega, setOrdeRealEntrega] = useState(0);
    const [loading, setLoading] = useState(false);
    const [ prueba, setPrueba ] = useState(3);
    const [ prueba2, setPrueba2 ] = useState(4);
    const [ prueba3, setPrueba3 ] = useState(5);



    useEffect(()=>{
      setLoading(true)
      ordenPendi()
      ordenRealEntre()
      partePendien()
      setLoading(false)
    },[])


    const ordenPendi = () =>{
      api.get('admon/ordenPendi')
      .then(response =>{
        setOrdenPendiente(response.data)
      }).catch(error => console.error("Error: ", error))
    }


    const partePendien = () =>{
      api.get('admon/partePendient')
      .then(response =>{
        setPartePendiente(response.data)
      }).catch(error => console.error("Error: ", error))
    }

    const ordenRealEntre = () =>{
      api.get('admon/ordenRealEntrega')
      .then(response =>{
        setOrdeRealEntrega(response.data)
      }).catch(error => console.error("Error: ", error))
    }
    return(

      <div className="grid grid-cols-1 md:grid-cols-3 mt-2 gap-4 px-20 ">

        <div className="bg-indigo-500 rounded-lg text-center py-4 text-white text-base sm:text-lg md:text-2xl" style={{background: '#111827'}} >
          <a href={`admon/ordenes?prueba=${prueba}`}>
            <div className="mb-3">
              <h3><b>Ordenes pendientes</b></h3>
            </div>
            <div className="">
              <label className="w-2/4 shadow-md text-center p-2 rounded-lg text-lg sm:text-xl md:text-2xl" style={{background: '#11182E'}}>
                 { ordenPendiente }
              </label>
            </div>
          </a>
        </div>
        <div className="bg-indigo-500 rounded-lg text-center py-4 text-white text-base sm:text-lg md:text-2xl" style={{background: '#111827'}} >
            <a href={`admon/ordenes?prueba=${prueba2}`}>
            <div className="mb-3">
              <h3><b>Ordenes realizadas por entregar</b></h3>
            </div>
            <div className="">
              <label className="w-2/4 shadow-md text-center p-2 rounded-lg text-lg sm:text-xl md:text-2xl" style={{background: '#11182E'}}>
                 { ordeRealEntrega }
              </label>
            </div>
          </a>
        </div>

        <div className="bg-indigo-500 rounded-lg text-center py-4 text-white text-base sm:text-lg md:text-2xl" style={{background: '#111827'}} >
          <a href={`admon/Repuestos?prueba=${prueba3}`}>
            <div className="mb-3">
              <h3><b>Repuestos pendientes</b></h3>
            </div>
            <div className="">
              <label className="w-2/4 shadow-md text-center p-2 rounded-lg text-lg sm:text-xl md:text-2xl" style={{background: '#11182E'}}>
                 { partePendiente}
              </label>
            </div>
          </a>
        </div>

      </div>
    );


}
export default Property;
