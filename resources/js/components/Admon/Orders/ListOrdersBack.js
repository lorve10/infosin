import React,{ useState, useEffect} from 'react';
import ReactDOM from 'react-dom';
import '../../../../css/app.css';
import Swal from 'sweetalert2';
import api from '../../Servis/Api';
import util from "../../means/util";
import { Button, Modal } from 'react-bootstrap';
import Pagination from '../../means/paginate';


function ListOrders(props){

  const [ text, setText ] = useState('')
  const [ listCust, setListCust ] = useState([])
  const [ listContenido, setListContenido ] = useState([])
  ///variables paginador
  const [ listCustBack, setListCustBack ] = useState([])
  const [ currentPage, setCurrentPage ] = useState(1)
  const [ perPage, setPerPage ] = useState(6)
  /// opciones modal
  const [ listDetalle, setListDetalle] = useState([])
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  ///_token
  const [ token, setToken ] = useState(null)
  const ip = 'http://localhost:8000/';


  useEffect(()=>{
    obtain_Orders()
  },[])

  useEffect(()=>{
    let csrf = document.querySelector('meta[name="csrf-token"]').content;
    setToken(csrf);
  })

  const obtain_Orders = async () =>{
    api.get('admon/getOrders')
      .then(data =>{
      setListCust(data)
      setListCustBack(data)
      setListContenido(data)
      console.log(data);
    }).catch(console.warn);
  }

  const updateCurrentPage = async (number) => {
    await setListCust([])
    await setCurrentPage(number)
    const dataNew = util.paginate(listCustBack,number,perPage)
    await setListCust(dataNew)
  }

///funcion para filtrar datos de Ordenes en la tabla
  const filterOrderText = (event) => {
      var text = event.target.value
      const data = listContenido

      const newData = data.filter(function(item){
          var textAll = " "
          textAll = textAll+" "+item.orden
          textAll = textAll+" "+item.nombre.toUpperCase()

          const textData = text.toUpperCase()
          return textAll.indexOf(textData) > -1
      })
      setListCust(newData)
      setText(text)
    }

///funcion editar que pasa el valor a la funcion dataUpdate
  const editar = (data)=>{
    props.dataUpdate(data);
  }

  const [estado, setEstado ] = useState([]);


//activar detalles
  const detalles = (data) =>{
    handleShow();
    setListDetalle(data);
    setEstado(data.estado);

  }


return(
  <div className="relative mt-4">

    <div className="flex justify-content-center">
      <div className="col-6">
        <input className="shadow appearance-none border rounded-full w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
        type="text" placeholder="Buscar orden"
        value = {text}
        onChange={(e) => filterOrderText(e)}
        />
      </div>
    </div>

    <section className="container mx-auto p-6 font-mono">
      <div className="w-full mb-8 overflow-hidden rounded-lg shadow-lg">
        <div className="w-full overflow-x-auto">
      <div className="mt-2">
        <table className="max-w-5xl mx-auto table-auto">
          <thead className="justify-between">
            <tr className="bg-green-600">
              <th className="px-16 py-2">
                <span className="text-gray-100 font-semibold">Orden</span>
              </th>
              <th className="px-16 py-2">
                <span className="text-gray-100 font-semibold">Tipo orden</span>
              </th>

              <th className="px-16 py-2">
                <span className="text-gray-100 font-semibold">Tipo servicio</span>
              </th>

              <th className="px-16 py-2">
                <span className="text-gray-100 font-semibold">Carácter de orden</span>
              </th>

              <th className="px-16 py-2">
                <span className="text-gray-100 font-semibold">Documento</span>
              </th>

              <th className="px-16 py-2">
                <span className="text-gray-100 font-semibold">Nombre</span>
              </th>
              <th className=" py-2">
                <span className="text-gray-100 font-semibold">Detalles</span>
              </th>
              <th className="px-16 py-2">
                <span className="text-gray-100 font-semibold">Pdf</span>
              </th>

            </tr>
          </thead>
          <tbody className="bg-gray-200">
            {
              listCust.map((item,index)=>{
                return(
                  <tr className="bg-white border-b-2 border-gray-200" key={index}>
                    <td className="text-center ml-2 font-semibold">
                      {item.orden}
                    </td>
                    <td>
                      <span className="text-center ml-2 font-semibold">{item.tipo_orden == 1? 'Garantia' :item.tipo_orden == 2 ?'Cobrable' :'Extendida' }</span>
                    </td>
                    <td className="px-16 py-2">
                      <span>{item.servi.tipo_servicio}</span>
                    </td>
                    <td className="px-16 py-2">
                      <span>{item.carat.caract_orden}</span>
                    </td>

                    <td className="px-16 py-2">
                      <span>{item.id_doc}</span>
                    </td>

                    <td className="py-2">
                      <span>{item.nombre}</span>
                    </td>
                    <td className="px-5 py-2 text-sm border">
                      <span className="material-icons cursor-pointer" onClick={()=>detalles(item)}>
                        <span className="material-icons">
                        visibility
                        </span>
                      </span>
                    </td>
                    <td className="px-16 py-2">
                      <form className="d-flex" action={ip+"admon/print/order"} method="POST" target="_blank">
                          <input type= "hidden"  name= "data" value={JSON.stringify(item)}  />
                          <input type="hidden" name="_token" value={token} />
                          <button type="submit"  className="btn " style={{borderRadius:10, color:'black'}}>  <span className="material-icons">
                              picture_as_pdf
                            </span></button>
                      </form>

                    </td>
                  </tr>
                );
              })
            }
          </tbody>
        </table>
      </div>
      <div className="d-flex col-md-12 col-12 justify-content-end mt-2">
        <Pagination currentPage={currentPage} perPage={perPage} countdata={listCustBack.length} updateCurrentPage={(number)=>updateCurrentPage(number)}/>
      </div>
    </div>
  </div>
  </section>
  <Modal show={show} onHide={handleClose}>
    <Modal.Header closeButton>
      <Modal.Title>Detalles cliente</Modal.Title>
    </Modal.Header>
    <Modal.Body>
      <div className="container">

          <div>
            <p><strong>Orden: </strong> {listDetalle.orden}</p>
            <p><strong>Tipo de orden:</strong> {listDetalle.tipo_orden == 1? 'Garantia' :listDetalle.tipo_orden == 2 ?'Cobrable' :'Extendida'}</p>
            {
              listDetalle.tipo_servicio ?
              <div>
                <p><strong>Tipo de servicio:</strong> {listDetalle.servi.tipo_servicio}</p>
                <p><strong>Caracter de orden:</strong> {listDetalle.carat.caract_orden}</p>
              </div>
              :null
            }
            <p><strong>Documento cliente:</strong> {listDetalle.id_doc}</p>
            <p><strong>Nombre:</strong> {listDetalle.nombre}</p>
            <p><strong>Dirección:</strong> {listDetalle.direccion}</p>
            <p><strong>Barrio:</strong> {listDetalle.barrio}</p>
            <p><strong>Ciudad:</strong> {listDetalle.ciudad}</p>
            <p><strong>Email:</strong> {listDetalle.email}</p>
            <p><strong>celular:</strong> {listDetalle.celular}</p>
            <p><strong>Telefono de casa:</strong> {listDetalle.telcasa}</p>
            <p><strong>Telefono trabajo:</strong> {listDetalle.teltrabajo}</p>
            <p><strong>Factura:</strong> {listDetalle.factura}</p>
            <p><strong>seguimiento:</strong> {listDetalle.seguimiento}</p>
            <p><strong>Aseguradora:</strong> {listDetalle.aseguradora}</p>
            {
              listDetalle.marca ?
              <div>
                <p><strong>Marca:</strong> {listDetalle.brands.marca == null ? "" :listDetalle.brands.marca}</p>
                <p><strong>Articulo:</strong> {listDetalle.items.articulo == null ? "" :listDetalle.items.articulo}</p>
                <p><strong>linea:</strong> {listDetalle.oline.linea == null ? "" :listDetalle.oline.linea}</p>
              </div>
              :null
            }
            <p><strong>Modelo:</strong> {listDetalle.modelo}</p>
            <p><strong>Serie:</strong> {listDetalle.serie}</p>
            <p><strong>Fecha compra:</strong> {listDetalle.fecha_compra}</p>
            <p><strong>Distribuidor:</strong> {listDetalle.distribuidor}</p>
            <p><strong>Accesorios:</strong> {listDetalle.accesorios}</p>
            <p><strong>Fecha entrada:</strong> {listDetalle.fecha_entrada}</p>
            <p><strong>Estado fisico:</strong> {listDetalle.estado_fisico}</p>
            <p><strong>sintoma:</strong> {listDetalle.sintoma	}</p>
            <p><strong>observacione entrada:</strong> {listDetalle.obs_entrada}</p>
            {
              listDetalle.tecrecibe ?
                <div>
                  <p><strong>Tecnico resive:</strong> {listDetalle.user.name}</p>
                  <p><strong>Quien registra:</strong> {listDetalle.user2.name}</p>
                  <p><strong>Tecnico asignado:</strong> {listDetalle.user3.name}</p>
                </div>
              :null
            }

            <p><strong>estado_repa:</strong> {estado.nombre}</p>
          </div>

      </div>
    </Modal.Body>
    <Modal.Footer>
      <Button variant="secondary" onClick={handleClose}>
        Cerrar
      </Button>
    </Modal.Footer>
  </Modal>
</div>
)


}
export default ListOrders;
