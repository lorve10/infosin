import React,{ useState, useEffect, useRef, useCallback} from 'react';
import ReactDOM from 'react-dom';
import '../../../../css/app.css';
import Swal from 'sweetalert2';
import api from '../../Servis/Api'; // trae tambien IP
import ip from '../../Servis/ip'; // trae tambien IP
import util from "../../means/util";
import { Button, Modal } from 'react-bootstrap';
import Pagination from '../../means/paginate';
import Select from 'react-select';
import makeAnimated from 'react-select/animated';
/// tabla filter
import ReactDataGrid from '@inovua/reactdatagrid-community';
import '@inovua/reactdatagrid-enterprise/index.css';
import SelectFilter from '@inovua/reactdatagrid-community/SelectFilter'
import NumberFilter from '@inovua/reactdatagrid-community/NumberFilter'
//// Context
import { useContext } from 'react';
import orderContext from '../../Context/Orders/OrderContext';

//import { ip }  from '../../Servis/Api';
function ListOrders({ dataUpdate, goBack }){
  // varibales
  const {getOrder, getOrderSelect, order,getOrderOut } = useContext(orderContext);
  const [prueba, setPrueba ] = useState(true);
  const prue = getParameterByName('prueba');


  useEffect(()=>{ /// validacion de estado de orden dependiendo de la ruta escojida
    if (prue == '') {
      getOrder()
    }
    if(prue == 3) {
      getOrderSelect()
    }
    if(prue == 4) {
      getOrderOut()
    }
  },[prue])

  function getParameterByName(name) {  /// esta funcion recoje la info que viene de la ruta
      if (name !== "" && name !== null && name != undefined) {
          name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
          var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
              results = regex.exec(location.search);
          return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
      } else {
          var arr = location.href.split("/");
          return arr[arr.length - 1];
      }
  }


  const lista = useRef([])
  lista.current = order;
  const [ listCust, setListCust ] = useState(lista)
  const [ listCustBack, setListCustBack ] = useState(lista)
  const [ estado, setEstado ] = useState([]);
  const [ listDetalle, setListDetalle ] = useState([])
  const [ pdf, setPdf ] = useState([])

  ///control de modal orden
  const [showModal, setShowModal] = useState(false);
  const handleCloseModal = () =>{
     setShowModal(false);
     setControl(false);
   };
  const handleShowModal = () => setShowModal(true);

  ///control de modal entregar orden
  const [ showModalEntregar, setShowModalEntregar ] = useState(false);
  const handleCloseModalEntregar = () =>{
     setShowModalEntregar(false);
     setControl(false);
   };
  const handleShowModalEntregar = () => setShowModalEntregar(true);

  /// constantes para modal entregar ordenDetalle
  const [ fechaEntrega, setFechaEntrega] = useState("");
  const [ tecEntrega, setTecEntrega ] = useState(null);
  const [ tecEntregaSelect, setTecEntregaSelect ] = useState([]);
  const [ animatedComponents, setAnimatedComponents] = useState(makeAnimated);
  const [ obrEntrega, setObrEntrega ] = useState("");
  const [ idEn, setIdEn ] = useState("");
  const [ userRegistra, setUserRegistra ] = useState(null);
  const [ userRegistraSelect, setUserRegistraSelect ] = useState([]);

  const [ partesOrden, setPartesOrden ] = useState([]);


  ///_token
  const [ token, setToken ] = useState(null);
  const ip = ip;

  const [valorPosition, setValorPosition ] = useState("");

  const gridStyle = { minHeight: 600 }; // esta le da el tamaño tabla


  /// generador de token
    useEffect(()=>{
    let csrf = document.querySelector('meta[name="csrf-token"]').content;
    setToken(csrf);
    obtain_user();
    obtain_secre();
    ///validacion para secretaria - no pueda hacer ediciones ni reingresos
    let valor = document.getElementById("position").value;
    setValorPosition(valor);
    console.log(valor);
},[])


  //filtrado de tabla
  const [ gridRef, setGridRef ] = useState(null);
  const [ searchText, setSearchText ] = useState('');
  // este rec
  const searchTextRef = useRef(searchText);
  searchTextRef.current = searchText;
  const [ control, setControl ] = useState(false);


/// useCallback esta funcion trae los datos de las columnas y filtra
const render = useCallback(({ value }) => {
   const lowerSearchText = searchTextRef.current.toLowerCase();
   if (!lowerSearchText) {
     return value;
   }

   const str = value + '' // get string value
   const v = str.toLowerCase() // our search is case insesitive
   const index = v.indexOf(lowerSearchText);

   if (index === -1) {
     return value;
   }

   return [
     <span key="before">{str.slice(0, index)}</span>,
     <span key="match" style={{ background: 'yellow', fontWeight: 'bold'}}>{str.slice(index, index + lowerSearchText.length)}</span>,
     <span key="after">{str.slice(index + lowerSearchText.length)}</span>
   ]
 }, [])

  // aqui se define las columnas de las tablas y como se llama ademas de los valores dependiendo de data [json] - de consulta
  const columns = [
  { name: 'orden' , header: 'Orden',defaultFlex: 1,defaultWidth: 1000, type: 'string',
  render:({ value})=><div style={{display:'flex',justifyContent:'center',alignItems:'center'}}><span>{value}</span></div>},
  { name: 'tipo_orden', header: 'Tipo orden', maxWidth: 1000, defaultFlex: 1, type: 'string' ,
    filterEditor: SelectFilter,
   filterEditorProps: {
     multiple: true,
     wrapMultiple: false,
     dataSource: [ 1 , 2, 3 ].map(c => {
         return { id: c , label: c == 1 ? "Garantía" :c == 2 ? "Cobrable" :"Extendida" }
   }),
 },
   render: ({ value }) => <span> <p>{value == 1 ? 'Garantia' :value == 2 ? 'Cobrable':'Extendida' }</p></span> },

  { name: 'tipo_servicio', header: 'Tipo servicio', maxWidth: 1000, defaultFlex: 1,type: 'string'},

  { name: 'caract_orden', header: 'Carácter de orden', maxWidth: 1000, defaultFlex: 1, type: 'string'  },
  { name: 'id_doc', header: 'Documento', defaultFlex: 1},
  { name: 'nombre', header: 'Nombre', maxWidth: 1000, defaultFlex: 1, type: 'string'},
  { name: 'id', header: 'Acciones', render: ({value }) =>
  <div className="d-flex p-0 m-0" style={{display: 'flex',justifyContent: 'center', alignItems:'center'}}>
        <span className="material-icons cursor-pointer" style={{marginTop:4, marginLeft:3, marginRight:4}} onClick={()=>entregarOrden(value)}>
          <span className="material-icons">
           done
          </span>
        </span>

        <span className="material-icons cursor-pointer" style={{marginTop:4, marginLeft:3}} onClick={()=>ordenDetalle(value)}>
          <span className="material-icons">
           visibility
          </span>
        </span>
        {
            valorPosition == 3 ?
        <></>
            :
        <>
            <span data-tooltip-target="tooltip-default" className="material-icons cursor-pointer" style={{marginTop:4, marginLeft:8}} onClick={()=>reingreso(value)}>
                <span className="material-icons">
                reply
                </span>
          </span>
          <span data-tooltip-target="tooltip-default" className="material-icons cursor-pointer" style={{marginTop:4, marginLeft:8}} onClick={()=>editar(value)}>
              <span className="material-icons">
              edit
              </span>
           </span>
        </>

        }

  </div>
  }


]

/// funcion filter
const onSearchChange = ({ target: { value } }) => {

  const visibleColumns = gridRef.current.visibleColumns;

  setSearchText(value);

  const newDataSource = listCustBack.current.filter(p => {
    return visibleColumns.reduce((acc, col) => {
      const v = (p[col.id] + '').toLowerCase() // get string value
      return acc || v.indexOf(value.toLowerCase()) != -1 // make the search case insensitive
    }, false)
  });

  setListCust({current: newDataSource});
}

const loadData = ({ skip, limit, sortInfo }) => {
  const url =
    DATASET_URL +
    '?skip=' +
    skip +
    '&limit=' +
    limit +
    '&sortInfo=' +
    JSON.stringify(sortInfo);

  return fetch(url).then(response => {
      const totalCount = response.headers.get('X-Total-Count');
      return response.json().then(data => {
        return Promise.resolve({ data, count: parseInt(totalCount) });
      })
    })
}

  //activar detalles
    const ordenDetalle = (data) =>{
      setControl(true)
      handleShowModal();
      const newData = listCust.current.filter(e=>e.id == data)
      setListDetalle(newData[0]);
      setEstado(newData[0].estado);
      setIdEn(newData[0].id)

      api.get('admon/listChallenger').then(response=>{
         let res = response.data;
         const filtro = res.filter(e=>e.ordenkalley == data);
          setPartesOrden(filtro)

        })
    }

    const entregarOrden = (data) => {
      const newData = listCust.current.filter(e=>e.id == data)
      if(!newData[0].fecha_reparacion){
        util.MessageInfo(`Aún no se le ha realizado reparación a la orden ${newData[0].orden}`)
      }else if (newData[0].fecha_entrega) {
        util.MessageInfo(`La orden ${newData[0].orden} ya tiene registro de entrega`)
      }else {
        setControl(true)
        handleShowModalEntregar();
      }
      setListDetalle(newData[0]);
      setIdEn(newData[0].id)
      setEstado(newData[0].estado);
    }

  const pdfData = (data) =>{
    const newData = listCust.current.filter(e=>e.id == data)

    setPdf(newData[0]);
  }

  /// tecnico que entrega el producto
     const obtain_user = async () =>{
       api.get('admon/getUsers')
       .then(response=>{
         let resive = [];
         let filtrado = response.data.filter((e)=>e.position == 2);
         filtrado.map(item=>{
         const data = {
           value:item.id,
           label:item.name
         }
         resive.push(data)
         setTecEntregaSelect(resive)
       })
       }).catch(error => console.error("Error: ", error))
     }

     const obtain_secre  = async () =>{
       api.get('admon/getUsers')
       .then(response=>{

         let resive = [];
         let res = response.data;
         let filtrado = response.data.filter((e)=>e.position == 3);
         filtrado.map(item=>{
         const data = {
           value:item.id,
           label:item.name
         }
         resive.push(data)
         setUserRegistraSelect(resive)

       })

       }).catch(error => console.error("Error: ", error))
     }

  //// funcion de reingreso
  const reingreso = (data) =>{
    const newData = listCust.current.filter(e=>e.id == data)
    dataUpdate(newData, false);
  }
  const editar = (data) =>{
    console.log("entonces sssss");
    const newData = listCust.current.filter(e=>e.id == data)
    dataUpdate(newData, true);
  }


  // declaraciones de variable para saber type y filtrar con este
  const defaultFilterValue = [
  { name: 'orden', type: 'string', operator: 'contains', value: '' },
  { name: 'nombre', type: 'string', operator: 'contains', value: '' },
  { name: 'id_doc',  operator: 'contains', type: 'string', value: '' },
  { name: 'tipo_orden', operator: 'inlist', type: 'select', value: '' },
  { name: 'tipo_servicio',  type: 'string', operator: 'contains', value: '' },
  { name: 'caract_orden', type: 'string', operator: 'contains', value: '' },

];


///funcion para guardar datos
  const saveEntrega = async () => {

    var message = ''
    var error = false

    if (fechaEntrega == "") {
      error = true
      message = "Por favor ingrese una fecha de entrega"
    }
    else if (obrEntrega == "") {
      error = true
      message = "Por favor diligencie el campo observación de entrega"
    }

    if (error) {
      util.MessageError(message)
    }
    else {
      const data = new FormData()
      data.append('id', idEn)
      data.append('fecha_entrega', fechaEntrega)
      data.append('obs_entrega', obrEntrega)
      data.append('tecentrega', tecEntrega.value)
      data.append('regentrega', userRegistra.value)
      api.post('admon/saveEntrega',data).then(response=>{
        if(response.success == true){
          util.MessageSuccess("Se guardó exitosamente")
          handleCloseModalEntregar()
        } else {
          util.MessageError("Error al guardar")
          handleCloseModalEntregar()
        }

      }).catch(err => console.warn(err))
    }
  }

const dataSource = useCallback(loadData, []);

return(
  <div className="mt-4">


    <div className="flex justify-content-center">
      <div className="col-6">
        <input className="text-center shadow appearance-none border rounded-full w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
        type="text" placeholder="Buscar orden"
        value={searchText} onChange={onSearchChange}
        />
      </div>
    </div>

    <section className="container mx-auto p-6 font-mono">
      <div className="w-full  overflow-hidden rounded-lg shadow-lg">
        <div className="w-full overflow-x-auto">
      <div className="mt-2">
        <div>
              <ReactDataGrid
                onReady={setGridRef}
                idProperty="id"
                defaultFilterValue={defaultFilterValue}
                 columns={columns}
                 dataSource={listCust.current}
                 style={gridStyle}
                 pagination
                defaultLimit={6}
              />
        </div>
      </div>

    </div>
    </div>
    </section>


    <Modal show={showModal} size="lg" onHide={handleCloseModal} closeButton>
      <div className="container px-3 pt-4 pb-0 " >
        <div className="grid grid-cols-3 grid-rows-2 gap-1  " >
          <div className="row-span-2 flex justify-end">
              <img className="w-2/4 h-5/6 " src={ip+listDetalle.logo}/>
          </div>
          <div className="col-span-2 text-4xl pt-2 text-center"><h3 style={{ color:'#001250 '}}><strong>Orden No. {listDetalle.orden} </strong></h3></div>
          <div className="col-span-2 text-2xl pt-0 text-center"><h3><strong>{listDetalle.tipo_orden == 1? 'Garantia' :listDetalle.tipo_orden == 2 ?'Cobrable' :'Extendida'} - {listDetalle.marca}</strong> </h3></div>
        </div>
      </div>

      <Modal.Body className="pt-0">
        <div className="container px-1 py-0">

          <div className=" w-full overflow-hidden rounded-xl shadow-sm border-b border-slate-700 mb-1">
            <table className="w-full">
              <thead className="">
                <tr className=" text-center text-base text-white font-semibold" style={{ background:'#07376D'}}>
                  <th className="px-1 py-1 w-32 border border-gray-600 " colSpan="3">Datos del cliente</th>
                </tr>
              </thead>
              <tbody className="bg-white text-sm" >
                      <tr className="" style={{ background:'#FBFBF9'}} >
                        <td className="border px-2 py-1">
                          <label className="pr-2 font-bold">Nombre</label>{listDetalle.nombre}
                        </td>
                        <td className="border  px-2 py-1">
                          <label className="pr-2 font-bold" >No. de cédula / NIT </label>{listDetalle.id_doc}
                        </td>
                        <td className="border  px-2 py-1">
                          <label className="pr-2 font-bold">Celular</label>{listDetalle.celular}
                        </td>
                      </tr>

                      <tr className="" style={{ background:'#ededed'}} >
                        <td className="border px-2 py-1">
                          <label className="pr-2 font-bold">Tel. Casa</label>{listDetalle.telcasa}
                        </td>
                        <td className="border  px-2 py-1">
                          <label className="pr-2 font-bold" >Tel. Oficina </label>{listDetalle.teltrabajo}
                        </td>
                        <td className="border  px-2 py-1">
                          <label className="pr-2 font-bold">Email</label>{listDetalle.email}
                        </td>
                      </tr>

                      <tr className="" style={{ background:'#FBFBF9'}} >
                        <td className="border px-2 py-1">
                          <label className="pr-2 font-bold">Ciudad</label>{listDetalle.ciudad}
                        </td>
                        <td className="border  px-2 py-1">
                          <label className="pr-2 font-bold" >Barrio</label>{listDetalle.barrio}
                        </td>
                        <td className="border  px-2 py-1">
                          <label className="pr-2 font-bold">Dirección</label>{listDetalle.direccion}
                        </td>
                      </tr>
              </tbody>
            </table>
          </div>

          <div className=" w-full overflow-hidden rounded-xl shadow-sm border-b border-slate-700 mb-1 ">
            <table className="w-full">
              <thead className="">
                <tr className=" text-center text-base text-white font-semibold" style={{ background:'#07376D'}}>
                  <th className="px-1 py-1 w-32 border border-gray-600 " colspan="3">Datos del artículo</th>
                </tr>
              </thead>
              <tbody className="bg-white text-sm" >
                      <tr className="" style={{ background:'#FBFBF9'}} >
                        <td className="border px-2 py-1">
                          <label className="pr-2 font-bold">Articulo</label>{listDetalle.articulo}
                        </td>
                        <td className="border  px-2 py-1">
                          <label className="pr-2 font-bold" >Serie</label>{listDetalle.serie}
                        </td>
                        <td className="border  px-2 py-1">
                          <label className="pr-2 font-bold">Modelo</label>{listDetalle.modelo}
                        </td>
                      </tr>

                      <tr className="" style={{ background:'#ededed'}} >
                        <td className="border px-2 py-1">
                          <label className="pr-2 font-bold">Distribuidor</label>{listDetalle.distribuidor}
                        </td>
                        <td className="border  px-2 py-1">
                          <label className="pr-2 font-bold" >Factura</label>{listDetalle.factura}
                        </td>
                        <td className="border  px-2 py-1">
                          <label className="pr-2 font-bold">Fecha entrada</label>{listDetalle.fecha_entrada}
                        </td>
                      </tr>

                      <tr className="" style={{ background:'#FBFBF9'}} >
                        <td className="border px-2 py-1">
                          <label className="pr-2 font-bold">Fecha compra</label>{listDetalle.fecha_compra}
                        </td>
                        <td className="border  px-2 py-1">
                          <label className="pr-2 font-bold" >Recibió</label>{listDetalle.tecrecibe}
                        </td>
                        <td className="border  px-2 py-1">
                          <label className="pr-2 font-bold">Registró</label>{listDetalle.regrecibe}
                        </td>
                      </tr>

                      <tr className="" style={{ background:'#ededed'}} >
                        <td className="border px-2 py-1">
                          <label className="pr-2 font-bold">Tec. asignado</label>{listDetalle.tecasignado}
                        </td>
                        <td className="border  px-2 py-1">
                          <label className="pr-2 font-bold" >Accesorios</label>{listDetalle.accesorios}
                        </td>
                        <td className="border  px-2 py-1">
                          <label className="pr-2 font-bold">Estado físico</label>{listDetalle.estado_fisico}
                        </td>
                      </tr>

                      <tr className="" style={{ background:'#FBFBF9'}} >
                        <td className="border px-2 py-1" >
                          <label className="pr-2 font-bold">Síntoma</label>{listDetalle.sintoma}
                        </td>
                        <td className="border  px-2 py-1" colspan="2">
                          <label className="pr-2 font-bold">Observación de entrada</label>{listDetalle.obs_entrada}
                        </td>
                      </tr>
              </tbody>
            </table>
          </div>

          <div className=" w-full overflow-hidden rounded-xl shadow-sm border-b border-slate-700 mb-1">
            <table className="w-full">
              <thead className="">
                <tr className=" text-center text-base text-white font-semibold" style={{ background:'#07376D'}}>
                  <th className="px-1 py-1 w-32 border border-gray-600 " colspan="3">Datos de la reparación</th>
                </tr>
              </thead>
              <tbody className="bg-white text-sm" >
                      <tr className="" style={{ background:'#FBFBF9'}} >
                        <td className="border px-2 py-1">
                          <label className="pr-2 font-bold">Fecha revisión</label>{listDetalle.fecha_revision}
                        </td>
                        <td className="border  px-2 py-1">
                          <label className="pr-2 font-bold" >Fecha reparación</label>{listDetalle.fecha_reparacion}
                        </td>
                        <td className="border  px-2 py-1">
                          <label className="pr-2 font-bold">Estado</label>{listDetalle.estado_repa}
                        </td>
                      </tr>

                      <tr className="" style={{ background:'#ededed'}} >
                        <td className="border px-2 py-1">
                          <label className="pr-2 font-bold">Técnico que repara</label>{listDetalle.tecasignado}
                        </td>
                        <td className="border px-2 py-1" colspan="2">
                          <label className="pr-2 font-bold">Descripción de la falla</label>{listDetalle.desc_falla}
                        </td>
                      </tr>

                      <tr className="" style={{ background:'#FBFBF9'}} >
                        <td className="border  px-2 py-1" colspan="3">
                          <label className="pr-2 font-bold" >Observación de la reparación</label>{listDetalle.fecha_reparacion}
                        </td>
                      </tr>

                      <tr className="" style={{ background:'#ededed'}} >
                        <td className="border  px-2 py-1" colspan="3">
                          <label className="pr-2 font-bold">Descripción de la reparación</label>{listDetalle.obs_reparacion}
                        </td>
                      </tr>
              </tbody>
            </table>
          </div>

          <div className=" w-full overflow-hidden rounded-xl shadow-sm border-b border-slate-700 mb-1">
            <table className="w-full">
              <thead className="">
                <tr className=" text-center text-base text-white font-semibold" style={{ background:'#07376D'}}>
                  <th className="px-1 py-1 w-32 border border-gray-600 " colspan="4">Datos de repuestos</th>
                </tr>
              </thead>
                  {
                    partesOrden.length > 0 ?
                        partesOrden.map((item,index)=>(
                          <tbody className="bg-white text-sm" >
                              <tr className="" style={{ background:'#FBFBF9'}} key={index}>
                                <td className="border px-2 py-1" colspan="2">
                                  <label className="pr-2 font-bold">Repuesto {index+1}</label>{item.descripcion}
                                </td>
                                <td className="border  px-2 py-1" colspan="2">
                                  <label className="pr-2 font-bold">Fecha llegada</label>{item.fechallegada ? item.fechallegada : 'En proceso'}
                                </td>
                              </tr>

                              <tr className="" style={{ background:'#ededed'}} >
                                <td className="border px-2 py-1" colspan="3">
                                  <label className="pr-2 font-bold">Observaciones</label>{item.observaciones}
                                </td>
                                <td className="border  px-2 py-1">
                                    <label className="pr-2 font-bold">Recibió</label>{item.tecnicorecibe ? item.tecnicorecibe.name : null }
                                </td>
                              </tr>

                          </tbody>
                          ))
                      :
                      <tbody className="bg-white text-sm" >
                          <tr className="" style={{ background:'#FBFBF9'}} >
                            <td className="border px-2 py-1">
                              <label className="pr-2 font-bold">Repuesto</label>N/A
                            </td>
                            <td className="border  px-2 py-1">
                              <label className="pr-2 font-bold">Fecha llegada</label>N/A
                            </td>
                            <td className="border  px-2 py-1">
                              <label className="pr-2 font-bold">Quien recibió</label>N/A
                            </td>
                          </tr>
                          <tr className="" style={{ background:'#ededed'}} >
                            <td className="border px-2 py-1" colspan="3">
                              <label className="pr-2 font-bold">Observaciones</label>N/A
                            </td>
                          </tr>
                      </tbody>
                    }

              </table>
          </div>

          <div className=" w-full overflow-hidden rounded-xl shadow-sm border-b border-slate-700 mb-1">
            <table className="w-full">
              <thead className="">
                <tr className=" text-center text-base text-white font-semibold" style={{ background:'#07376D'}}>
                  <th className="px-1 py-1 w-32 border border-gray-600 " colspan="3">Datos de la entrega</th>
                </tr>
              </thead>
              <tbody className="bg-white text-sm" >
                      <tr className="" style={{ background:'#FBFBF9'}} >
                        <td className="border px-2 py-1" colspan="2">
                          <label className="pr-2 font-bold">Fecha entrega</label>{listDetalle.fecha_entrega ? listDetalle.fecha_entrada : 'Aún no se entrega'}
                        </td>
                        <td className="border  px-2 py-1"colspan="2">
                          <label className="pr-2 font-bold" >Entregó</label>{listDetalle.regentregaorden}
                        </td>
                      </tr>
                      <tr className="" style={{ background:'#ededed'}} >
                        <td className="border  px-2 py-1"colspan="4">
                          <label className="pr-2 font-bold">Observación de salida</label>{listDetalle.obs_entrega}
                        </td>
                      </tr>
                      <tr className="" style={{ background:'#FBFBF9'}} >
                        <td className="border px-2 py-1" colspan="2">
                        <div className="flex">
                          <label className="pr-2 pt-2 font-bold">Registro de entrada</label>
                          <form  action={ip+"admon/print/order"} method="POST" target="_blank" style={{ marginTop:0}}>
                                <input type= "hidden"  name= "data" value={JSON.stringify(pdf)}  />
                                <input type="hidden" name="_token" value={token} />
                                <button type="submit" onClick={()=>pdfData(listDetalle.id)} className="btn " style={{margin:1, borderRadius:10, padding:'1px', color:'black'}}>  <span className="material-icons">
                                    picture_as_pdf
                                  </span></button>
                            </form>
                          </div>
                        </td>
                        <td className="border px-2 py-1" colspan="2">
                        <div className="flex">
                          <label className="pr-2 pt-2 font-bold">Registro de salida</label>
                          {
                            listDetalle.fecha_entrega ?
                            <form  action={ip+"admon/print/ordersalida"} method="POST" target="_blank" style={{ marginTop:0}}>
                                  <input type= "hidden"  name= "data" value={JSON.stringify(pdf)}  />
                                  <input type="hidden" name="_token" value={token} />
                                  <button type="submit" onClick={()=>pdfData(listDetalle.id)} className="btn " style={{margin:1, borderRadius:10, padding:'1px', color:'black'}}>  <span className="material-icons">
                                      picture_as_pdf
                                    </span></button>
                              </form>
                              :
                              null
                          }
                          </div>
                        </td>
                      </tr>
              </tbody>
            </table>
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleCloseModal}>
          Cerrar
        </Button>
      </Modal.Footer>
    </Modal>


    <Modal show={showModalEntregar} size="lg" onHide={handleCloseModalEntregar} >
      <Modal.Header closeButton>
        <div className="container p-2 mt-3" >
            <div className="text-center text-xl md: text-4xl">
                <h3><strong>Entregar orden No. {listDetalle.orden}</strong></h3>
            </div>
        </div>
      </Modal.Header >

      <Modal.Body>
        <div className="container">
            <div className="grid grid-cols-1 md:grid-cols-2 mt-2 gap-3 px-3">

                <div className="">
                   <label className=" block text-gray-700 text-sm font-bold mb-2" htmlFor="password">
                        Fecha de entrega
                    </label>
                     <input className="shadow shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                       value = {fechaEntrega}
                       onChange = {(e)=>setFechaEntrega(e.target.value)}
                       type="date" placeholder="Fecha de entrega"/>
                </div>

                <div className="">
                   <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="username">
                       Técnico que entrega
                   </label>
                   <Select
                    className="shadow block text-gray-700 text-sm font-bold mb-2"
                    value={tecEntrega}
                    closeMenuOnSelect={true}
                    components={animatedComponents}
                    options={tecEntregaSelect}
                    onChange={(e)=>setTecEntrega(e)}
                    placeholder = "Seleccionar técnico que entrega"
                    />
                </div>
            </div>
            <div className="grid grid-cols-1 md:grid-cols-2 mt-2 gap-3 px-3">

                <div className="">
                   <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="username">
                       Usuario que registra
                   </label>
                   <Select
                    className="shadow block text-gray-700 text-sm font-bold mb-2"
                    value={userRegistra}
                    closeMenuOnSelect={true}
                    components={animatedComponents}
                    options={userRegistraSelect}
                    onChange={(e)=>setUserRegistra(e)}
                    placeholder = "Seleccionar usuario que registra"
                    />
                </div>
                <div className="">
                   <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="username">
                       Observación de entrega
                   </label>
                   <textarea className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                     type="text" placeholder="Observación de entrega"
                     value = {obrEntrega}
                     onChange = {(e)=>setObrEntrega(e.target.value)}
                     ></textarea>
                </div>
            </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleCloseModalEntregar}>Cerrar</Button>
          <Button variant="primary" onClick={saveEntrega}>Guardar</Button>
      </Modal.Footer>
    </Modal>

</div>
)

}
export default ListOrders;
