import React,{ useState, useEffect} from 'react';
import ReactDOM from 'react-dom';
import '../../../../css/app.css';
import Swal from 'sweetalert2';
import api from '../../Servis/Api';
import Select from 'react-select';
import makeAnimated from 'react-select/animated';
import util  from '../../means/util';

function NewOrders({ list, goBack, id, entrada}) {

  ///variables del formulario

   const [ devices, setDevices ] = useState(null)
   const [ deviceSelect, setDeviceSelect ] = useState([])

   const [ tipoOrden, setTipoOrden ] = useState(0)
   const [ tipoOrdenSelect, setTipoOrdenSelect ] = useState([
     {
     value:'1',
     label:'Garantía',
     },
     {
     value:'2',
     label:'Cobrable',
     },
     {
     value:'3',
     label:'Extendida',
     }
   ])
   const [ tipoServi, setTipoServi ] = useState(0)
   const [ tipoServiSelect, setTipoServiSelect ] = useState([]);
   const [ tipoCat, setTipoCat ] = useState(0)
   const [ tipoCatSelect, setCatSelect ] = useState([])
   const [ cliente, setCliente ] = useState(null)
   const [ clienteSelect, setClienteSelect ] = useState([])
   const [ factura, setFactura ] = useState("")
   const [ fechaCompra, setFechaCompra ] = useState("")
   const [ distribuidor, setDistribuidor ] = useState("")
   const [ seguimiento, setSeguimiento ] = useState("")
   const [ aseguradora, setAseguradora ] = useState("")
   const [ accesorios, setAccesorios ] = useState("")
   const [ fechaEntrada, setFechaEntrada ] = useState("")
   const [ estadoFisico, setEstadoFisico ] = useState("")
   const [ sintoma, setSintoma ] = useState("")
   const [ obrEntrada, setObrEntrada ] = useState("")

   const [ resive, setResive ] = useState(null) ///este resive el usuario que registra la orden
   const [ resiveSelect, setResiveSelect ] = useState([])

   const [ userRegistra, setUserRegistra ] = useState(null)
   const [ userRegistraSelect, setUserRegistraSelect ] = useState([])

   const [ tecAsign, setTecAsign ] = useState(null)
   const [ tecAsignSelect, setTecAsignSelect ] = useState([])


   const [ tecnico, setTecnico ] = useState([])
   const [ tecnicoSelect, setTecnicoSelect ] = useState([
     {
     value:'1',
     label:'prueba',
     },
     {
     value:'2',
     label:'respuesta',
     },
     {
     value:'3',
     label:'otra',
     }
   ])
   const [ estadoOrden, setEstadoOrden ] = useState(0)
   const [ estadoSelect, setEstadoSelect ] = useState([])
   const [ animatedComponents, setAnimatedComponents] = useState(makeAnimated);

   const [ aseg, setAseg ] = useState(null)
   const [ asegSelet, setAsegSelect ] = useState([])
   useEffect(()=>{

     obtain_client()
     obtain_tipoSer()
     obtain_cate()
     obtain_status()
     ////
     obtain_user()
     obtain_user2()
     obtain_secre()
     obtain_asg()
     //
     tipoOrdenFun()
   },[])

   useEffect(()=>{
     if(id >0){
            setAccesorios(list.accesorios)
            setEstadoFisico(list.estado_fisico)
            setSintoma(list.sintoma)
            setObrEntrada(list.obs_entrada)
            setFechaEntrada(list.fecha_entrada)
     }
   },[id])

   /// Traer el tipo de orden para usarlo al momento de realizar un reingreso
   const tipoOrdenFun = () => {
     if (list.tipo_orden === 1) {
       const data = {
         value:'1', label:'Garantía'
       }
       setTipoOrden(data)
     } else if (list.tipo_orden === 2) {
       const data = {
         value:'2', label:'Cobrable'
       }
       setTipoOrden(data)
     }else if (list.tipo_orden === 3) {
       const data = {
         value:'3', label:'Extendida'
       }
       setTipoOrden(data)
     }
   }

   ///aseguradoras
   const obtain_asg = async () =>{
     api.get('admon/get')
     .then(response=>{
         let marca = [];
         var res = response.data;
         let filtrado = res.filter(e=>e.tipo == 2);
         filtrado.map(item=>{
           const data = {
             value:item.id,
             label:item.marc
           }
           marca.push(data)
           setAsegSelect(marca)
         })
         if (id > 0) {  var filtroMarca = filtrado.filter(e=>e.marc == list.aseguradora)

           const data = { value: filtroMarca[0].id, label: filtroMarca[0].marc }
           setAseg(data)
         }
     }).catch(error => console.error("Error: ", error))
   }

/// tecnico que resive el producto
   const obtain_user = async () =>{
     api.get('admon/getUsers')
     .then(response=>{
       let resive = [];
       let filtrado = response.data.filter((e)=>e.position == 2);
       filtrado.map(item=>{
       const data = {
         value:item.id,
         label:item.name
       }
       resive.push(data)
       setResiveSelect(resive)
       if (id > 0) {  var filtro = filtrado.filter(e=>e.id == list.userone)

         const data = { value: filtro[0].id, label: filtro[0].name }
         setResive(data)
       }
     })
     }).catch(error => console.error("Error: ", error))
   }

   const obtain_user2 = async () =>{
     api.get('admon/getUsers')
     .then(response=>{
       let resive = [];
       let filtrado = response.data.filter((e)=>e.position == 2);
       filtrado.map(item=>{
       const data = {
         value:item.id,
         label:item.name
       }
       resive.push(data)

       setTecAsignSelect(resive)
       if (id > 0) {  var filtro = filtrado.filter(e=>e.id == list.userthree)

         const data = { value: filtro[0].id, label: filtro[0].name }
         setTecAsign(data)
       }
     })
     }).catch(error => console.error("Error: ", error))
   }


   const obtain_secre  = async () =>{
     api.get('admon/getUsers')
     .then(response=>{

       let resive = [];
       let res = response.data;
       let filtrado = response.data.filter((e)=>e.position == 3);
       filtrado.map(item=>{
       const data = {
         value:item.id,
         label:item.name
       }
       resive.push(data)
       setUserRegistraSelect(resive)

       if (id > 0) {  var filtro = filtrado.filter(e=>e.id == list.usertwo)

         const data = { value: filtro[0].id, label: filtro[0].name }
         setUserRegistra(data)
       }

     })



     }).catch(error => console.error("Error: ", error))
   }

   const obtain_client = async () =>{
     api.get('admon/getCustomers')
     .then(response=>{
       var res = response.data;
       var client = [];
       res.map(item=>{
       const data = {
         value:item.id,
         label:item.cust_nombre
       }
       client.push(data)
       setClienteSelect(client)

       if (id>0){
         var filtro = res.filter(e=>e.id == list.customer_id)
         const data = { value: filtro[0].id, label: filtro[0].cust_nombre }
         setCliente(data)
         obtain_marca(data.value, true)
       }

     })
     }).catch(error => console.error("Error: ", error))
   }

   const filtrado = (value) => {
       setCliente(value);
       obtain_marca(value.value, true)
   }

   const obtain_marca = async (value, boolean) =>{
     setDeviceSelect([])
     api.get('admon/getCustDiv')
       .then(response=>{
         let marca = [];
         let filtrado = response.data.filter(item =>item.persona.id == value);

         let filt = filtrado.filter(e=>e.dispositivo.brands.tipo == 1);
         filt.map(item=>{
         const data = {
           value:item.dispositivo.id,
           label:item.dispositivo.brands.marc == null ? "" : item.dispositivo.brands.marc  +" - "+ item.dispositivo.serie
         }
         marca.push(data)
         setDeviceSelect(marca)
       })

       if (id>0){

         var filtro = filt.filter(e=>e.dispositivo.brands.marc == list.marca)
         const data = { value: filtro[0].dispositivo.id, label: filtro[0].dispositivo.brands.marc }
         setDevices(data)
       }


     }).catch(error =>console.log(error))
     if(boolean){
       setDevices("")
     }
   }

   const obtain_tipoSer = async () =>{
     api.get('admon/getServe')
     .then(response=>{

       let serv = [];
       let res = response.data;

       response.data.map(item=>{
       const data = {
         value:item.id,
         label:item.tipo_servi
       }
       serv.push(data)
       setTipoServiSelect(serv)
     })
     if (id > 0 ){
            let filtro = res.filter(f=>f.tipo_servi  == list.tipo_servicio)
            const data = {
              value: filtro[0].id,
              label: filtro[0].tipo_servi
            }
            setTipoServi(data)
        }

     }).catch(error => console.error("Error: ", error))
   }
   const obtain_cate = async () =>{
     api.get('admon/getCart')
     .then(response=>{
       let cat = [];
      let res = response.data;
       response.data.map(item=>{
       const data = {
         value:item.id,
         label:item.caract_orde
       }
       cat.push(data)
       setCatSelect(cat)
     })
     if (id > 0 ){
            let filtro = res.filter(f=>f.caract_orde  == "Reingreso")
            const data = {
              value: filtro[0].id,
              label: filtro[0].caract_orde
            }
            setTipoCat(data)
          }
     }).catch(error => console.error("Error: ", error))
   }

   const obtain_status = async () =>{
     api.get('admon/getStatus')
     .then(response=>{
       let cat = [];
       let res =  response.data;
      res.map(item=>{
       const data = {
         value:item.id,
         label:item.nombreStado
       }
       cat.push(data)
       setEstadoSelect(cat)
     })
     if (id > 0 ){

            let filtro =  res.filter(f=>f.nombreStado == list.estado_repa)

            filtro.map(item=>{
             const data = {
               value:item.id,
               label:item.nombreStado

             }
             setEstadoOrden(data)

           })


          }

     }).catch(error => console.error("Error: ", error))
   }

  const validar = () => {

    if(id > 0) {
      setAseguradora(list.aseguradora)
    }
  }

///funcion para guardar datos
  const save = async () => {

    var message = ''
    var error = false


    if (tipoOrden == 0) {
      error = true
      message = "Por favor seleccione un tipo de orden"
    }
    else if (tipoServi == 0) {
      error = true
      message = "Por favor seleccione un tipo de servicio"
    }
    else if (tipoCat == 0) {
      error = true
      message = "Por favor seleccione una característica de orden"
    }
    else if (cliente == null) {
      error = true
      message = "Por favor seleccione un cliente"
    }
    else if (devices.length == 0) {
      error = true
      message = "Por favor seleccione un dispositivo"
    }
    else if (tipoOrden.value == 3 && seguimiento == "") {
        error = true
        message = "Por favor diligencie el campo seguimiento"
    }

    else if (tipoOrden.value == 1 && factura == "") {
        error = true
        message = "Por favor ingrese una factura"
    }
    else if (tipoOrden.value == 1 && fechaCompra == ""){
      error = true
      message = "Por favor ingrese una fecha de compra"
    }
    else if (tipoOrden.value == 1 && distribuidor=="" ){
      error = true
      message = "Por favor ingrese un distribuidor"
    }

    else if (accesorios == "") {
      error = true
      message = "Por favor diligencie campo de accesorios"
    }
    else if (fechaEntrada == "") {
      error = true
      message = "Por favor ingrese una fecha de entrada"
    }
    else if (estadoFisico == "") {
      error = true
      message = "Por favor diligencie el campo de estado físico"
    }
    else if (sintoma == "") {
      error = true
      message = "Por favor diligencie el campo de sintoma"
    }
    else if (obrEntrada == "") {
      error = true
      message = "Por favor diligencie el campo observación de entrada"
    }
    else if (resive == null) {
      error = true
      message = "Por favor seleccione el tecnico que recibe"
    }
    else if (userRegistra == null) {
      error = true
      message = "Por favor seleccione el usuario que registra"
    }
    else if (tecAsign == null) {
      error = true
      message = "Por favor seleccione un técnico para reparación"
    }
    else if (estadoOrden == 0) {
      error = true
      message = "Por favor seleccione un estado de orden"
    }

    if (error) {
      util.MessageError(message)
    }
    else {
      const data = new FormData()
      data.append('id', id)
      data.append('orden', 0)
      data.append('tipo_orden', tipoOrden.value)
      data.append('tipo_servicio', tipoServi.value)
      data.append('caract_orden', tipoCat.value)
      data.append('customer_id', cliente.value)
      data.append('device_id', devices.value)
      data.append('factura', factura == null ? 0 : factura )
      data.append('fecha_compra', fechaCompra == null ? '' : fechaCompra)
      data.append('distribuidor', distribuidor == null ? '' : distribuidor)
      data.append('seguimiento', seguimiento)
      data.append('aseguradora', aseg == null ? aseg==null : aseg.value )
      data.append('accesorios', accesorios)
      data.append('fecha_entrada', fechaEntrada)
      data.append('estado_fisico', estadoFisico)
      data.append('sintoma', sintoma)
      data.append('obs_entrada', obrEntrada)
      data.append('tecrecibe', resive.value)
      data.append('regrecibe', userRegistra.value)
      data.append('tecasignado', tecAsign.value)
      data.append('estado_repa', estadoOrden.value)
      data.append('entrada', entrada== false ? 2 : 3 )
      api.post('admon/save',data).then(response=>{
        if(response.success){
            if(response.data == 2){
              util.MessageSuccess("Orden registrada exitosamente")
              goBack();
            }
            if(response.data == 3 ){
                util.MessageSuccess("Registro editado exitosamente")
                goBack();
            }
            if(response.id > 0 && response.data == 2){
                util.MessageSuccess(" Reingreso creado correctamente")
                goBack();
            }
        }
        else {
          alert("Error")
        }
      }).catch(err => console.warn(err))
    }
  }

  return(
    <div>
      <div>
          {
            id > 0 ?
              entrada == true ?
              <h2 className="text-gray-800 text-3xl font-semibold ml-3">Editar Orden</h2>
              :
            <h2 className="text-gray-800 text-3xl font-semibold ml-3">Registrar Reingreso</h2>
            :
            <h2 className="text-gray-800 text-3xl font-semibold ml-3">Registrar Orden</h2>
          }
      </div>
      <div className="overflow-auto h-5/6">
      <div className="flex mt-4">

           <div className="col-4">
              <label className="block text-gray-700 text-sm font-bold mb-2">
                  Tipo de orden
               </label>
               <Select
                className=" shadow block text-gray-700 text-sm font-bold mb-2"
                value={tipoOrden}
                closeMenuOnSelect={true}
                components={animatedComponents}
                options={tipoOrdenSelect}
                onChange={(e)=>setTipoOrden(e)}
                placeholder = "Seleccionar tipo de Orden"
                />
           </div>
           <div className="col-4">
              <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="username">
                 Tipo de servicio
              </label>
              <Select
               className="shadow block text-gray-700 text-sm font-bold mb-2"
               value={tipoServi}
               closeMenuOnSelect={true}
               components={animatedComponents}
               options={tipoServiSelect}
               onChange={(e)=>setTipoServi(e)}
               placeholder = "Tipo de servicio"
               />
           </div>
           <div className="col-4">
              <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="password">
                   Característica de orden
               </label>
               <Select
                className="shadow block text-gray-700 text-sm font-bold mb-2"
                value={tipoCat}
                closeMenuOnSelect={true}
                components={animatedComponents}
                options={tipoCatSelect}
                onChange={(e)=>setTipoCat(e)}
                placeholder = "Seleccionar característica de orden"
                />
            </div>
      </div>

      <div className="flex mt-2">
            <div className="col-4">
               <label className="block text-gray-700 text-sm font-bold mb-2">
                    Seleccionar cliente
                </label>
                <Select
                 className="shadow block text-gray-700 text-sm font-bold mb-2"
                 value={cliente}
                 closeMenuOnSelect={true}
                 components={animatedComponents}
                 options={clienteSelect}
                 onChange={(e)=>filtrado(e)}
                 placeholder = "Seleccionar cliente"
                 />
             </div>
             <div className="col-4">
                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="password">
                     Seleccionar dispositivo
                 </label>
                 <Select
                  className="shadow block text-gray-700 text-sm font-bold mb-2"
                  value={devices}
                  closeMenuOnSelect={true}
                  components={animatedComponents}
                  options={deviceSelect}
                  onChange={(e)=>setDevices(e)}
                  placeholder = "Seleccionar dispositivo"
                  />
              </div>
              {
                tipoOrden.value == 3 ?
                <div className="col-4">
                   <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="username">
                      Aseguradora
                   </label>
                   <Select
                    className="shadow block text-gray-700 text-sm font-bold mb-2"
                    value={aseg}
                    closeMenuOnSelect={true}
                    components={animatedComponents}
                    options={asegSelet}
                    onChange={(e)=>setAseg(e)}
                    placeholder = "Aseguradora"
                    />
                </div>
                :null
              }

        </div>
    {
      tipoOrden.value == 1 ?
      <div className="grid grid-cols-1 sm:grid-cols-3 mt-2 gap-4 px-3">

          <div className="">
             <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="username">
                Factura
             </label>
             <input className="shadow shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
               type="number" placeholder="Factura"
               value = {factura}
               onChange = {(e)=>setFactura(e.target.value)}
               />
          </div>
          <div className="">
             <label className=" block text-gray-700 text-sm font-bold mb-2" htmlFor="password">
                  Fecha de compra
              </label>
               <input className="shadow shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                 value = {fechaCompra}
                 onChange = {(e)=>setFechaCompra(e.target.value)}
                 type="date" placeholder="Fecha de compra"/>
          </div>
          <div className="">
             <label className=" block text-gray-700 text-sm font-bold mb-2" htmlFor="username">
               Distribuidor
             </label>
             <input className="shadow shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
               type="text" placeholder="Distribuidor"
               value = {distribuidor}
               onChange = {(e)=>setDistribuidor(e.target.value)}
               />
          </div>
      </div>
      :null
      }

      <div className= {
        tipoOrden.value == 3 ?  "grid grid-cols-1 sm:grid-cols-2 mt-2 gap-4 px-3" : "grid grid-cols-1 sm:grid-cols-1 mt-2 gap-4 px-3"
      }>
        <div className="">
           <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="username">
              Síntoma
           </label>
           <textarea className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
             type="text" placeholder="Síntoma"
             value = {sintoma}
             onChange = {(e)=>setSintoma(e.target.value)}
             ></textarea>
        </div>
        {
          tipoOrden.value == 3 ?
          <div className="">
             <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="password">
                 Seguimiento
              </label>
               <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                 type="text" placeholder="Seguimiento"
                 value = {seguimiento}
                 onChange = {(e)=>setSeguimiento(e.target.value)}
                 />
          </div>
          :null
        }
      </div>

      <div className="grid grid-cols-1 sm:grid-cols-3 mt-2 gap-4 px-3">
        <div className="">
           <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="username">
              Fecha de entrada
           </label>
           <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
             type="date" placeholder="Fecha de entrada"
             value = {fechaEntrada}
             onChange = {(e)=>setFechaEntrada(e.target.value)}
             />
        </div>
        <div className="">
           <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="username">
               Estado físico
           </label>
           <textarea className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
             type="text" placeholder="Estado físico"
             value = {estadoFisico}
             onChange = {(e)=>setEstadoFisico(e.target.value)}
             ></textarea>
        </div>

        <div className="">
           <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="username">
               Accesorios
           </label>
           <textarea className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
             type="text" placeholder="Accesorios"
             value = {accesorios}
             onChange = {(e)=>setAccesorios(e.target.value)}
             ></textarea>
        </div>
      </div>

      <div className="grid grid-cols-1 sm:grid-cols-3 mt-2 gap-4 px-3">
        <div className="">
           <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="username">
               Observación de entrada
           </label>
           <textarea className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
             type="text" placeholder="Observación de entrada"
             value = {obrEntrada}
             onChange = {(e)=>setObrEntrada(e.target.value)}
             ></textarea>
        </div>
        <div className="">
           <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="username">
                Técnico que recibe
           </label>
           <Select
            className="shadow block text-gray-700 text-sm font-bold mb-2"
            value={resive}
            closeMenuOnSelect={true}
            components={animatedComponents}
            options={resiveSelect}
            onChange={(e)=>setResive(e)}
            placeholder = "Seleccionar técnico que recibe"
            />
        </div>
        <div className="">
           <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="username">
               Usuario que hace registro
           </label>
           <Select
            className="shadow block text-gray-700 text-sm font-bold mb-2"
            value={userRegistra}
            closeMenuOnSelect={true}
            components={animatedComponents}
            options={userRegistraSelect}
            onChange={(e)=>setUserRegistra(e)}
            placeholder = "Seleccionar usuario que registra"
            />
        </div>
      </div>
      <div className="grid grid-cols-1 sm:grid-cols-3 mt-2 gap-4 px-3">
        <div className="">
           <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="username">
               Técnico para reparación
           </label>
           <Select
            className="shadow block text-gray-700 text-sm font-bold mb-2"
            value={tecAsign}
            closeMenuOnSelect={true}
            components={animatedComponents}
            options={tecAsignSelect}
            onChange={(e)=>setTecAsign(e)}
            placeholder = "Seleccionar técnico para reparación"
            />
        </div>
        <div className="">
           <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="username">
               Estado de orden
           </label>
           <Select
            className="shadow block text-gray-700 text-sm font-bold mb-2"
            value={estadoOrden}
            closeMenuOnSelect={true}
            components={animatedComponents}
            options={estadoSelect}
            onChange={(e)=>setEstadoOrden(e)}
            placeholder = "Seleccionar estado de orden"
            />
        </div>
      </div>

      <div className="mt-2 p-2 flex mr-3 justify-content-between">
          <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full" onClick={()=>goBack()}>
            Regresar
          </button>
          <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full" onClick={()=>save()} >
            Guardar
          </button>
      </div>
      </div>
    </div>
  );

}

export default NewOrders;
