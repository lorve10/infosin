import React,{ useState, useEffect} from 'react';
import ReactDOM from 'react-dom';
import '../../../../css/app.css';
import axios from 'axios';
import Swal from 'sweetalert2';
import api from '../../Servis/Api';
import NewOrders from './NewOrders';
import ListOrders from './ListOrders';
import OrderState from '../../Context/Orders/OrderState';

function Orders() {

  const [ shofrom, setShofrom ] = useState(1)
  const [ id, setId ] = useState(null)
  const [ data, setData ] = useState({})
  const [ tipoEntrada, setTipoEntrada ] = useState(false);

///regresar a la pagina principal
  const goBack =()=>{
    setShofrom(1)
    setId(null)
    setData({})
  }
  ///funcion para pasar datos a los input para editar
  const dataUpdate = (data, entrada) =>{
    console.log(data);
    console.log("entrada", entrada);
    let accesorios = data[0].accesorios
    let aseguradora = data[0].aseguradora
    let caract_orden = data[0].caract_orden ///
    let distribuidor = data[0].distribuidor
    let estado_fisico = data[0].estado_fisico// estado fis
    let estado_repa = data[0].estado_repa
    let sintoma = data[0].sintoma ///
    let tipo_servicio = data[0].tipo_servicio ///
    let customer_id = data[0].customer_id /// cliente
    let device_id = data[0].device_id /// dispositivo
    let tipo_orden = data[0].tipo_orden /// tipo de orden
    let fecha_entrada = data[0].fecha_entrada // fecha entrada
    let obs_entrada = data[0].obs_entrada
    let marca = data[0].marca
    let userone = data[0].userone
    let usertwo = data[0].usertwo
    let userthree = data[0].userthree

    const dataForAll = {
      userone,
      usertwo,
      userthree,
      accesorios,
      aseguradora,
      caract_orden,
      distribuidor,
      estado_fisico,
      estado_repa,
      sintoma,
      tipo_servicio,
      customer_id,
      device_id,
      tipo_orden,
      fecha_entrada,
      obs_entrada,
      marca
    }

    setId(data[0].id)
    setData(dataForAll)
    setTipoEntrada(entrada);
    setShofrom(2)

  }

  return(
    <div className=" py-2 px-6 bg-white shadow-lg rounded-lg">
    {
      shofrom == 1 ?
      <div className="mt-2">
          <h2 className="text-gray-800 text-3xl font-semibold ml-3">Lista de Ordenes</h2>
      </div>
      :null
    }

      <div className="flex justify-content-end mr-7 mt-3">

        {
          shofrom == 1 ?
          <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full" onClick={()=>setShofrom(2)}>
            Crear orden
          </button>
          :null
        }

      </div>
        {
          shofrom == 1 ?
          <OrderState>
            <ListOrders   dataUpdate ={(data,entrada)=>dataUpdate(data, entrada)} goBack = {()=>goBack()}  />
          </OrderState>
          :shofrom == 2 ?
          <NewOrders list = {data} entrada={tipoEntrada} id = {id} goBack = {()=>goBack()}/>
          :null
        }
    </div>

  );

}

export default Orders;
if (document.getElementById('Orders')) {
    ReactDOM.render(<Orders />, document.getElementById('Orders'));
}
