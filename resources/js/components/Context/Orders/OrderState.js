import React, { useReducer } from 'react';
import axios from 'axios';
import OrdersReducer from './OrdersReducer';
import OrderContext  from './OrderContext';
import api from '../../Servis/Api';
import { ip } from '../../Servis/ip';



//const ip = 'http://127.0.0.1:8000/';


const OrderState = (props) =>{
  const initialState = {
      orders: [],
  }
  const [ state, dispatch ] = useReducer(OrdersReducer, initialState);


  const getOrder = async () =>{
    try {
      const res = await axios.get(ip+"admon/getOrders")
      const data = res.data.data;
      dispatch({ type:'GET_ORDERS', payload: data})
  } catch (error) {
    console.error(error);
  }

}
  const getOrderSelect = async () =>{
    try {
      const res = await axios.get(ip+"admon/getOrderOf")
      const data = res.data.data;
      dispatch({
        type:'GET_ORDERS_OF', payload: data })
    } catch (error) {
      console.error(error);

    }
  }

  const getOrderOut = async () =>{
    try {
      const res = await axios.get(ip+"admon/getOrderOut")
      const data = res.data.data;
      dispatch({
        type:'GET_ORDERS_OUT', payload: data })
    } catch (error) {
      console.error(error);
      console.log(ip);
    }
  }

  return (
      <OrderContext.Provider value={{
          order: state.orders,
          getOrder,
          getOrderSelect,
          getOrderOut
        }}>
        {props.children}
      </OrderContext.Provider>
  )
}

export default OrderState;
