import React,{ useState, useEffect} from 'react';
import ReactDOM from 'react-dom';
import '../../../../css/app.css';
import Swal from 'sweetalert2';
import api from '../../Servis/Api';
import util from "../../means/util";
import { Button, Modal } from 'react-bootstrap';
import Pagination from '../../means/paginate';


function List({ resive, urlget }){

  const [ text, setText ] = useState('')
  const [ listCust, setListCust ] = useState([])
  const [ listContenido, setListContenido ] = useState([])
  ///variables paginador
  const [ listCustBack, setListCustBack ] = useState([])
  const [ currentPage, setCurrentPage ] = useState(1)
  const [ perPage, setPerPage ] = useState(6)
  /// opciones modal
  const [ listDetalle, setListDetalle] = useState([])
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  ///_token
  const [ token, setToken ] = useState(null)
  const ip = 'http://localhost:8000/';
  const [ count, setCount ] = useState(0);



  useEffect(()=>{
    const timer = setTimeout(() => {
      getData()
      setCount(prev=>prev + 1);
    }, 10000);
    return () => clearTimeout(timer)
  },[count])

  useEffect(()=>{
    getData()
    let csrf = document.querySelector('meta[name="csrf-token"]').content;
    setToken(csrf);
  },[])

  const getData = async () =>{
    api.get('tecnico/getGarantia')
    .then(response =>{
      setListCust(response.data)
      setListCustBack(response.data)
      setListContenido(response.data)
    }).catch(error => console.error("Error: ", error))
  }


  const updateCurrentPage = async (number) => {
    await setListCust([])
    await setCurrentPage(number)
    const dataNew = util.paginate(listCustBack,number,perPage)
    await setListCust(dataNew)
  }

///funcion para filtrar datos de Ordenes en la tabla
  const filterOrderText = (event) => {
      var text = event.target.value
      const data = listContenido

      const newData = data.filter(function(item){
          var textAll = " "
          textAll = textAll+" "+item.orden
          textAll = textAll+" "+item.nombre.toUpperCase()
          textAll = textAll+" "+item.servi.tipo_servi.toUpperCase()
          textAll = textAll+" "+item.id_doc
          textAll = textAll+" "+item.nombre

          const textData = text.toUpperCase()
          return textAll.indexOf(textData) > -1
      })
      setListCust(newData)
      setText(text)
    }

///funcion editar que pasa el valor a la funcion dataUpdate
  const editar = (data)=>{
    props.dataUpdate(data);
  }

//activar detalles
  const detalles = (data) =>{
    handleShow();
    setListDetalle(data);
  }
  const repart = (data) =>{
    if (data.estado_repa == 3) {
        util.MessageError("Esta orden ya fue reparada")
    }
    else {
      resive(data)
    }
  }

return(
  <div className="relative mt-4">

    <div className="flex justify-content-center">
      <div className="col-6">
        <input className="text-center shadow appearance-none border rounded-full w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
        type="text" placeholder="Buscar orden"
        value = {text}
        onChange={(e) => filterOrderText(e)}
        />
      </div>
    </div>

    <section className="container mx-auto p-6 font-mono">
      <div className="w-full mb-8 overflow-hidden rounded-lg shadow-lg">
        <div className="w-full overflow-x-auto">
      <div className="">
        <table className="w-full ">
          <thead className="">
            <tr className="text-md font-semibold tracking-wide text-center text-gray-900 bg-blue-400 uppercase border-b border-gray-600">
              <th className="px-2 py-3">Orden</th>
              <th className="px-2 py-3">Repara</th>
              <th className="px-2 py-3">Tipo orden</th>
              <th className="px-2 py-3">Tipo servicio</th>
              <th className="px-2 py-3">Carácter de orden</th>
              <th className="px-2 py-3">Documento</th>
              <th className="px-2 py-3">Nombre</th>
              <th className="px-2 py-3">Detalles</th>
            </tr>
          </thead>
          <tbody className="bg-white">
            {
              listCust.map((item,index)=>{
                return(
                  <tr className={item.estado_repa == 1 ? "bg-red-300 border-b-2 border-gray-200" :item.estado_repa == 2 ? "bg-yellow-300  border-b-2 border-gray-200" : item.estado_repa == 3 ? "bg-green-500 border-b-2 border-gray-200" : null  } key={index}>
                    <td className="text-center ml-2 font-semibold border">
                      {item.orden}
                    </td>
                    <td className="px-1 py-3 text-sm text-center border">
                      <span className="material-icons cursor-pointer"  onClick={()=>repart(item)} >
                        <span className="material-icons" style={{fontSize:'42'}}>
                          engineering
                        </span>
                      </span>
                    </td>
                    <td className="px-2 py-3 border">
                      <span className="text-center ml-2 font-semibold ">{item.tipo_orden == 1? 'Garantia' :item.tipo_orden == 2 ?'Cobrable' :'Extendida' }</span>
                    </td>
                    <td className="px-2 py-3 border text-center">
                      <span>{item.servi.tipo_servi}</span>
                    </td>
                    <td className="px-2 py-3 border">
                      <span>{item.carat.caract_orde}</span>
                    </td>

                    <td className="px-2 py-3 border">
                      <span>{item.id_doc}</span>
                    </td>

                    <td className="py-3 border px-2">
                      <span>{item.nombre}</span>
                    </td>
                    <td className="px-0 py-3 text-sm text-center border">
                      <span className="material-icons cursor-pointer" onClick={()=>detalles(item)}>
                        <span className="material-icons">
                        visibility
                        </span>
                      </span>
                    </td>
                  </tr>
                );
              })
            }
          </tbody>
        </table>
      </div>
      <div className="d-flex col-md-12 col-12 justify-content-end mt-2">
        <Pagination currentPage={currentPage} perPage={perPage} countdata={listCustBack.length} updateCurrentPage={(number)=>updateCurrentPage(number)}/>
      </div>
    </div>
  </div>
  </section>
  <Modal show={show} onHide={handleClose}>
    <Modal.Header closeButton>
      <Modal.Title>Detalles cliente</Modal.Title>
    </Modal.Header>
    <Modal.Body>
      <div className="container">

          <div>
            <p><strong>Orden: </strong> {listDetalle.orden}</p>
            <p><strong>Tipo de orden:</strong> {listDetalle.tipo_orden == 1? 'Garantia' :listDetalle.tipo_orden == 2 ?'Cobrable' :'Extendida'}</p>
            {
              listDetalle.tipo_servi ?
              <div>
                <p><strong>Tipo de servicio:</strong> {listDetalle.servi.tipo_servi}</p>
                <p><strong>Caracter de orden:</strong> {listDetalle.carat.caract_orde}</p>
              </div>
              :null
            }
            <p><strong>Documento cliente:</strong> {listDetalle.id_doc}</p>
            <p><strong>Nombre:</strong> {listDetalle.nombre}</p>
            <p><strong>Dirección:</strong> {listDetalle.direccion}</p>
            <p><strong>Barrio:</strong> {listDetalle.barrio}</p>
            <p><strong>Ciudad:</strong> {listDetalle.ciudad}</p>
            <p><strong>Email:</strong> {listDetalle.email}</p>
            <p><strong>celular:</strong> {listDetalle.celular}</p>
            <p><strong>Telefono de casa:</strong> {listDetalle.telcasa}</p>
            <p><strong>Telefono trabajo:</strong> {listDetalle.teltrabajo}</p>
            <p><strong>Factura:</strong> {listDetalle.factura}</p>
            <p><strong>seguimiento:</strong> {listDetalle.seguimiento}</p>
            <p><strong>Aseguradora:</strong>
               {
                 listDetalle.aseguradora ? listDetalle.aseguradora.marc : ""
               }
             </p>
            {
              listDetalle.marca ?
              <div>
                <p><strong>Marca:</strong> {listDetalle.brands.marc == null ? "" :listDetalle.brands.marc}</p>
                <p><strong>Articulo:</strong> {listDetalle.items.articu == null ? "" :listDetalle.items.articu}</p>
                <p><strong>linea:</strong> {listDetalle.oline.line == null ? "" :listDetalle.oline.line}</p>
              </div>
              :null
            }
            <p><strong>Modelo:</strong> {listDetalle.modelo}</p>
            <p><strong>Serie:</strong> {listDetalle.serie}</p>
            <p><strong>Fecha compra:</strong> {listDetalle.fecha_compra}</p>
            <p><strong>Distribuidor:</strong> {listDetalle.distribuidor}</p>
            <p><strong>Accesorios:</strong> {listDetalle.accesorios}</p>
            <p><strong>Fecha entrada:</strong> {listDetalle.fecha_entrada}</p>
            <p><strong>Estado fisico:</strong> {listDetalle.estado_fisico}</p>
            <p><strong>sintoma:</strong> {listDetalle.sintoma	}</p>
            <p><strong>observacione entrada:</strong> {listDetalle.obs_entrada}</p>
            {
              listDetalle.tecrecibe ?
                <div>
                  <p><strong>Tecnico resive:</strong> {listDetalle.user.name}</p>
                  <p><strong>Quien registra:</strong> {listDetalle.user2.name}</p>
                  <p><strong>Tecnico asignado:</strong> {listDetalle.user3.name}</p>
                </div>
              :null
            }
          </div>

      </div>
    </Modal.Body>
    <Modal.Footer>
      <Button variant="secondary" onClick={handleClose}>
        Cerrar
      </Button>
    </Modal.Footer>
  </Modal>
</div>
)


}
export default List;
