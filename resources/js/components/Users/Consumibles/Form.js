import React, { useState, useEffect } from 'react';
import api from '../../Servis/Api';
import util  from '../../means/util';
import Select from 'react-select';
import makeAnimated from 'react-select/animated';

function Form({ list, goBack, id}){

    const [ fechaRevi, setFechaRevi ] = useState('');
    const [ descFalla, setDescFalla ] = useState('');
    const [ obsReparacion, setObsReparacion ] = useState('');
    const [ descReparacion, setDescReparacion ] = useState('');
    const [ fechaReparacion, setFechaReparacion ] = useState('');

    const [ estadoOrden, setEstadoOrden ] = useState('');
    const [ estadoSelect, setEstadoSelect ] = useState([]);
    const [animatedComponents, setAnimatedComponents] = useState(makeAnimated);

    useEffect(()=>{
      consulta()
      validar()
    },[])

    
    const validar  = () =>{
      setFechaRevi(list.fecha_revision);
      setDescFalla(list.desc_falla)
    }

    const consulta = () =>{
      api.get('admon/getStatus')
      .then(response=>{
          let cat = [];
          let res = response.data;
          console.log('data', res);
          response.data.map(item=>{ const data = { value:item.id,  label:item.nombreStado }
              cat.push(data)
              setEstadoSelect(cat)
          })

          if (id > 0 ){
            let filtro = res.filter(f=>f.id == list.estado_repa)
            const data = {
              value: filtro[0].id,
              label: filtro[0].nombreStado
            }
            setEstadoOrden(data)
          }

      }).catch(error => console.error("Error: ", error))
    }

    const save = () =>{
    var message = ''
    var error = false

      const data = new FormData;
      data.append('id', list.id)
      data.append('fecha_revision', fechaRevi)
      data.append('desc_falla', descFalla)
      data.append('obs_reparacion', obsReparacion)
      data.append('desc_reparacion', descReparacion)
      data.append('fecha_reparacion', fechaReparacion)
      data.append('estado_repa', estadoOrden.value)
      api.post('tecnico/saveCobrable',data).then(data=>{
        console.log(data);
          if (data.success){
              util.MessageSuccess('Ya se reparo el dispositivo');
              goBack()
          }
          else {
              util.MessageError('!UPS¡ ocurrio un error');
          }
      }).catch(console.warn())

    }


    return(
      <div >
        <div className="flex mb-3">
            <h2 className="text-gray-800 text-3xl font-semibold ml-3">Iniciando reparación ( {list.items.articu} - Modelo:  {list.modelo}  )</h2>
        </div>

        <div className="overflow-auto h-5/6">
          <div className="grid grid-cols-1 sm:grid-cols-2 mt-2 gap-4 px-3">
              <div className="">
                 <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="password">
                      Fecha de revisión
                  </label>
                   <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                     value = {fechaRevi}
                     onChange = {(e)=>setFechaRevi(e.target.value)}
                     type="date" placeholder="Fecha de revición"/>
              </div>
              <div className="">
                 <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="username">
                     Descripción de la falla
                 </label>
                 <textarea className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                   type="text" placeholder="Descripción de la falla"
                   value = {descFalla}
                   onChange = {(e)=>setDescFalla(e.target.value)}
                   ></textarea>
              </div>
          </div>
          <div className="grid grid-cols-1 sm:grid-cols-2 mt-2 gap-4 px-3">
            <div className="">
               <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="username">
                   Observación de reparación
               </label>
               <textarea className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                 type="text" placeholder="Observación de reparación"
                 value = {obsReparacion}
                 onChange = {(e)=>setObsReparacion(e.target.value)}
                 ></textarea>
            </div>
              <div className="">
                 <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="username">
                     Descripción de reparación
                 </label>
                 <textarea className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                   type="text" placeholder="Descripción de reparación"
                   value = {descReparacion}
                   onChange = {(e)=>setDescReparacion(e.target.value)}
                   ></textarea>
              </div>

          </div>
          <div className="grid grid-cols-1 sm:grid-cols-2 mt-2 gap-4 px-3">
            <div className="">
               <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="password">
                    Fecha de reparación
                </label>
                 <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                   value = {fechaReparacion}
                   onChange = {(e)=>setFechaReparacion(e.target.value)}
                   type="date" placeholder="Fecha de revición"/>
            </div>
            <div className="">
               <label className=" block text-gray-700 text-sm font-bold mb-2" htmlFor="username">
                   Estado de la orden
               </label>
               <Select
                className="shadow block text-gray-700 text-sm font-bold mb-2"
                value={estadoOrden}
                closeMenuOnSelect={true}
                components={animatedComponents}
                options={estadoSelect}
                onChange={(e)=>setEstadoOrden(e)}
                placeholder = "Seleccionar estado de la orden"
                />
            </div>

          </div>
          <div className="mt-2 p-2 flex mr-3 justify-content-between">
              <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full" onClick={()=>goBack()}>
                Regresar
              </button>
              <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full" onClick={()=>save()} >
                Guardar
              </button>
          </div>

      </div>

      </div>
    )
}

export default Form;
