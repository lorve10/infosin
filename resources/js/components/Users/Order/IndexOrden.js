import React,{ useState, useEfFect } from 'react';
import ReactDOM from 'react-dom';
import List from '../Consumibles/List';
import Form from '../Consumibles/Form';

function IndexOrden() {
  const [ shofrom, setShofrom ] = useState(1);
  const [ id, setId ] = useState(null);
  const [ info, setInfo ] = useState({});

  const resive = (data) =>{
      setShofrom(2);
      setId(data.id);
      setInfo(data)
  }
  const goBack = () =>{
    setShofrom(1);
  }
  return(
    <div className=" py-2 px-8 bg-white shadow-lg rounded-lg">
      {
        shofrom == 1 ?
        <div>
          <div className="flex justify-content-start mt-2 ml-4">
              <div className="bg-green-500 text-white p-2 mr-2 rounded-lg">Orden reparada</div>
              <div className="bg-red-300 text-white p-2 rounded-lg">Orden pendiente</div>
                <div className="bg-yellow-300 text-white p-2  mr-2 ml-2 rounded-lg">Orden por repuesto</div>

          </div>
          <div className="flex justify-content-end mr-7">
            <div>
                <h2 className="text-gray-800 text-3xl font-semibold ml-3">Lista de reparaciones</h2>
            </div>
          </div>
        </div>
        :null
      }
      {
        shofrom ==  1 ?
        <List resive={(data)=>resive(data)} urlget={'55'}/>
      : shofrom == 2 ?
        <Form list= {info} id = {id} goBack = {()=>goBack()}/>
      : null
      }
    </div>
  )
}
export default IndexOrden;
if(document.getElementById('IndexOrden')){
   ReactDOM.render(<IndexOrden />, document.getElementById('IndexOrden'));
}
