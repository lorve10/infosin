import React,{ useState, useEffect, Component} from 'react';
import ReactDOM from 'react-dom';
import '../../../css/app.css';
import api from '../Servis/Api';
import Swal from 'sweetalert2';
import { Link } from 'react-router-dom';

function Login() {

  const [ error, setError ] = useState(null);
  const [ email, setEmail ] = useState(null);
  const [ password, setPassword ] = useState(null);


  const MessageError = async (data) => {
      Swal.fire({
        title: 'Error',
        text: data,
        icon: 'warning',
      })
    }
    const MessageSuccess = async (data) => {
      Swal.fire({
        text: data,
        icon: 'success',
      })
    }

  const onClickLogin = async () => {
    const data = new FormData()
    data.append('email', email)
    data.append('password', password)
    api.post('login_admin',data).then(response=>{
      if(response){
        MessageSuccess("Bienvenido")
        window.location.href = "admon/welcome"
      }
      else{
        MessageError("Nombre de usuario y contraseña incorrectos")

      }
    })
  }

console.log(email);
  return(
    <div class="h-screen font-sans login bg-cover">
    <div class="container mx-auto h-full flex flex-1 justify-center items-center">
        <div class="w-full max-w-lg">
          <div class="leading-loose">
            <div class="max-w-sm m-4 p-10 bg-white bg-opacity-25 rounded shadow-xl">
                <p class="text-white font-medium text-center text-lg font-bold">LOGIN</p>
                  <div class="">
                    <label class="block text-sm text-white" for="email">Usuario</label>
                    <input class="w-full px-5 py-1 text-gray-700 bg-gray-300 rounded focus:outline-none focus:bg-white"
                       value={email} onChange={(event)=>setEmail(event.target.value)}
                       type="email" id="email"  placeholder="Digite usuario" />
                  </div>
                  <div class="mt-2">
                    <label class="block  text-sm text-white">Contraseña</label>
                     <input class="w-full px-5 py-1 text-gray-700 bg-gray-300 rounded focus:outline-none focus:bg-white"
                      value={password} onChange={(event)=>setPassword(event.target.value)}
                      type="password" id="password" placeholder="Digite a contraseña" arial-label="password" required/>
                  </div>

                  <div class="mt-4 justify-center	text-center">
                    <button class="px-4 py-1 align-content: center text-white font-light tracking-wider bg-gray-900 hover:bg-gray-800 rounded"
                      onClick={()=>onClickLogin()}
                      >Entrar</button>
                  </div>
                  <div class="text-center">
                  </div>
            </div>

          </div>
        </div>
      </div>
    </div>

  );

}

export default Login;
if (document.getElementById('Login')) {
    ReactDOM.render(<Login />, document.getElementById('Login'));
}
