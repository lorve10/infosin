import React ,{ useEffect, useState } from 'react';
import axios from 'axios';
import { ip } from './ip';
const http = {}
// let csrf = document.querySelector('meta[name="csrf-token"]').content;
// console.log(csrf);




http.get = async (url) =>{
  try {
    const res = await axios.get(ip + url).then(response=>{
          return response.data;

      }).catch(error => console.log("Error: ", error))
      return res;

  } catch (e) {
    console.log(e);
  }
}

http.post = async (url, data)=>{
  try {
    let res = await axios.post(ip+ url, data,{
      data: {
        _token: '{{ csrf_token() }}'
      }
    }).then(response=>{
        return response.data;
    }).catch(error => console.log("Error: ", error))
      return res;
    } catch (e) {
      console.log(e);
  }
}

export default http;

