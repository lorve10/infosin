<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{
    use HasFactory;

    protected $table = 'customers';

    protected $fillable = [
        'id',
        'cust_nombre',
        'cust_id_doc',
        'cust_direccion',
        'cust_barrio',
        'cust_ciudad',
        'cust_departamento',
        'cust_email',
        'cust_celular',
        'cust_telcasa',
        'cust_teltrabajo',
        'id_usuario'
    ];

    public function municipio(){
      return $this->HasOne("App\Models\Municipio","id_municipio","cust_ciudad");
    }

    public function departament(){
      return $this->HasOne("App\Models\Departament","id_departamento","cust_departamento");
    }


}
