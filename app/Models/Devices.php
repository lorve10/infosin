<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Devices extends Model
{
    use HasFactory;

    protected $table = 'devices';

    protected $fillable = [
      'id',
      'marca',
      'articulo',
      'linea',
      'modelo',
      'serie',
      'observaciones'
    ];

    // Funciones para relacionar las tablas
    public function brands(){
      return $this->HasOne("App\Models\Brands", "id", "marca")->where("deleted",0);
    }
    public function items(){
      return $this->HasOne("App\Models\Items", "id", "articulo")->where("deleted",0);
    }
    public function oline(){
      return $this->HasOne("App\Models\linea", "id",  "linea")->where("deleted",0);
    }
}
