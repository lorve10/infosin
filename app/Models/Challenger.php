<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Challenger extends Model
{
    use HasFactory;

    protected $table = 'challenger_parts';

    protected $fillable = [
        'id',
        'pedido',
        'partenumero',
        'descripcion',
        'cantidad',
        'modelo_part',
        'ordenkalley',
        'tecnico',
        'fechapedidotec',
        'solicita',
        'fechapedido',
        ///
        'recibe',
        'fechallegada',
        'cantidadllega',
        'observaciones',
        'order_id',
        'cust_id',
        'estado',
    ];

    // Funciones para relacionar las tablas
    public function ordenkall(){
      return $this->HasOne("App\Models\Orders", "id", "ordenkalley");
    }
    public function tecnicoped(){
      return $this->HasOne("App\Models\users", "id", "tecnico");
    }
    public function tecnicorecibe(){
      return $this->HasOne("App\Models\users", "id", "recibe");
    }
}
