<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\ResetPasswordNotification;


class Users extends Authenticatable implements MustVerifyEmail
{
    use HasFactory;

    protected $table = 'users';

    protected $fillable = [
        'id',
        'name',
        'iddoc',
        'address',
        'city',
        'departamento',
        'neighborhood',
        'movil',
        'phone',
        'position',
        'birth_date',
        'hired_date',
        'status',
        'notes',
        'email',
        'genero',
        'email_verified_at',
        'password',
        'rol',
    ];

    protected $hidden = [
      'password',
    ];

    public function municipio(){
      return $this->HasOne("App\Models\Municipio","id_municipio","city");
    }

    public function departament(){
      return $this->HasOne("App\Models\Departament","id_departamento","departamento");
    }

    public function posicion(){
      return $this->HasOne("App\Models\Cargo", "id","position");
    }
}
