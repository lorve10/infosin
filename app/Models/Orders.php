<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    use HasFactory;
    protected $table = "orders";

    protected $fillable = [
        'id',
        'orden',//
        'tipo_orden',//
        'tipo_servicio',//
        'caract_orden',//
        'nombre',//
        'id_doc',//
        'direccion',//
        'barrio',//
        'ciudad',//
        'email',//
        'celular',//
        'telcasa',//
        'teltrabajo',//
        'factura',//
        'seguimiento',//
        'aseguradora',//
        'marca',//
        'articulo',//
        'linea',//
        'modelo',//
        'serie',//
        'fecha_compra',//
        'distribuidor',//
        'accesorios',//
        'fecha_entrada',//
        'estado_fisico',//
        'sintoma',//
        'obs_entrada',//
        'tecrecibe',//
        'regrecibe',//
        'tecasignado',//
        'estado_repa',///

        'fecha_revision',
        'desc_falla',
        'obs_reparacion',
        'desc_reparacion',
        'fecha_reparacion',
        'fecha_entrega',
        'tecentrega',
        'regentrega',
        'obs_entrega',
        'status_entrega',
        'razon_devolucion',
        'customer_id',
        'device_id',
        'reingreso'
    ];



   public function estado(){
      return $this->HasOne("App\Models\Order_status","id","estado_repa")->where("deleted",0);
    }
   public function servi(){
      return $this->HasOne("App\Models\Order_types","id","tipo_servicio");
    }
    public function carat(){
       return $this->HasOne("App\Models\Order_traits","id","caract_orden");
   }

   public function brands(){
     return $this->HasOne("App\Models\Brands", "id", "marca")->where("deleted",0);
   }

   public function aseguradora(){
     return $this->HasOne("App\Models\Brands", "id", "aseguradora")->where("deleted",0);
   }

   public function items(){
     return $this->HasOne("App\Models\Items", "id", "articulo")->where("deleted",0);
   }
   public function oline(){
     return $this->HasOne("App\Models\linea", "id",  "linea")->where("deleted",0);
   }
   public function user(){
     return $this->HasOne("App\Models\Users", "id",  "tecrecibe");
   }
   public function user2(){
     return $this->HasOne("App\Models\Users", "id",  "regrecibe");
   }
   public function user3(){
     return $this->HasOne("App\Models\Users", "id",  "tecasignado");
   }

}
