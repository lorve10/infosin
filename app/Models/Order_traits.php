<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order_traits extends Model
{
    use HasFactory;

    protected $table = 'order_traits';

    protected $fillable = [
        'id',
        'caract_orde',
        'desc_codigo',
        'deleted'
    ];
}
