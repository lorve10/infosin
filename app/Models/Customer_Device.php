<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer_Device extends Model
{
    use HasFactory;

    protected $table = 'customer_device';

    protected $fillable = [
      'id',
      'customer_id',
      'device_id'
    ];

    public function persona(){
      return $this->HasOne("App\Models\Customers", "id", "customer_id");
    }
    public function dispositivo(){
      return $this->HasOne("App\Models\Devices", "id", "device_id");
    }
}
