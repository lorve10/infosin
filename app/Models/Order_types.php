<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order_types extends Model
{
    use HasFactory;

    protected $table = 'order_types';

    protected $fillable = [
        'id',
        'tipo_servi',
        'desc_codigo',
        'deleted'
    ];
}
