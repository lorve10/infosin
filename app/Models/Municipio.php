<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Municipio extends Model
{
  use HasFactory;
    protected $table='municipios';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        //Id de la categoria
        'id_municipio',
        //nombre
        'municipio',
        //id departament
        'id_departamento',
        // estado
        'estado',
    ];

}
