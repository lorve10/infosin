<?php

namespace App\Http\Controllers\ADMIN;
use Log;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Devices;
use App\Models\Customers;
use App\Models\Customer_Device;

class DevicesController extends Controller
{
    /// Función para crear la vista, llamarla, está en resources, views, admon, DevicesAdmon
    public function Devices()
    {
      return view('admon.DevicesAdmon');
    }

    ///Función para guardar y editar
    public function saveDevices(Request $request)
    {
      try {
        $id = $request['id'];
        $customer_id = $request['customerId'];
        $customerInfo = Customers::find($customer_id);

        $data['marca'] = $request['marca'];
        $data['articulo'] = $request['articulo'];
        $data['linea'] = $request['linea'];
        $data['modelo'] = $request['modelo'];
        $data['serie'] = $request['serie'];
        $data['observaciones'] = $request['observacion'];

        if($id > 0){
          Devices::find($id)->update($data);
        }

        else{
          $id_devices = Devices::create($data);
          // Log::info($id_devices);
          // Log::info($id_devices->id);

          $dataRelacion['customer_id'] = $customerInfo->id;
          $dataRelacion['device_id'] = $id_devices->id;
          Customer_Device::create($dataRelacion);
        }
        return response()->json([ 'message' => "Successfully created", "success" => true ], 200);

      } catch (\Exception $e) {
        return response()->json([ 'message' => $e->getMessage(), "success" => false ], 500);
      }

    }

    ///Función para listar
    public function listDevices(){
      try {
        $data = Devices::with('brands','items','oline')->get();
        return response()->json(['message' => "Successfully loaded", 'data' => $data, "success" => true], 200);
      } catch (\Exception $e) {
        return response()->json(['message' => $e->getMessage(), 'success' => false ], 500);

      }
    }

}
