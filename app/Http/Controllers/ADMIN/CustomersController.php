<?php

namespace App\Http\Controllers\ADMIN;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Customers;

class CustomersController extends Controller
{
    public function Custumer()
    {
      return view('admon.CustomersAdmon');
    }

    public function saveCustomers(Request $request)
    {
      try {

        $id = $request['id'];
    
        $data['cust_nombre'] = $request['nombre'];
        $data['cust_id_doc'] = $request['documento'];
        $data['cust_direccion'] = $request['direccion'];
        $data['cust_barrio'] = $request['barrio'];
        $data['cust_ciudad'] = $request['ciudad'];
        $data['cust_departamento'] = $request['departamento'];
        $data['cust_email'] = $request['email'];
        $data['cust_celular'] = $request['celular'];
        $data['cust_telcasa'] = $request['teleCasa'];
        $data['cust_teltrabajo'] = $request['teleTrabajo'];

        if($id > 0){
              Customers::find($id)->update($data);
        }
        else{
              Customers::create($data);
          }
        return response()->json([ 'message' => "Successfully created", 'success' => true ], 200);

      } catch (\Exception $e) {
        return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);

      }

    }

    public function listCustomers(){
      try {
        $data = Customers::with("municipio","departament")->get();
        return response()->json([
          'message' => "Successfully loaded",
          'data'=> $data,
          'success' => true
        ], 200);
      } catch (\Exception $e) {
        return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);
      }

    }
}
