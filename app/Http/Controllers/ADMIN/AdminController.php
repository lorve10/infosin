<?php

namespace App\Http\Controllers\ADMIN;


use Log;
use DB;
use Auth;
use App\Models\Admon;
use App\Models\Users;
use App\Models\Brands;
use App\Models\Items;
use App\Models\linea;
use App\Models\Cargo;
use App\Models\Departament;
use App\Models\Municipio;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Notification;
use App\Notifications\InvoicePaid;


class AdminController extends Controller
{

  public function encrypted()
  {
   $pass = "12345678";
   $encript = Hash::make($pass);
   echo $encript;
  }

  public function logout_admin()
  {
    //logout user
   auth()->logout();
   // redirect to homepage
   return redirect('/');
  }

///traer datos de la marca
  public function Marca(){
    try {
      $data = Brands::where('deleted',0)->get();
      return response()->json(['message' => "Successfully loaded", 'data'=> $data, 'success' => true ], 200);
    } catch (\Exception $e) {
      return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);
    }
  }

// traer datos de articulos
  public function Articulo(){
    try {
      $data = Items::where('deleted',0)->get();
      return response()->json(['message' => "Successfully loaded", 'data'=> $data, 'success' => true ], 200);
    } catch (\Exception $e) {
      return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);
    }
  }


  // traer datos de Lineas
    public function Linea(){
      try {
        $data = linea::where('deleted',0)->get();
        return response()->json(['message' => "Successfully loaded", 'data'=> $data, 'success' => true ], 200);
      } catch (\Exception $e) {
        return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);
      }
    }

    // traer datos de Cargo
      public function Cargo(){
        try {
          $data = Cargo::where('deleted',0)->get();
          return response()->json(['message' => "Successfully loaded", 'data'=> $data, 'success' => true ], 200);
        } catch (\Exception $e) {
          return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);
        }
      }

    // traer datos de Departamento
    public function Departamento(){
        try {
          $data = Departament::get();
          return response()->json(['message' => "Successfully loaded", 'data'=> $data, 'success' => true ], 200);
        } catch (\Exception $e) {
          return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);
        }
    }

    // traer datos de Municipio
    public function Municipio()
    {
      try {
        $data = Municipio::get();
        return response()->json(['message' => "Successfully loaded", 'data'=> $data, 'success' => true ], 200);
      } catch (\Exception $e) {
        return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);
      }
    }

  public function start()
  {
   return view('admon.Welcome');
  }




}
