<?php

namespace App\Http\Controllers\ADMIN;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\linea;
use App\Models\Devices;

class LineaController extends Controller
{
  public function Linea()
  {
    return view('admon.lineaController');
  }

  ///Función para guardar y editar
  public function saveLinea(Request $request)
  {
    try {
      $id = $request['id'];
      $data['line'] = $request['linea'];
      if($id > 0){
            linea::find($id)->update($data);
      }
      else{
            linea::create($data);
      }
      return response()->json([ 'message' => "Successfully created", "success" => true ], 200);
    } catch (\Exception $e) {
      return response()->json([ 'message' => $e->getMessage(), "success" => false ], 500);
    }
  }

  public function deletedLinea(Request $request){
    try {
      $id = $request['id'];
      $existe = Devices::where('linea','=',$id)->first();

      if($existe){
        $otro = 2;
        return response()->json([ "data" => $otro,'message' => "Successfully created", "success" => true ], 200);
      }
      else {
        linea::where('id', $request['id'])->update([
        'deleted'=> 1
      ]);
        $data = 1;
        return response()->json([ "data" => $data, 'message' => "Successfully created", "success" => true ], 200);
      }
    } catch (\Exception $e){
      return response()->json([ 'message' => $e->getMessage(), "success" => false], 500);
    }
  }

}
