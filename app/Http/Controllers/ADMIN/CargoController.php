<?php

namespace App\Http\Controllers\ADMIN;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Cargo;
use App\Models\Users;
use Log;

class CargoController extends Controller
{
  public function Cargo()
  {
    return view('admon.CargoController');
  }

  // Funcion para guardar y Editar
  public function saveCargo(Request $request)
  {
    try{
      $id = $request['id'];
      $data['cargo'] = $request['cargo'];
      Log::info($data);

      if($id > 0){
        Cargo::find($id)->update($data);
      }
      else{
        Cargo::create($data);
      }
      return response()->json(['message' => "Successfully created", "success" => true], 200);
    } catch (\Exception $e){
      return response()->json(['message' => $e->getMessage(),"success"=>false], 500);
    }
  }

  public function deleted(Request $request){
    try {
      $id = $request['id'];
      $existe = Users::where('position','=',$id)->first();

      if($existe){
        $otro = 2;
        return response()->json([ "data" => $otro,'message' => "Successfully created", "success" => true ], 200);
      }
      else {
        Cargo::where('id', $request['id'])->update([
        'deleted'=> 1
      ]);
        $data = 1;
        return response()->json([ "data" => $data, 'message' => "Successfully created", "success" => true ], 200);
      }
    } catch (\Exception $e){
      return response()->json([ 'message' => $e->getMessage(), "success" => false], 500);
    }
  }
}
