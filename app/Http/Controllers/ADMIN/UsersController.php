<?php

namespace App\Http\Controllers\ADMIN;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Users;
use App\Models\Admon;
use Illuminate\Support\Facades\Hash;
use Log;

class UsersController extends Controller
{
    public function Users (){
      return view('admon.UsersAdmon');
    }

    public function saveUsers(Request $request) {
      try {
        $id= $request['id'];
        $data['name'] = $request['nombre'];
        $data['iddoc'] = $request['documento'];
        $data['address'] = $request['direccion'];
        $data['city'] = $request['ciudad'];
        $data['departamento'] =$request['departamento'];
        $data['neighborhood'] = $request['barrio'];
        $data['movil'] = $request['celular'];
        $data['phone'] = $request['telefono'];
        $data['position'] = $request['posicion'];
        $data['birth_date'] = $request['fechaNacimiento'];
        $data['hired_date'] = $request['fechaContratacion'];
        $data['status'] = $request['estadoEmpleado'];
        $data['notes'] = $request['observaciones'];
        $data['email'] = $request['email'];
        $data['genero'] = $request['genero'];
        $data['rol'] = 2;
        $data['password'] = Hash::make($request['password']);
        Log::info($data);
        if($id > 0){
              Users::find($id)->update($data);
        }
        else{
              Users::create($data);
          }
        return response()->json([ 'message' => "Successfully created", 'success' => true ], 200);

      } catch (\Exception $e) {
        return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);

      }

    }

    public function listUsers(){
      try {
        $data = Users::with("municipio","departament","posicion")->where('status', 'Activo')->orWhere('status','=','Inactivo')->get();
        return response()->json([
          'message' => "Successfully loaded",
          'data'=> $data,
          'success' => true
        ], 200);
      } catch (\Exception $e) {
        return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);
      }

    }
    public function userId(Request $request){
      try {
        Log::info("aqui id respuest");
        Log::info($request['id']);

        $data = Users::where('id',$request['id'])->get();
        return response()->json([
          'message' => "Successfully loaded",
          'data'=> $data,
          'success' => true
        ], 200);
      } catch (\Exception $e) {
        return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);
      }

    }
}
