<?php

namespace App\Http\Controllers\ADMIN;

use Log;
use PDF;
use Mail;
use App\Models\Order_traits;
use App\Models\Order_types;
use App\Models\Order_status;
use App\Models\Orders;
use App\Models\Users;
use App\Models\History;
use App\Models\Brands;
use App\Models\Devices;
use App\Models\Customers;
use App\Models\Customer_Device;
use App\Models\Challenger;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class OrdersController extends Controller
{
  public function Orders()
  {
    return view('admon.OrdersAdmon');
  }

  public function saveOrders(Request $request)
  {

    try {
      $id = $request['id'];
      Log::info("oooe");


      $client = $request['customer_id'];
      $dispo  = $request['device_id'];
      Log::info($dispo);
      Log::info($client);
      Log::info("holaaa estado");
      Log::info($request['estado_repa']);
      Log::info($request['entrada']);


      $custumer = Customers::find($client);
      $res = Devices::find($dispo);

      $data['tipo_orden'] = $request['tipo_orden'];
      $data['tipo_servicio'] = $request['tipo_servicio'];
      $data['caract_orden'] = $request['caract_orden'];
      $data['nombre'] = $custumer->cust_nombre;
      $data['id_doc'] = $custumer->cust_id_doc;
      $data['direccion'] = $custumer->cust_direccion;
      $data['barrio'] = $custumer->cust_barrio;
      $data['ciudad'] = $custumer->cust_ciudad;
      $data['email'] = $custumer->cust_email;
      $data['celular'] = $custumer->cust_celular;
      $data['telcasa'] = $custumer->cust_telcasa;
      $data['teltrabajo'] = $custumer->cust_teltrabajo;
      $data['marca'] = $res->marca;
      $data['articulo'] = $res->articulo;
      $data['linea'] = $res->linea;
      $data['modelo'] = $res->modelo;
      $data['serie'] = $res->serie;
      $data['fecha_compra'] = $request['fecha_compra'];
      $data['seguimiento'] = $request['seguimiento'];
      $data['aseguradora'] = $request['aseguradora'];
      $data['accesorios'] = $request['accesorios'];
      $data['fecha_entrada'] = $request['fecha_entrada'];
      $data['estado_fisico'] = $request['estado_fisico'];
      $data['sintoma'] = $request['sintoma'];
      $data['obs_entrada'] = $request['obs_entrada'];
      $data['tecrecibe'] = $request['tecrecibe'];
      $data['regrecibe'] = $request['regrecibe'];
      $data['tecasignado'] = $request['tecasignado'];
      $data['estado_repa'] = $request['estado_repa'];
      $data['customer_id'] = $custumer->id;
      $data['device_id'] = $res->id;

      if($request['tipo_orden'] == 1 ){
            $res = Brands::find($res->marca);
            $prefijo = $res->id;
            $data['fecha_compra'] = $request['fecha_compra'];
            $data['factura'] = $request['factura'];
            $data['distribuidor'] = $request['distribuidor'];

            ///////// validacion de editar ordenes
            if ($request['entrada'] == 3) {
              Log::info("entro aquii");
                  Orders::find($id)->update($data);
                  $j = strval($id);
                  $e = strval($prefijo);
                    Orders::find($id)->update([
                      'orden'=>$e.'-'.$j
                    ]);
                    /// variable historial
                      $data2['orden'] = $id;
            }
            if($request['entrada'] == 2){
              Log::info("entro a crear");
              if ($id > 0) {
                $data['reingreso'] = $request['id'];
               }
              $otro = Orders::create($data);
              $j = strval($otro->id);
              $e = strval($prefijo);
                Orders::find($otro->id)->update([
                  'orden'=>$e.'-'.$j
                ]);
                /// variable historial
                  $data2['orden'] = $otro->id;
            }

          //// crecion de tabla historial git
          $git = Users::find($request['regrecibe']);
          $data2['nombre'] = $git->name;
          $data2['correo'] = $git->email;
          $data2['cargo'] = $git->position;

          History::create($data2);
        }
        if($request['tipo_orden'] == 2){
              $res = Brands::where('marc', '=', 'COBRABLE')->get();
              Log::info($res[0]->id);
              $prefijo = $res[0]->id;

              if ($request['entrada'] == 3) {
                    Orders::find($id)->update($data);
                    $j = strval($id);
                    $e = strval($prefijo);
                      Orders::find($id)->update([
                        'orden'=>$e.'-'.$j
                      ]);
                      /// variable historial
                        $data2['orden'] = $id;
              }
              if($request['entrada'] == 2){
                  Log::info("entro a crear");
                if ($id > 0) {
                  $data['reingreso'] = $request['id'];
                 }

                  $otro = Orders::create($data);
                  $j = strval($otro->id);
                  $e = strval($prefijo);
                    Orders::find($otro->id)->update([
                      'orden'=>$e.'-'.$j
                    ]);
                    $data2['orden'] = $otro->id;
              }


          //// crecion de tabla historial git
          $git = Users::find($request['regrecibe']);
          $data2['nombre'] = $git->name;
          $data2['correo'] = $git->email;
          $data2['cargo'] = $git->position;
          History::create($data2);
        }
        if($request['tipo_orden'] == 3){
          $asegurador = $request['aseguradora'];
          $res = Brands::find($asegurador);
          $prefijo = $res->id;
        /// editando orden
          if ($request['entrada'] == 3) {
                Orders::find($id)->update($data);
                $j = strval($id);
                $e = strval($prefijo);
                  Orders::find($id)->update([
                    'orden'=>$e.'-'.$j
                  ]);
                  /// variable historial
                    $data2['orden'] = $id;
          }
          if($request['entrada'] == 2){
              Log::info("entro a crear");
            if ($id > 0) {
                $data['reingreso'] = $request['id'];
             }
              $otro = Orders::create($data);
              $j = strval($otro->id);
              $e = strval($prefijo);
                Orders::find($otro->id)->update([
                  'orden'=>$e.'-'.$j
                ]);
                /// variable historial
                  $data2['orden'] = $otro->id;
          }
          //// crecion de tabla historial git
          $git = Users::find($request['regrecibe']);
          $data2['nombre'] = $git->name;
          $data2['correo'] = $git->email;
          $data2['cargo'] = $git->position;

          History::create($data2);
        }

      return response()->json([ 'message' => "Successfully created", 'data'=> $request['entrada'], "id"=> $id, 'success' => true ], 200);

    } catch (\Exception $e) {
      return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);

    }

  }


  // Funcion para actualizar las ordenes para realizar la entrega
  public function updateOrders(Request $request)
  {
    try {
      $id = $request['id'];
      Log::info("oooe");
      Log::info($id);
      $data['fecha_entrega'] = $request['fecha_entrega'];
      $data['obs_entrega'] = $request['obs_entrega'];
      $data['tecentrega'] = $request['tecentrega'];
      $data['regentrega'] = $request['regentrega'];
      $data['status_entrega'] = 1;
      Log::info($data);
      if($id > 0){
            Orders::find($id)->update($data);

            $git = Users::find($request['regentrega']);
            $data2['nombre'] = $git->name;
            $data2['correo'] = $git->email;
            $data2['cargo'] = $git->position;
            $data2['orden'] = $id;
            History::create($data2);
        }

      return response()->json([ 'message' => "Successfully actualizado", 'success' => true ], 200);

    } catch (\Exception $e) {
      return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);

    }

  }


/// traer datos de orden
  public function getOrders(){
  try {
    $response ='SELECT  orders.`id`, orders.`email`,  brands.`logo` AS logo, tipo_orden, tipo_servi AS tipo_servicio, caract_orde AS caract_orden,
  	nombre, orden, id_doc, direccion, barrio, ciudad,
  	celular, telcasa, teltrabajo, factura, seguimiento, brands2.`marc` AS aseguradora, brands.`marc` AS marca,
  	articu AS articulo, line AS linea,
   	modelo, serie, fecha_compra, distribuidor, accesorios, fecha_entrada, estado_fisico, sintoma, obs_entrada,
  	users.`name` AS tecrecibe  , users2.`name` AS regrecibe , users3.`name` AS tecasignado, users4.`name` AS regentregaorden, nombreStado AS estado_repa,fecha_revision, desc_falla, obs_reparacion,desc_reparacion,fecha_reparacion,
  	fecha_entrega, tecentrega, regentrega, obs_entrega, status_entrega,razon_devolucion,customer_id,device_id, orders.`tecrecibe` AS userone, orders.`regrecibe` AS usertwo, orders.`tecasignado` AS userthree
  	FROM orders
  	INNER JOIN order_types ON tipo_servicio = order_types.id
  	INNER JOIN order_traits ON caract_orden = order_traits.id
  	INNER JOIN brands ON marca = brands.id
  	LEFT JOIN brands AS brands2  ON aseguradora = brands2.id
  	INNER JOIN linea ON linea = linea.id
  	INNER JOIN items ON articulo = items.id
  	INNER JOIN order_status ON estado_repa = order_status.id
  	INNER JOIN users ON tecrecibe = users.`id`
  	INNER JOIN users AS users2 ON regrecibe = users2.`id`
  	INNER JOIN users AS users3 ON tecasignado = users3.`id`
    LEFT JOIN users AS users4 ON regentrega = users4.`id`

  	ORDER BY orders.`id` DESC';

    $data = DB::select($response);

     return response()->json(['message' => "Successfully loaded", 'data'=> $data, 'success' => true ], 200);
   } catch (\Exception $e) {
      return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);
    }
  }

  /// traer datos de orden
    public function getOrderOf(){
    try {
      $response ='SELECT  orders.`id`, orders.`email`,  brands.`logo` AS logo, tipo_orden, tipo_servi AS tipo_servicio, caract_orde AS caract_orden, nombre, orden, id_doc, direccion, barrio, ciudad,
    celular, telcasa, teltrabajo, factura, seguimiento, aseguradora, marc AS marca, brands.id AS idMarca,   articu AS articulo, line AS linea ,
    modelo, serie, fecha_compra, distribuidor, accesorios, fecha_entrada, estado_fisico, sintoma, obs_entrada,
    users.`name` AS tecrecibe  , users2.`name` AS regrecibe , users3.`name` AS tecasignado, nombreStado AS estado_repa,fecha_revision, desc_falla, obs_reparacion,desc_reparacion,fecha_reparacion,
    fecha_entrega, tecentrega, regentrega, obs_entrega, status_entrega,razon_devolucion,customer_id,device_id   FROM orders
    INNER JOIN order_types ON tipo_servicio = order_types.id
    INNER JOIN order_traits ON caract_orden = order_traits.id
    INNER JOIN brands ON marca = brands.id
    INNER JOIN linea ON linea = linea.id
    INNER JOIN items ON articulo = items.id
    INNER JOIN order_status ON estado_repa = order_status.id
    INNER JOIN users ON tecrecibe = users.`id`
    INNER JOIN users AS users2 ON regrecibe = users2.`id`
    INNER JOIN users AS users3 ON tecasignado = users3.`id`
    WHERE orders.`estado_repa` = 1
    ORDER BY orders.`id` DESC';

      $data = DB::select($response);
       return response()->json(['message' => "Successfully loaded", 'data'=> $data, 'success' => true ], 200);
     } catch (\Exception $e) {
        return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);
      }
    }


    ///orden status setOrdenRealizada

    /// traer datos de orden
      public function getOrdersOut(){
      try {
        $response ='SELECT  orders.`id`, orders.`email`,  brands.`logo` AS logo, tipo_orden, tipo_servi AS tipo_servicio, caract_orde AS caract_orden,
      	nombre, orden, id_doc, direccion, barrio, ciudad,
      	celular, telcasa, teltrabajo, factura, seguimiento, brands2.`marc` AS aseguradora, brands.`marc` AS marca,
      	articu AS articulo, line AS linea ,
      	modelo, serie, fecha_compra, distribuidor, accesorios, fecha_entrada, estado_fisico, sintoma, obs_entrada,
      	users.`name` AS tecrecibe  , users2.`name` AS regrecibe , users3.`name` AS tecasignado, nombreStado AS estado_repa,fecha_revision, desc_falla, obs_reparacion,desc_reparacion,fecha_reparacion,
      	fecha_entrega, tecentrega, regentrega, obs_entrega, status_entrega,razon_devolucion,customer_id,device_id
      	FROM orders
      	INNER JOIN order_types ON tipo_servicio = order_types.id
      	INNER JOIN order_traits ON caract_orden = order_traits.id
      	INNER JOIN brands ON marca = brands.id
      	INNER JOIN brands AS brands2  ON aseguradora = brands2.id
      	INNER JOIN linea ON linea = linea.id
      	INNER JOIN items ON articulo = items.id
      	INNER JOIN order_status ON estado_repa = order_status.id
      	INNER JOIN users ON tecrecibe = users.`id`
      	INNER JOIN users AS users2 ON regrecibe = users2.`id`
      	INNER JOIN users AS users3 ON tecasignado = users3.`id`
        WHERE orders.`status_entrega` = 0 AND orders.`estado_repa` = 3
      	ORDER BY orders.`id` DESC';

        $data = DB::select($response);
         return response()->json(['message' => "Successfully loaded", 'data'=> $data, 'success' => true ], 200);
       } catch (\Exception $e) {
          return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);
        }
      }


  public function TipoCarat(){
    try {
      $data = Order_traits::where("deleted",0)->get();
      return response()->json(['message' => "Successfully loaded", 'data'=> $data, 'success' => true ], 200);
    } catch (\Exception $e) {
      return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);
    }
  }

  public function TipoServi(){
    try {
      $data = Order_types::where("deleted",0)->get();
      return response()->json(['message' => "Successfully loaded", 'data'=> $data, 'success' => true ], 200);
    } catch (\Exception $e) {
      return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);
    }
  }

  public function Status(){
    try {
      $data = Order_status::where("deleted",0)->get();
      return response()->json(['message' => "Successfully loaded", 'data'=> $data, 'success' => true ], 200);
    } catch (\Exception $e) {
      return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);
    }
  }

  public function Customer_Device(){
    try {
      $data = Customer_Device::with("persona","dispositivo.brands")->get();
      return response()->json(['message' => "Successfully loaded", 'data'=> $data, 'success' => true ], 200);
    } catch (\Exception $e) {
      return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);
    }
  }
  ////funcion para pdf

  public function ordenPdf()
  {
    return view('files.MarcaEntrada');
  }

  public function pdfOrder(Request $request){

    $respuesta = $request['data'];
    // LOG::info($respuesta);
    $data = [
       'orden' => json_decode($respuesta),
     ];
    $res = json_decode($respuesta);
    if ($res->tipo_orden == 1){
       $pdf = PDF::loadView('files.GarantiaEntrada', $data);
       return $pdf->stream('GarantiaEntrada.pdf');
    }
    if($res->tipo_orden == 2){
         $pdf = PDF::loadView('files.CobrableEntrada', $data);
         return $pdf->stream('CobrableEntrada.pdf');
    }
    if($res->tipo_orden == 3){
         $pdf = PDF::loadView('files.GarExtendidaEntrada', $data);
         return $pdf->stream('GarExtendidaEntrada.pdf');
    }
    else{
      return "No se encuentra ningún tipo de validación, por favor verifique los datos";
    }
  }

  public function pdfOrderSalida(Request $request){
    $respuesta = $request['data'];
    // LOG::info($respuesta);
    $data = [
       'orden' => json_decode($respuesta),
     ];
    $res = json_decode($respuesta);

    if($res->tipo_orden == 1 && $res->marca =='SAMGUNG'){
         $pdf = PDF::loadView('files.SamsungSalida', $data);
         return $pdf->stream('SamsungSalida.pdf');
    }
    if($res->tipo_orden == 1 && $res->marca =='LG'){
         $pdf = PDF::loadView('files.LGSalida', $data);
         return $pdf->stream('LGSalida.pdf');
    }
    if ($res->tipo_orden == 1){
       $pdf = PDF::loadView('files.MarcaSalida', $data);
       return $pdf->stream('MarcaSalida.pdf');
    }
    if($res->tipo_orden == 2){
         $pdf = PDF::loadView('files.CobrableSalida', $data);
         return $pdf->stream('CobrableSalida.pdf');
    }
    if($res->tipo_orden == 3){
         $pdf = PDF::loadView('files.GarExtendidaSalida', $data);
         return $pdf->stream('GarExtendidaSalida.pdf');
    }
    else{
      return "No se encuentra ningún tipo de validación, por favor verifique los datos";
    }

  }


    // traer la cantidad de ordenes pendientes
      public function ordenPendientes(){
        try {
          $data = Orders::where('estado_repa',1)->get()->count();
          return response()->json(['message' => "Successfully loaded", 'data'=> $data, 'success' => true ], 200);
        } catch (\Exception $e) {
          return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);
        }
      }

    // traer la cantidad de ordenes realizadas por entregar
      public function ordenRealiPorEntrega(){
        try {
          $data = Orders::where('status_entrega',0)->where('estado_repa',3)->get()->count();
          return response()->json(['message' => "Successfully loaded", 'data'=> $data, 'success' => true ], 200);
        } catch (\Exception $e) {
          return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);
        }
      }

      public function OrdenHistory(){
        try {
          $data = History::orderBy('id','DESC')->get();
          return response()->json(['message' => "Successfully loaded", 'data'=> $data, 'success' => true ], 200);
        } catch (\Exception $e) {
          return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);
        }
      }


}
