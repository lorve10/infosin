<?php

namespace App\Http\Controllers\ADMIN;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order_traits;
use App\Models\Orders;

class CaratOrdenController extends Controller
{

public function saveCara(Request $request){
  try {
    $id = $request['id'];
    $data['caract_orde'] = $request['caract_orden'];
    $data['desc_codigo'] = $request['codigo'];

    if($id > 0){
      Order_traits::find($id)->update($data);
    }
    else{
      Order_traits::create($data);
    }
    return response()->json([ 'message' => "Successfully created", "success" => true ], 200);
  } catch (\Exception $e) {
    return response()->json([ 'message' => $e->getMessage(), "success" => false ], 500);
  }

}


public function deleted(Request $request){
  try {
    $id  = $request['id'];
    $existe = Orders::where('tipo_servicio','=', $id)->first();

    if($existe){
      $otro = 2;
      return response()->json([ "data" => $otro,'message' => "Successfully created", "success" => true ], 200);
    }
    else {
      Order_traits::where('id', $request['id'])->update([
      'deleted'=> 1
    ]);
      $data = 1;
      return response()->json([ "data" => $data, 'message' => "Successfully created", "success" => true ], 200);
    }
  } catch (\Exception $e) {
    return response()->json([ 'message' => $e->getMessage(), "success" => false ], 500);
  }

}
}
