<?php

namespace App\Http\Controllers\ADMIN;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Items;
use App\Models\Devices;
use Log;

class ItemsController extends Controller
{
  public function Items()
  {
    return view('admon.ItemsController');
  }

  // Funcion para guardar y Editar
  public function saveArticulo(Request $request)
  {
    try{
      $id = $request['id'];
      $data['articu'] = $request['articulo'];
      // Log::info($data);
      if ($id > 0){
        Items::find($id)->update($data);
      }
      else{
        Items::create($data);
      }
      return response()->json([ 'message' => "Successfully created", "success" => true ], 200);
    } catch (\Exception $e){
      return response()->json(['message' => $e->getMessage(),"success"=>false], 500);
    }
  }

  public function deletedItems(Request $request){
    try {
      $id = $request['id'];
      $existe = Devices::where('articu','=',$id)->first();

      if($existe){
        $otro = 2;
        return response()->json([ "data" => $otro,'message' => "Successfully created", "success" => true ], 200);
      }
      else {
        Items::where('id', $request['id'])->update([
        'deleted'=> 1
      ]);
        $data = 1;
        return response()->json([ "data" => $data, 'message' => "Successfully created", "success" => true ], 200);
      }
    } catch (\Exception $e){
      return response()->json([ 'message' => $e->getMessage(), "success" => false], 500);
    }
  }
}
