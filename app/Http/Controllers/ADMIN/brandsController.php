<?php

namespace App\Http\Controllers\ADMIN;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Brands;
use App\Models\Devices;
use Log;

class BrandsController extends Controller
{
  public function Brands()
  {
    return view('admon.brandsController');
  }

  ///Función para guardar y editar
  public function saveMarca(Request $request)
  {
    try {
      $id = $request['id'];
      $file = $request->file('file')->store('marcas_empresa','public');
      $res = "storage/".$file;
      $data['marc'] = strtoupper($request['marca']);
      $data['tipo'] = $request['tipo'];
      $data['logo'] = $res;
      if($id > 0){
        Brands::find($id)->update($data);
      }
      else{
        Brands::create($data);
      }
      return response()->json([ 'message' => "Successfully created", "success" => true ], 200);
    } catch (\Exception $e) {
      return response()->json([ 'message' => $e->getMessage(), "success" => false ], 500);
    }

  }

  public function deleted(Request $request){
    try {
      $id  = $request['id'];
      $existe = Devices::where('marca','=', $id)->first();

      if($existe){
        $otro = 2;
        return response()->json([ "data" => $otro,'message' => "Successfully created", "success" => true ], 200);
      }
      else {
        Brands::where('id', $request['id'])->update([
        'deleted'=> 1
      ]);
        $data = 1;
        return response()->json([ "data" => $data, 'message' => "Successfully created", "success" => true ], 200);
      }
    } catch (\Exception $e) {
      return response()->json([ 'message' => $e->getMessage(), "success" => false ], 500);
    }

  }

}
