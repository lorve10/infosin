<?php

namespace App\Http\Controllers\ADMIN;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Challenger;
use App\Models\Orders;
use Log;

class ChallengerController extends Controller
{
    public function Challenger(){
      return view('admon.Challenger');
    }

    public function saveChallenger(Request $request){
      try {
        $id = $request['id'];

        $data['pedido'] = $request['pedido'];
        $data['partenumero'] = $request['partenumero'];
        $data['descripcion'] = $request['descripcion'];
        $data['cantidad'] = $request['cantidad'];
        $data['modelo_part'] = $request['modelo'];
        $data['ordenkalley'] = $request['ordenkalley'];
        $data['tecnico'] = $request['tecnico'];
        $data['fechapedidotec'] = $request['fechapedidotec'];
        $data['solicita'] = $request['solicita'];
        $data['fechapedido'] = $request['fechapedido'];
        $data['order_id'] = $request['ordenkalley'];
        $user = Orders::find($request['ordenkalley']);
        Log::info("prueba");
        Log::info($user);
        Log::info($user->customer_id);

        $data['cust_id'] = $user->customer_id;

        if($id > 0){
              Challenger::find($id)->update($data);
        }
        else{
              Challenger::create($data);
          }
        return response()->json([ 'message' => "Successfully created", 'success' => true ], 200);

      } catch (\Exception $e) {
        return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);

      }

    }

    public function listChallenger(){
      try {
        $data = Challenger::with('ordenkall', 'tecnicoped', 'tecnicorecibe')->get();
        return response()->json([
          'message' => "Successfully loaded",
          'data'=> $data,
          'success' => true
        ], 200);
      } catch (\Exception $e) {
        return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);
      }

    }

    public function listChallengerP(){ /// funcion partes pendientes
      try {
        $data = Challenger::with('ordenkall', 'tecnicoped')->where('estado',0)->get();
        return response()->json([
          'message' => "Successfully loaded",
          'data'=> $data,
          'success' => true
        ], 200);
      } catch (\Exception $e) {
        return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);
      }

    }

    public function partes(){
      return view('admon.PartesAdmon');
    }


    public function savePart(Request $request){
      try {
        Log::info($request['order_id']);
        $id = $request['id'];
        $data['recibe'] = $request['recibe'];
        $data['fechallegada'] = $request['fechallegada'];
        $data['cantidadllega'] = $request['cantidadllega'];
        $data['observaciones'] = $request['observaciones'];
        $data['order_id'] = $request['order_id'];
        $data['cust_id'] = $request['customer_id'];
        $data['estado'] = 2;
        Challenger::find($id)->update($data);
        return response()->json([ 'message' => "Successfully created", 'success' => true ], 200);
      } catch (\Exception $e) {
        return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);

      }

    }


    // traer la cantidad de ordenes realizadas
      public function partesPendientes(){
        try {
          $data = Challenger::where('estado',0)->get()->count();
          return response()->json(['message' => "Successfully loaded", 'data'=> $data, 'success' => true ], 200);
        } catch (\Exception $e) {
          return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);
        }
      }


}
