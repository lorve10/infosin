<?php

namespace App\Http\Controllers\ADMIN;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order_status;
use App\Models\Orders;

class StatusController extends Controller
{
  public function saveStatus(Request $request){
    try {
      $id = $request['id'];
      $data['nombreStado'] = $request['nombre'];

      if($id > 0){
        Order_status::find($id)->update($data);
      }
      else{
        Order_status::create($data);
      }
      return response()->json([ 'message' => "Successfully created", "success" => true ], 200);
    } catch (\Exception $e) {
      return response()->json([ 'message' => $e->getMessage(), "success" => false ], 500);
    }

  }


  public function deleted(Request $request){
    try {
      $id  = $request['id'];
      $existe = Orders::where('tipo_servicio','=', $id)->first();

      if($existe){
        $otro = 2;
        return response()->json([ "data" => $otro,'message' => "Successfully created", "success" => true ], 200);
      }
      else {
        Order_status::where('id', $request['id'])->update([
        'deleted'=> 1
      ]);
        $data = 1;
        return response()->json([ "data" => $data, 'message' => "Successfully created", "success" => true ], 200);
      }
    } catch (\Exception $e) {
      return response()->json([ 'message' => $e->getMessage(), "success" => false ], 500);
    }

  }
}
