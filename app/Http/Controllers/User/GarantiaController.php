<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Orders;


class GarantiaController extends Controller
{
  public function garantia(){
    return view('Usuarios.UserOrdenes');
  }

  public function getGarantia(){
    try {
      $loguet = Auth()->user()->id;
      $data = Orders::with("servi","carat","brands","aseguradora","items","oline","user","user2","user3")
      ->where('tecasignado','=', $loguet)->get();

      return response()->json(['message' => "Successfully loaded", 'data'=> $data, 'success' => true ], 200);
    } catch (\Exception $e) {
      return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);
    }
  }


  public function saveCobrable (Request $request){

      try {
      $id = $request['id'];

      $data['fecha_revision'] = $request['fecha_revision'];
      $data['desc_falla'] = $request['desc_falla'];
      $data['obs_reparacion'] = $request['obs_reparacion'];
      $data['desc_reparacion'] = $request['desc_reparacion'];
      $data['fecha_reparacion'] = $request['fecha_reparacion'];
      $data['estado_repa'] = $request['estado_repa'];
      $data['estado'] = 2;
      Orders::find($id)->update($data);

      return response()->json([ 'message' => "Successfully created", 'success' => true ], 200);

    } catch (\Exception $e) {
      return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);
    }

  }
}
